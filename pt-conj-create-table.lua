local exports = {}
local lang = require('Module:languages').getByCode('pt')

local colors = {
 table_bg  = { 0xf6, 0xf6, 0xf6 },
 header    = { 0xd0, 0xd0, 0xd0 },
 infn      = { 0xc4, 0x98, 0xff },
 part_pres = { 0x98, 0xff, 0xc4 },
 part_past = { 0xff, 0xc4, 0x98 },
 indi      = { 0xd0, 0xdf, 0xf4 },
 subj      = { 0xd0, 0xf4, 0xd0 },
 impe      = { 0xf4, 0xe4, 0xd0 },
}

local border_style = '1px solid #999999'

local function offsetColor(color, offset)
   new_color = {}
   for i, c in ipairs(color) do new_color[i] = c - offset end
   return new_color
end

local function makeColor(color)
   s = '#'
   for i, c in ipairs(color) do s = s .. string.format("%02x", c) end
   return s
end

local function makeLink(term, face)
    local m_links = require('Module:links')
    local alt, sc, id, annotations, allowSelflink = nil, nil, nil, nil, true

    return m_links.full_link(term, alt, lang, sc, face, id, annotations, allowSelflink)
end

local function header(tag, wiki_text, color, attrs, css_attr)
   return tag:tag('th')
      :css('border', border_style)
      :css('background', makeColor(color))
      :css(css_attr or {})
      :attr(attrs or {})
      :wikitext(wiki_text or '')
      :done()
end

local function verbDescription(verbdata)
   local description = "This is "
   if verbdata.irregular then
      description = description .. "an irregular "
   else
      description = description .. "a regular "
   end
   if verbdata.abundant then description = description .. "abundant " end
   if verbdata.defective then description = description .. "defective " end
   return description .. "verb of the '''-" .. verbdata.suffix .. "''' group."
end

local function verbExamples(verbdata)
   local examples = "Verbs with this conjugation include: "
   for i, verb in ipairs(verbdata.examples) do
      examples = examples .. makeLink(verb, 'term')
      if i < #verbdata.examples then
         examples = examples .. ', '
      end
   end
   return examples .. '.'
end

local function addVerbInformation(tag, verbdata)
   local list = tag:tag('ul')
      :tag('li'):wikitext(verbDescription(verbdata)):done()
   for i, comment in ipairs(verbdata.comments) do
      list = list:tag('li'):wikitext(comment):done()
   end
   return list:tag('li'):wikitext(verbExamples(verbdata)):done():done()
end

local function addRowForForms(tag, name, forms, color)
   local makeCell = function(tag, content)
      local attrs = { valign='top' }
      if content == nil or content == "" then
        content = '-'
      else
        content = makeLink(content)
      end
      return tag:tag('td'):wikitext(content):attr(attrs):css('border', border_style)
   end

   local addOtherForm = function(tag, description, form)
      return tag
              :tag('br'):done()
              :wikitext(description)
              :tag('br'):done()
              :tag('small'):wikitext(makeLink(form)):done()
   end

   local tr = tag:tag('tr'):header(name, offsetColor(color, 32))
   if forms.sing and forms.plur then
      for _, number in ipairs({'sing', 'plur'}) do
         for i=1,3 do
            local defective = forms[number][tostring(i).."_defective"]
            local obsolete  = forms[number][tostring(i).."_obsolete"]
            local altforms  = forms[number][tostring(i).."_alt"]
            local stdform   = forms[number][tostring(i)]
            tr = makeCell(tr, stdform)
            if altforms then
                for _, form in ipairs(altforms) do
                   tr = tr:tag('br'):done():wikitext(makeLink(form))
                end
            end
            if defective then tr = addOtherForm(tr, "'''Normally defective:'''", defective) end
            if obsolete then tr = addOtherForm(tr, "'''Obsolete:'''", obsolete) end
            tr = tr:done()
        end
      end
   elseif #forms > 0 then
      for i=1,#forms do tr = makeCell(tr, forms[i]):attr({colspan=6/#forms}):done() end
   else
      error("don't know how to render " .. tostring(forms))
   end
   return tr:done()
end

local function addNormalPastParticiple(tag, f)
    return tag
    :tag('tr')
        :header("''Past Participle''", colors.part_past, {colspan='7'})
        :done()
    :addRowForForms('Masculine', { f.part_past.sing.m, f.part_past.plur.m }, colors.part_past)
    :addRowForForms('Feminine',  { f.part_past.sing.f, f.part_past.plur.f }, colors.part_past)
end

local function addShortAndLongPastParticiple(tag, f)
    return tag
    :tag('tr')
        :header("''Short past participle'''<sup><small>[1]</small></sup>", colors.part_past, {colspan='7'})
        :done()
    :addRowForForms('Masculine', { f.short_part_past.sing.m, f.short_part_past.plur.m }, colors.part_past)
    :addRowForForms('Feminine',  { f.short_part_past.sing.f, f.short_part_past.plur.f }, colors.part_past)
    :tag('tr')
        :header("''Long past participle'''<sup><small>[2]</small></sup>", colors.part_past, {colspan='7'})
        :done()
    :addRowForForms('Masculine', { f.long_part_past.sing.m, f.long_part_past.plur.m }, colors.part_past)
    :addRowForForms('Feminine',  { f.long_part_past.sing.f, f.long_part_past.plur.f }, colors.part_past)
end

local function addPastParticiple(tag, f)
   if f.short_part_past and f.long_part_past then
      return addShortAndLongPastParticiple(tag, f)
   else
      return addNormalPastParticiple(tag,f )
   end
end

local function addTableFooter(tag, f)
   if f.short_part_past and f.long_part_past then
      tag = tag:tag('ul')
      for i, tbl in ipairs({ { 'ser', 'estar'}, { 'haver', 'ter' } }) do
         tag = tag:tag('li')
            :tag('sup'):tag('small'):wikitext('['..i..']'):done():done()
            :wikitext("Used with the auxiliary verbs ")
            for j, verb in ipairs(tbl) do
               tag = tag:wikitext(makeLink(verb)):wikitext(j < #tbl and " and " or "")
            end
        tag = tag:wikitext('.'):done()
      end
      return tag:done()
   else
      return tag
   end
end


local function editLink(verbdata)
  local module = string.sub(verbdata.suffix, #verbdata.suffix-1)
  return "[".. tostring(mw.uri.fullUrl(
      "Module:User:Jberkel/pt-conj/data/-"..module, {action='edit'}))
      .." [edit]]"
end

local function createTable(verbdata)
   local builder =  mw.html.create(nil)
   getmetatable(builder).__index.header = header
   getmetatable(builder).__index.addVerbInformation = addVerbInformation
   getmetatable(builder).__index.addRowForForms = addRowForForms
   getmetatable(builder).__index.addPastParticiple = addPastParticiple
   getmetatable(builder).__index.addTableFooter = addTableFooter
   local header_css_attr = { width='12.5%' }
   local f = verbdata.forms
   return builder
    :tag('table')
      :attr('summary', "Conjugation of the " .. lang:getCanonicalName() .. " verb " .. verbdata.forms.infn.impe)
      :css('margin', '.4em')
      :css('border', border_style)
      :css('background', makeColor(colors.table_bg))
      :attr('cellpadding', '3')
      :attr('cellspacing', '0')
        :tag('tr')
           :tag('td')
           :css('border', border_style)
           :attr({colspan='7'})
           :tag('div')
              :css('text-align', 'left')
              :wikitext("'''Notes:'''")
              :tag('sup'):addClass('plainlinks')
                :wikitext(editLink(verbdata))
                :done()
              :done()
           :tag('div')
              :css('white-space', 'normal'):css('text-align', 'left')
              :addVerbInformation(verbdata)
              :done()
           :done()
        :done()
        :tag('tr')
          :header('', offsetColor(colors.header, 32), {rowspan='2'})
          :header('Singular', colors.header, {colspan='3'})
          :header('Plural',   colors.header, {colspan='3'})
          :done()
        :tag('tr')
          :header('First-person<br>('  .. makeLink('eu') .. ')', colors.header, {}, header_css_attr)
          :header('Second-person<br>(' .. makeLink('tu') .. ')', colors.header, {}, header_css_attr)
          :header('Third-person<br>('  .. makeLink('ele') .. ' / ' .. makeLink('ela') .. ' / ' .. makeLink('você') .. ')', colors.header, {}, header_css_attr)
          :header('First-person<br>('  .. makeLink('nós') .. ')', colors.header, {}, header_css_attr)
          :header('Second-person<br>(' .. makeLink('vós') .. ')', colors.header, {}, header_css_attr)
          :header('Third-person<br>('  .. makeLink('eles') .. ' / ' .. makeLink('elas') .. ' / ' .. makeLink('vocês') .. ')', colors.header, {}, header_css_attr)
          :done()
        :tag('tr')
          :header("''Infinitive''", colors.infn, {colspan='7'})
          :done()
        :addRowForForms("'''Impersonal'''", { f.infn.impe }, colors.infn)
        :addRowForForms("'''Personal'''",     f.infn.pers  , colors.infn)
        :tag('tr')
          :header("''Gerund''", colors.part_pres, {colspan='7'})
          :done()
        :addRowForForms('', { f.part_pres }, colors.part_pres)
        :addPastParticiple(f)
        :tag('tr')
          :header("''Indicative''", colors.indi, {colspan='7'})
          :done()
        :addRowForForms('Present',     f.indi.pres, colors.indi)
        :addRowForForms('Imperfect',   f.indi.impf, colors.indi)
        :addRowForForms('Preterite',   f.indi.pret, colors.indi)
        :addRowForForms('Pluperfect',  f.indi.plpf, colors.indi)
        :addRowForForms('Future',      f.indi.futu, colors.indi)
        :addRowForForms('Conditional', f.indi.cond, colors.indi)
        :tag('tr')
          :header("''Subjunctive''", colors.subj, {colspan='7'})
          :done()
        :addRowForForms('Present',   f.subj.pres, colors.subj)
        :addRowForForms('Imperfect', f.subj.impf, colors.subj)
        :addRowForForms('Future',    f.subj.futu, colors.subj)
        :tag('tr')
          :header("''Imperative''", colors.impe, {colspan='7'})
          :done()
        :addRowForForms('Affirmative', f.impe.affr, colors.impe)
        :addRowForForms('Negative (' .. makeLink('não') .. ')', f.impe.negt, colors.impe)
    :done() -- table
    :addTableFooter(f)
end

local function createNavFrame(verbdata)
    return mw.html.create('div')
    :addClass('NavFrame')
    :css('clear', 'both')
    :css('white-space', 'nowrap')
    :tag('div')
    :addClass('NavHead')
    :wikitext("&nbsp;&nbsp;Conjugation of the [[Appendix:" .. lang:getCanonicalName() .. " verbs|" ..
            lang:getCanonicalName() .. " ''-" ..
            verbdata.suffix .. "'' verb]] " ..
            "''" .. verbdata.forms.infn.impe .. "''")
    :done()
    :tag('div')
    :addClass('NavContent')
    :attr({align='left'})
end

function exports.createNavAndTable(verbdata)
    return tostring(createNavFrame(verbdata)
    :node(createTable(verbdata)):allDone())
end

return exports
