WIKTIONARY_USER := jberkel
#WIKTIONARY_PASSWORD :=

PAGES := \
	Module:pt-conj \
	Module:pt-conj/testcases \
	Module:pt-conj/testcases/documentation \
	Module:pt-conj/table \
	Module:pt-conj/table/testcases \
	Module:pt-conj/table/testcases/documentation \
	Module:pt-headword \
	Module:pt-headword/testcases \
	Module:pt-headword/testcases/documentation \
	Template:pt-verb \
	Module:pt-conj/data/-ar \
	Module:pt-conj/data/-ir \
	Module:pt-conj/data/-er

# https://bits.wikimedia.org/en.wiktionary.org/load.php?debug=true&lang=en&modules=site&only=styles&skin=vector&*
STYLES := \
	MediaWiki:Common.css

SHALLOW_CLONE = true

%.json.lua: %.json
	(echo "return " && lua json2lua.lua) < $< > $<.lua

table:
	./create_table.lua > table.html

tidy: table
	tidy -i table.html > tidy.html

test:
	lua pt-conj-test.lua

lint:
	for module in pages/Module:*; do \
		luac -p $$module ; \
	done

clean:
	rm -f table.html data.json tidy.html *.json.lua

pages:
	git clone \
	-c remote.origin.shallow=$(SHALLOW_CLONE) \
	-c remote.origin.pages='$(PAGES)' \
	-c remote.origin.mwLogin=$(WIKTIONARY_USER) \
	-c remote.origin.mwPassword=$(WIKTIONARY_PASSWORD) \
	mediawiki::https://en.wiktionary.org/w/ $@

clone_styles:
	git clone \
	-c remote.origin.shallow=false \
	-c remote.origin.pages='$(STYLES)' \
	mediawiki::http://en.wiktionary.org/w/ styles

install:
	cpan MediaWiki::API
	cpan -f DateTime::Format::ISO8601
	cpan Net::SSLeay
	cpan IO::Socket::SSL
	cpan LWP::Protocol::https
