require 'lua-nucleo'
local tpretty = require 'lua-nucleo/tpretty'
local input = io.read("*all")
local input_data = load(input)()

for k,v in pairs(input_data) do
    for _, form in pairs({'1', '1_alt', '1_obsolete', '1_defective', '3', '3_alt', '3_obsolete', '3_defective'}) do
        v.forms.impe.affr.sing[form] = nil
        v.forms.impe.affr.plur[form] = nil
    end

    v.forms.impe.negt = nil
end

print(tpretty.tpretty_ordered(input_data))
