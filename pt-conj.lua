local exports = {}

local function verbData(ending)
   local group
   if ending == 'pôr' or ending == 'por' then
     group = 'er'
   else
     group = string.sub(ending, #ending-1)
   end
   local success, m_verb_data = pcall(require, "Module:User:Jberkel/pt-conj/data/-"..group)
   if success and m_verb_data[ending] then
      return mw.clone(m_verb_data[ending])
   else
      return nil
   end
end

local function applyFuncToTableValues(tbl, func)
   for k,v in pairs(tbl) do
      if type(v) == 'table' then
         applyFuncToTableValues(v, func)
      else
         tbl[k] = func(v)
      end
   end
end

function exports.inflect(stem, ending, compound)
   local data = verbData(ending)
   if data then
      applyFuncToTableValues(data.forms, function(form)
         if form ~= "" then
            return stem .. form .. (compound and ('&#32;' .. compound) or '')
         else
            return ""
         end
      end)
      return data
   else
      return nil
   end
end


function exports.createTable(frame)
   local m_table_creator = require("Module:User:Jberkel/pt-conj/tableCreator")
   local args = frame:getParent().args
   local stem, ending, compound = args[1], args[2], args[3]
   local word = exports.inflect(stem, ending, compound)
   if word then
      return m_table_creator.createNavAndTable(word)
   else
      error("No inflection data found")
   end
end


return exports
