#!/usr/bin/env python

import re
import codecs
import json
import glob

FORMS = 'forms'
EXAMPLE_PATTERN = re.compile(r'example(\d+)=([^<]+)')
COMMENT_PATTERN = re.compile(r'comment(\d+)=([^<]+)')
SUFFIX_PATTERN = re.compile(r'suffix=([^<]+)')
IRREGULAR_PATTERN = re.compile(r'irregular=([^<]+)')
DEFECTIVE_PATTERN = re.compile(r'defective=([^<]+)')
ABUNDANT_PATTERN = re.compile(r'abundant=([^<]+)')
# infn_pers_sing_1st/form1
# part_pres/form1=fazendo
# part_past_sing_masc/form1
# part_past_sing_femi/form1
CONJ_PATTERN = re.compile(r'\|([^/]+)/(form|defective|obsolete)(\d)=([^<]*)')
STD_PATTERN = re.compile(r'([^_]+)_([^_]+)_(plur|sing)_(\d)(?:st|nd|rd)')
PART_PATTERN = re.compile(r'((?:short_|long_)?[^_]+)_([^_]+)_(plur|sing)_(masc|femi)')
ALT_PART_PATTERN = re.compile(r'([^_]+)_([^_]+)_(masc|femi)_(plur|sing)')


def extract(file):
    ending = re.compile(r'%2F([^\.]+)\.mw').search(file)
    if ending:
        verb = ending.group(1)
    else:
        raise Exception("could not determine ending of "+file)

    data = {'verb': verb, 'examples': [], 'comments': [], 'forms': {}}
    with codecs.open(file, 'r', 'UTF-8') as f:
        for line in f.readlines():
            match = EXAMPLE_PATTERN.search(line)
            if match:
                data['examples'].append(match.group(2))
                continue
            match = COMMENT_PATTERN.search(line)
            if match:
                data['comments'].append(match.group(2))
                continue
            match = IRREGULAR_PATTERN.search(line)
            if match:
                data['irregular'] = match.group(1) == '1'
                continue
            match = DEFECTIVE_PATTERN.search(line)
            if match:
                data['defective'] = match.group(1) == '1'
                continue
            match = ABUNDANT_PATTERN.search(line)
            if match:
                data['abundant'] = match.group(1) == '1'
                continue
            match = SUFFIX_PATTERN.search(line)
            if match:
                data['suffix'] = match.group(1)
                continue
            match = CONJ_PATTERN.search(line)
            if match:
                name = match.group(1)
                form_type = match.group(2)
                form_number = match.group(3)
                form = match.group(4)
                match = STD_PATTERN.match(name)
                if match:
                    mood, tense, sp, number = match.group(1), match.group(2), match.group(3), match.group(4)
                    if mood not in data[FORMS]:
                        data[FORMS][mood] = {}
                    if tense not in data[FORMS][mood]:
                        data[FORMS][mood][tense] = {}
                    if sp not in data[FORMS][mood][tense]:
                        data[FORMS][mood][tense][sp] = {}

                    if form_type == 'defective':
                        number = number + "_defective"
                    elif form_type == 'obsolete':
                        number = number + "_obsolete"

                    if form_number != "1":
                        k = number + "_alt"
                        if not k in data[FORMS][mood][tense][sp]:
                            data[FORMS][mood][tense][sp][k] = []
                        data[FORMS][mood][tense][sp][k].append(form)
                    else:
                        data[FORMS][mood][tense][sp][number] = form
                    continue
                match = PART_PATTERN.match(name)
                if match:
                    mood, tense, sp, mf = match.group(1), match.group(2), match.group(3), match.group(4)
                    prefix = mood + "_" + tense
                    if prefix not in data[FORMS]:
                        data[FORMS][prefix] = {}

                    if sp not in data[FORMS][prefix]:
                        data[FORMS][prefix][sp] = {}
                    data[FORMS][prefix][sp][mf[0:1]] = form
                    continue
                match = ALT_PART_PATTERN.match(name)
                if match:
                    mood, tense, mf, sp = match.group(1), match.group(2), match.group(3), match.group(4)
                    prefix = mood + "_" + tense
                    if prefix not in data[FORMS]:
                        data[FORMS][prefix] = {}

                    if sp not in data[FORMS][prefix]:
                        data[FORMS][prefix][sp] = {}
                    data[FORMS][prefix][sp][mf[0:1]] = form
                    continue

                if name in ['part_pres']:
                    data[FORMS][name] = form
                    continue
                if name == 'infn':
                    if 'infn' not in data[FORMS]:
                        data[FORMS]['infn'] = {}
                    data[FORMS]['infn']['impe'] = form
                    continue

                raise Exception("unhandled: %s/%s" %(name, form))

    if len(data['forms']) == 0:
        return None
    else:
        return data


def list_tables():
    return glob.glob('pages/Template:pt-verb%2F*')


def clean_data(data):
    impe_affr_sing = data['forms']['impe']['affr']['sing']
    impe_affr_plur = data['forms']['impe']['affr']['plur']

    if 'negt' in data['forms']['impe']:
        impe_negt_sing = data['forms']['impe']['negt']['sing']
        impe_negt_plur = data['forms']['impe']['negt']['plur']
    else:
        impe_negt_sing = {}
        impe_negt_plur = {}

    for forms in [impe_affr_sing, impe_negt_sing]:
        for key in filter(lambda key: key.startswith("1"), forms.keys()):
            del forms[key]
            #if "_" in key:
            #    del forms[key]
            #else:
            #   forms[key] = ""

    return data


def main():
    data = {}
    for extracted in map(clean_data, filter(lambda ret: ret is not None, map(lambda file: extract(file), list_tables()))):
        data[extracted['verb']] = extracted
    with open('data.json', 'w') as out:
        json.dump(data,  out)

main()
