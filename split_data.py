#!/usr/bin/env python

import json
import codecs
import sys

writer = codecs.getwriter('UTF-8')(sys.stdout)
with open(sys.argv[1], 'r') as f:
    data = json.load(f)
    #print(data)

    by_short_suffix = {}
    for (verb, vdata) in data.iteritems():
        suffix = vdata['suffix']
        short_suffix = suffix[-2:]
        #writer.write(verb+"\t"+suffix+"\n")
        if short_suffix not in by_short_suffix:
            by_short_suffix[short_suffix] = {}

        by_short_suffix[short_suffix][vdata['verb']] = vdata

    for suffix, data in by_short_suffix.iteritems():
        with open(suffix+'.json', 'w') as f:
            json.dump(data, f)
