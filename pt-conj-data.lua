return 
{
  abolir =
  {
    comments =
    {
      "When the suffix of a conjugated form starts with ''a'' or ''o'', this conjugated form does not exist. However, this rule may be ignored, so the verb can also be fully conjugated.";
    };
    defective = true;
    examples = { "abolir", "explodir", "latir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "abolamos";
            ["2"] = "aboli";
            ["3_defective"] = "abolam";
          };
          sing =
          {
            ["1_defective"] = "abola";
            ["2"] = "abole";
            ["3_defective"] = "abola";
          };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "abolamos";
            ["2_defective"] = "abolais";
            ["3_defective"] = "abolam";
          };
          sing =
          {
            ["1_defective"] = "abola";
            ["2_defective"] = "abolas";
            ["3_defective"] = "abola";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "aboliríamos", ["2"] = "aboliríeis", ["3"] = "aboliriam" };
          sing = { ["1"] = "aboliria", ["2"] = "abolirias", ["3"] = "aboliria" };
        };
        futu =
        {
          plur = { ["1"] = "aboliremos", ["2"] = "abolireis", ["3"] = "abolirão" };
          sing = { ["1"] = "abolirei", ["2"] = "abolirás", ["3"] = "abolirá" };
        };
        impf =
        {
          plur = { ["1"] = "abolíamos", ["2"] = "abolíeis", ["3"] = "aboliam" };
          sing = { ["1"] = "abolia", ["2"] = "abolias", ["3"] = "abolia" };
        };
        plpf =
        {
          plur = { ["1"] = "abolíramos", ["2"] = "abolíreis", ["3"] = "aboliram" };
          sing = { ["1"] = "abolira", ["2"] = "aboliras", ["3"] = "abolira" };
        };
        pres =
        {
          plur = { ["1"] = "abolimos", ["2"] = "abolis", ["3"] = "abolem" };
          sing = { ["1_defective"] = "abolo", ["2"] = "aboles", ["3"] = "abole" };
        };
        pret =
        {
          plur = { ["1"] = "abolimos", ["2"] = "abolistes", ["3"] = "aboliram" };
          sing = { ["1"] = "aboli", ["2"] = "aboliste", ["3"] = "aboliu" };
        };
      };
      infn =
      {
        impe = "abolir";
        pers =
        {
          plur = { ["1"] = "abolirmos", ["2"] = "abolirdes", ["3"] = "abolirem" };
          sing = { ["1"] = "abolir", ["2"] = "abolires", ["3"] = "abolir" };
        };
      };
      part_past =
      {
        plur = { f = "abolidas", m = "abolidos" };
        sing = { f = "abolida", m = "abolido" };
      };
      part_pres = "abolindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "abolirmos", ["2"] = "abolirdes", ["3"] = "abolirem" };
          sing = { ["1"] = "abolir", ["2"] = "abolires", ["3"] = "abolir" };
        };
        impf =
        {
          plur = { ["1"] = "abolíssemos", ["2"] = "abolísseis", ["3"] = "abolissem" };
          sing = { ["1"] = "abolisse", ["2"] = "abolisses", ["3"] = "abolisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "abolamos";
            ["2_defective"] = "abolais";
            ["3_defective"] = "abolam";
          };
          sing =
          {
            ["1_defective"] = "abola";
            ["2_defective"] = "abolas";
            ["3_defective"] = "abola";
          };
        };
      };
    };
    suffix = "ir";
    verb = "abolir";
  };
  aguar =
  {
    comments =
    {
      "An acute accent is added in the stem of a few conjugated forms.";
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced.";
    };
    examples = { "aguar", "apropinquar", "desaguar", "enxaguar", "minguar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "aguemos";
            ["1_obsolete"] = "agüemos";
            ["2"] = "aguai";
            ["3"] = "águem";
            ["3_obsolete"] = "ágüem";
          };
          sing =
          {
            ["1"] = "águe";
            ["1_obsolete"] = "ágüe";
            ["2"] = "agua";
            ["3"] = "águe";
            ["3_obsolete"] = "ágüe";
          };
        };
        negt =
        {
          plur =
          {
            ["1"] = "aguemos";
            ["1_obsolete"] = "agüemos";
            ["2"] = "agueis";
            ["2_obsolete"] = "agüeis";
            ["3"] = "águem";
            ["3_obsolete"] = "ágüem";
          };
          sing =
          {
            ["1"] = "águe";
            ["1_obsolete"] = "ágüe";
            ["2"] = "águes";
            ["2_obsolete"] = "ágües";
            ["3"] = "águe";
            ["3_obsolete"] = "ágüe";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "aguaríamos", ["2"] = "aguaríeis", ["3"] = "aguariam" };
          sing = { ["1"] = "aguaria", ["2"] = "aguarias", ["3"] = "aguaria" };
        };
        futu =
        {
          plur = { ["1"] = "aguaremos", ["2"] = "aguareis", ["3"] = "aguarão" };
          sing = { ["1"] = "aguarei", ["2"] = "aguarás", ["3"] = "aguará" };
        };
        impf =
        {
          plur = { ["1"] = "aguávamos", ["2"] = "aguáveis", ["3"] = "aguavam" };
          sing = { ["1"] = "aguava", ["2"] = "aguavas", ["3"] = "aguava" };
        };
        plpf =
        {
          plur = { ["1"] = "aguáramos", ["2"] = "aguáreis", ["3"] = "aguaram" };
          sing = { ["1"] = "aguara", ["2"] = "aguaras", ["3"] = "aguara" };
        };
        pres =
        {
          plur = { ["1"] = "aguamos", ["2"] = "aguais", ["3"] = "águam" };
          sing = { ["1"] = "águo", ["2"] = "águas", ["3"] = "água" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "aguamos";
            ["1_alt"] = { "aguámos" };
            ["2"] = "aguastes";
            ["3"] = "aguaram";
          };
          sing =
          {
            ["1"] = "aguei";
            ["1_obsolete"] = "agüei";
            ["2"] = "aguaste";
            ["3"] = "aguou";
          };
        };
      };
      infn =
      {
        impe = "aguar";
        pers =
        {
          plur = { ["1"] = "aguarmos", ["2"] = "aguardes", ["3"] = "aguarem" };
          sing = { ["1"] = "aguar", ["2"] = "aguares", ["3"] = "aguar" };
        };
      };
      part_past =
      {
        plur = { f = "aguadas", m = "aguados" };
        sing = { f = "aguada", m = "aguado" };
      };
      part_pres = "aguando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "aguarmos", ["2"] = "aguardes", ["3"] = "aguarem" };
          sing = { ["1"] = "aguar", ["2"] = "aguares", ["3"] = "aguar" };
        };
        impf =
        {
          plur = { ["1"] = "aguássemos", ["2"] = "aguásseis", ["3"] = "aguassem" };
          sing = { ["1"] = "aguasse", ["2"] = "aguasses", ["3"] = "aguasse" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "aguemos";
            ["1_obsolete"] = "agüemos";
            ["2"] = "agueis";
            ["2_obsolete"] = "agüeis";
            ["3"] = "ágüem";
            ["3_obsolete"] = "águem";
          };
          sing =
          {
            ["1"] = "águe";
            ["1_obsolete"] = "ágüe";
            ["2"] = "águes";
            ["2_obsolete"] = "ágües";
            ["3"] = "águe";
            ["3_obsolete"] = "ágüe";
          };
        };
      };
    };
    suffix = "ar";
    verb = "aguar";
  };
  air =
  {
    comments = { };
    examples = { "atrair", "cair", "contrair", "distrair", "sair", "subtrair", "trair" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "aiamos", ["2"] = "ai", ["3"] = "aiam" };
          sing = { ["1"] = "aia", ["2"] = "ai", ["3"] = "aia" };
        };
        negt =
        {
          plur = { ["1"] = "aiamos", ["2"] = "aiais", ["3"] = "aiam" };
          sing = { ["1"] = "aia", ["2"] = "aias", ["3"] = "aia" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "airíamos", ["2"] = "airíeis", ["3"] = "airiam" };
          sing = { ["1"] = "airia", ["2"] = "airias", ["3"] = "airia" };
        };
        futu =
        {
          plur = { ["1"] = "airemos", ["2"] = "aireis", ["3"] = "airão" };
          sing = { ["1"] = "airei", ["2"] = "airás", ["3"] = "airá" };
        };
        impf =
        {
          plur = { ["1"] = "aíamos", ["2"] = "aíeis", ["3"] = "aíam" };
          sing = { ["1"] = "aía", ["2"] = "aías", ["3"] = "aía" };
        };
        plpf =
        {
          plur = { ["1"] = "aíramos", ["2"] = "aíreis", ["3"] = "aíram" };
          sing = { ["1"] = "aíra", ["2"] = "aíras", ["3"] = "aíra" };
        };
        pres =
        {
          plur = { ["1"] = "aímos", ["2"] = "aís", ["3"] = "aem" };
          sing = { ["1"] = "aio", ["2"] = "ais", ["3"] = "ai" };
        };
        pret =
        {
          plur = { ["1"] = "aímos", ["2"] = "aístes", ["3"] = "aíram" };
          sing = { ["1"] = "aí", ["2"] = "aíste", ["3"] = "aiu" };
        };
      };
      infn =
      {
        impe = "air";
        pers =
        {
          plur = { ["1"] = "airmos", ["2"] = "airdes", ["3"] = "aírem" };
          sing = { ["1"] = "air", ["2"] = "aíres", ["3"] = "air" };
        };
      };
      part_past =
      {
        plur = { f = "aídas", m = "aídos" };
        sing = { f = "aída", m = "aído" };
      };
      part_pres = "aindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "airmos", ["2"] = "airdes", ["3"] = "aírem" };
          sing = { ["1"] = "air", ["2"] = "aíres", ["3"] = "air" };
        };
        impf =
        {
          plur = { ["1"] = "aíssemos", ["2"] = "aísseis", ["3"] = "aíssem" };
          sing = { ["1"] = "aísse", ["2"] = "aísses", ["3"] = "aísse" };
        };
        pres =
        {
          plur = { ["1"] = "aiamos", ["2"] = "aiais", ["3"] = "aiam" };
          sing = { ["1"] = "aia", ["2"] = "aias", ["3"] = "aia" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "air";
  };
  apropinquar =
  {
    comments =
    {
      "An acute accent is added in the stem of a few conjugated forms.";
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced.";
    };
    examples = { "aguar", "apropinquar", "desaguar", "enxaguar", "minguar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "apropinquemos";
            ["1_obsolete"] = "apropinqüemos";
            ["2"] = "apropinquai";
            ["3"] = "apropínquem";
            ["3_obsolete"] = "apropínqüem";
          };
          sing =
          {
            ["1"] = "apropínque";
            ["1_obsolete"] = "apropínqüe";
            ["2"] = "apropinqua";
            ["3"] = "apropínque";
            ["3_obsolete"] = "apropínqüe";
          };
        };
        negt =
        {
          plur =
          {
            ["1"] = "apropinquemos";
            ["1_obsolete"] = "apropinqüemos";
            ["2"] = "apropinqueis";
            ["2_obsolete"] = "apropinqüeis";
            ["3"] = "apropínquem";
            ["3_obsolete"] = "apropínqüem";
          };
          sing =
          {
            ["1"] = "apropínque";
            ["1_obsolete"] = "apropínqüe";
            ["2"] = "apropínques";
            ["2_obsolete"] = "apropínqües";
            ["3"] = "apropínque";
            ["3_obsolete"] = "apropínqüe";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "apropinquaríamos";
            ["2"] = "apropinquaríeis";
            ["3"] = "apropinquariam";
          };
          sing =
          {
            ["1"] = "apropinquaria";
            ["2"] = "apropinquarias";
            ["3"] = "apropinquaria";
          };
        };
        futu =
        {
          plur =
          {
            ["1"] = "apropinquaremos";
            ["2"] = "apropinquareis";
            ["3"] = "apropinquarão";
          };
          sing =
          {
            ["1"] = "apropinquarei";
            ["2"] = "apropinquarás";
            ["3"] = "apropinquará";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "apropinquávamos";
            ["2"] = "apropinquáveis";
            ["3"] = "apropinquavam";
          };
          sing =
          {
            ["1"] = "apropinquava";
            ["2"] = "apropinquavas";
            ["3"] = "apropinquava";
          };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "apropinquáramos";
            ["2"] = "apropinquáreis";
            ["3"] = "apropinquaram";
          };
          sing =
          {
            ["1"] = "apropinquara";
            ["2"] = "apropinquaras";
            ["3"] = "apropinquara";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "apropinquamos";
            ["2"] = "apropinquais";
            ["3"] = "apropínquam";
          };
          sing =
          {
            ["1"] = "apropínquo";
            ["2"] = "apropínquas";
            ["3"] = "apropínqua";
          };
        };
        pret =
        {
          plur =
          {
            ["1"] = "apropinquamos";
            ["1_alt"] = { "apropinquámos" };
            ["2"] = "apropinquastes";
            ["3"] = "apropinquaram";
          };
          sing =
          {
            ["1"] = "apropinquei";
            ["1_obsolete"] = "apropinqüei";
            ["2"] = "apropinquaste";
            ["3"] = "apropinquou";
          };
        };
      };
      infn =
      {
        impe = "apropinquar";
        pers =
        {
          plur =
          {
            ["1"] = "apropinquarmos";
            ["2"] = "apropinquardes";
            ["3"] = "apropinquarem";
          };
          sing =
          {
            ["1"] = "apropinquar";
            ["2"] = "apropinquares";
            ["3"] = "apropinquar";
          };
        };
      };
      part_past =
      {
        plur = { f = "apropinquadas", m = "apropinquados" };
        sing = { f = "apropinquada", m = "apropinquado" };
      };
      part_pres = "apropinquando";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1"] = "apropinquarmos";
            ["2"] = "apropinquardes";
            ["3"] = "apropinquarem";
          };
          sing =
          {
            ["1"] = "apropinquar";
            ["2"] = "apropinquares";
            ["3"] = "apropinquar";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "apropinquássemos";
            ["2"] = "apropinquásseis";
            ["3"] = "apropinquassem";
          };
          sing =
          {
            ["1"] = "apropinquasse";
            ["2"] = "apropinquasses";
            ["3"] = "apropinquasse";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "apropinquemos";
            ["1_obsolete"] = "apropinqüemos";
            ["2"] = "apropinqueis";
            ["2_obsolete"] = "apropinqüeis";
            ["3"] = "apropínqüem";
            ["3_obsolete"] = "apropínquem";
          };
          sing =
          {
            ["1"] = "apropínque";
            ["1_obsolete"] = "apropínqüe";
            ["2"] = "apropínques";
            ["2_obsolete"] = "apropínqües";
            ["3"] = "apropínque";
            ["3_obsolete"] = "apropínqüe";
          };
        };
      };
    };
    suffix = "ar";
    verb = "apropinquar";
  };
  ar =
  {
    comments = { };
    examples =
    {
      "amar";
      "cantar";
      "gritar";
      "marchar";
      "mostrar";
      "nadar";
      "parar";
      "participar";
      "retirar";
      "separar";
      "viajar";
    };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "emos", ["2"] = "ai", ["3"] = "em" };
          sing = { ["1"] = "e", ["2"] = "a", ["3"] = "e" };
        };
        negt =
        {
          plur = { ["1"] = "emos", ["2"] = "eis", ["3"] = "em" };
          sing = { ["1"] = "e", ["2"] = "es", ["3"] = "e" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "aríamos", ["2"] = "aríeis", ["3"] = "ariam" };
          sing = { ["1"] = "aria", ["2"] = "arias", ["3"] = "aria" };
        };
        futu =
        {
          plur = { ["1"] = "aremos", ["2"] = "areis", ["3"] = "arão" };
          sing = { ["1"] = "arei", ["2"] = "arás", ["3"] = "ará" };
        };
        impf =
        {
          plur = { ["1"] = "ávamos", ["2"] = "áveis", ["3"] = "avam" };
          sing = { ["1"] = "ava", ["2"] = "avas", ["3"] = "ava" };
        };
        plpf =
        {
          plur = { ["1"] = "áramos", ["2"] = "áreis", ["3"] = "aram" };
          sing = { ["1"] = "ara", ["2"] = "aras", ["3"] = "ara" };
        };
        pres =
        {
          plur = { ["1"] = "amos", ["2"] = "ais", ["3"] = "am" };
          sing = { ["1"] = "o", ["2"] = "as", ["3"] = "a" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "amos";
            ["1_alt"] = { "ámos" };
            ["2"] = "astes";
            ["3"] = "aram";
          };
          sing = { ["1"] = "ei", ["2"] = "aste", ["3"] = "ou" };
        };
      };
      infn =
      {
        impe = "ar";
        pers =
        {
          plur = { ["1"] = "armos", ["2"] = "ardes", ["3"] = "arem" };
          sing = { ["1"] = "ar", ["2"] = "ares", ["3"] = "ar" };
        };
      };
      part_past =
      {
        plur = { f = "adas", m = "ados" };
        sing = { f = "ada", m = "ado" };
      };
      part_pres = "ando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "armos", ["2"] = "ardes", ["3"] = "arem" };
          sing = { ["1"] = "ar", ["2"] = "ares", ["3"] = "ar" };
        };
        impf =
        {
          plur = { ["1"] = "ássemos", ["2"] = "ásseis", ["3"] = "assem" };
          sing = { ["1"] = "asse", ["2"] = "asses", ["3"] = "asse" };
        };
        pres =
        {
          plur = { ["1"] = "emos", ["2"] = "eis", ["3"] = "em" };
          sing = { ["1"] = "e", ["2"] = "es", ["3"] = "e" };
        };
      };
    };
    suffix = "ar";
    verb = "ar";
  };
  aver =
  {
    comments = { };
    defective = true;
    examples = { "precaver", "reaver" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "avamos", ["2"] = "avei", ["3"] = "avam" };
          sing = { ["1"] = "ava", ["2"] = "ave", ["3"] = "ava" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "averíamos", ["2"] = "averíeis", ["3"] = "averiam" };
          sing = { ["1"] = "averia", ["2"] = "averias", ["3"] = "averia" };
        };
        futu =
        {
          plur = { ["1"] = "averemos", ["2"] = "avereis", ["3"] = "averão" };
          sing = { ["1"] = "averei", ["2"] = "averás", ["3"] = "averá" };
        };
        impf =
        {
          plur = { ["1"] = "avíamos", ["2"] = "avíeis", ["3"] = "aviam" };
          sing = { ["1"] = "avia", ["2"] = "avias", ["3"] = "avia" };
        };
        plpf =
        {
          plur = { ["1"] = "avêramos", ["2"] = "avêreis", ["3"] = "averam" };
          sing = { ["1"] = "avera", ["2"] = "averas", ["3"] = "avera" };
        };
        pres =
        {
          plur = { ["1"] = "avemos", ["2"] = "aveis" };
        };
        pret =
        {
          plur = { ["1"] = "avemos", ["2"] = "avestes", ["3"] = "averam" };
          sing = { ["1"] = "avi", ["2"] = "aveste", ["3"] = "aveu" };
        };
      };
      infn =
      {
        impe = "aver";
        pers =
        {
          plur = { ["1"] = "avermos", ["2"] = "averdes", ["3"] = "averem" };
          sing = { ["1"] = "aver", ["2"] = "averes", ["3"] = "aver" };
        };
      };
      part_past =
      {
        plur = { f = "avidas", m = "avidos" };
        sing = { f = "avida", m = "avido" };
      };
      part_pres = "avendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "avermos", ["2"] = "averdes", ["3"] = "averem" };
          sing = { ["1"] = "aver", ["2"] = "averes", ["3"] = "aver" };
        };
        impf =
        {
          plur = { ["1"] = "avêssemos", ["2"] = "avêsseis", ["3"] = "avessem" };
          sing = { ["1"] = "avesse", ["2"] = "avesses", ["3"] = "avesse" };
        };
      };
    };
    suffix = "er";
    verb = "aver";
  };
  caber =
  {
    comments = { };
    examples = { "caber" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "caibamos", ["2"] = "cabei", ["3"] = "caibam" };
          sing = { ["1"] = "caiba", ["2"] = "cabe", ["3"] = "caiba" };
        };
        negt =
        {
          plur = { ["1"] = "caibamos", ["2"] = "caibais", ["3"] = "caibam" };
          sing = { ["1"] = "caiba", ["2"] = "caibas", ["3"] = "caiba" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "caberíamos", ["2"] = "caberíeis", ["3"] = "caberiam" };
          sing = { ["1"] = "caberia", ["2"] = "caberias", ["3"] = "caberia" };
        };
        futu =
        {
          plur = { ["1"] = "caberemos", ["2"] = "cabereis", ["3"] = "caberão" };
          sing = { ["1"] = "caberei", ["2"] = "caberás", ["3"] = "caberá" };
        };
        impf =
        {
          plur = { ["1"] = "cabíamos", ["2"] = "cabíeis", ["3"] = "cabiam" };
          sing = { ["1"] = "cabia", ["2"] = "cabias", ["3"] = "cabia" };
        };
        plpf =
        {
          plur = { ["1"] = "coubéramos", ["2"] = "coubéreis", ["3"] = "couberam" };
          sing = { ["1"] = "coubera", ["2"] = "couberas", ["3"] = "coubera" };
        };
        pres =
        {
          plur = { ["1"] = "cabemos", ["2"] = "cabeis", ["3"] = "cabem" };
          sing = { ["1"] = "caibo", ["2"] = "cabes", ["3"] = "cabe" };
        };
        pret =
        {
          plur = { ["1"] = "coubemos", ["2"] = "coubestes", ["3"] = "couberam" };
          sing = { ["1"] = "coube", ["2"] = "coubeste", ["3"] = "coube" };
        };
      };
      infn =
      {
        impe = "caber";
        pers =
        {
          plur = { ["1"] = "cabermos", ["2"] = "caberdes", ["3"] = "caberem" };
          sing = { ["1"] = "caber", ["2"] = "caberes", ["3"] = "caber" };
        };
      };
      part_past =
      {
        plur = { f = "cabidas", m = "cabidos" };
        sing = { f = "cabida", m = "cabido" };
      };
      part_pres = "cabendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "coubermos", ["2"] = "couberdes", ["3"] = "couberem" };
          sing = { ["1"] = "couber", ["2"] = "couberes", ["3"] = "couber" };
        };
        impf =
        {
          plur = { ["1"] = "coubéssemos", ["2"] = "coubésseis", ["3"] = "coubessem" };
          sing = { ["1"] = "coubesse", ["2"] = "coubesses", ["3"] = "coubesse" };
        };
        pres =
        {
          plur = { ["1"] = "caibamos", ["2"] = "caibais", ["3"] = "caibam" };
          sing = { ["1"] = "caiba", ["2"] = "caibas", ["3"] = "caiba" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "caber";
  };
  car =
  {
    comments =
    {
      "The last letter ''c'' is changed to ''qu'', depending on the following vowel, resulting in these three possible sequences: -ca-, -que- and -co-.";
    };
    examples = { "brincar", "buscar", "pecar", "trancar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "quemos", ["2"] = "cai", ["3"] = "quem" };
          sing = { ["1"] = "que", ["2"] = "ca", ["3"] = "que" };
        };
        negt =
        {
          plur = { ["1"] = "quemos", ["2"] = "queis", ["3"] = "quem" };
          sing = { ["1"] = "que", ["2"] = "ques", ["3"] = "que" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "caríamos", ["2"] = "caríeis", ["3"] = "cariam" };
          sing = { ["1"] = "caria", ["2"] = "carias", ["3"] = "caria" };
        };
        futu =
        {
          plur = { ["1"] = "caremos", ["2"] = "careis", ["3"] = "carão" };
          sing = { ["1"] = "carei", ["2"] = "carás", ["3"] = "cará" };
        };
        impf =
        {
          plur = { ["1"] = "cávamos", ["2"] = "cáveis", ["3"] = "cavam" };
          sing = { ["1"] = "cava", ["2"] = "cavas", ["3"] = "cava" };
        };
        plpf =
        {
          plur = { ["1"] = "cáramos", ["2"] = "cáreis", ["3"] = "caram" };
          sing = { ["1"] = "cara", ["2"] = "caras", ["3"] = "cara" };
        };
        pres =
        {
          plur = { ["1"] = "camos", ["2"] = "cais", ["3"] = "cam" };
          sing = { ["1"] = "co", ["2"] = "cas", ["3"] = "ca" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "camos";
            ["1_alt"] = { "cámos" };
            ["2"] = "castes";
            ["3"] = "caram";
          };
          sing = { ["1"] = "quei", ["2"] = "caste", ["3"] = "cou" };
        };
      };
      infn =
      {
        impe = "car";
        pers =
        {
          plur = { ["1"] = "carmos", ["2"] = "cardes", ["3"] = "carem" };
          sing = { ["1"] = "car", ["2"] = "cares", ["3"] = "car" };
        };
      };
      part_past =
      {
        plur = { f = "cadas", m = "cados" };
        sing = { f = "cada", m = "cado" };
      };
      part_pres = "cando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "carmos", ["2"] = "cardes", ["3"] = "carem" };
          sing = { ["1"] = "car", ["2"] = "cares", ["3"] = "car" };
        };
        impf =
        {
          plur = { ["1"] = "cássemos", ["2"] = "cásseis", ["3"] = "cassem" };
          sing = { ["1"] = "casse", ["2"] = "casses", ["3"] = "casse" };
        };
        pres =
        {
          plur = { ["1"] = "quemos", ["2"] = "queis", ["3"] = "quem" };
          sing = { ["1"] = "que", ["2"] = "ques", ["3"] = "que" };
        };
      };
    };
    suffix = "ar";
    verb = "car";
  };
  cer =
  {
    comments =
    {
      "The last letter ''c'' is changed to ''ç'', depending on the following vowel, resulting in these four possible sequences: -ça-, -ce-, -ci- and -ço-.";
    };
    examples =
    {
      "acontecer";
      "amanhecer";
      "carecer";
      "conhecer";
      "descer";
      "esquecer";
      "merecer";
      "parecer";
      "reconhecer";
      "tecer";
      "vencer";
    };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "çamos", ["2"] = "cei", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ce", ["3"] = "ça" };
        };
        negt =
        {
          plur = { ["1"] = "çamos", ["2"] = "çais", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ças", ["3"] = "ça" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ceríamos", ["2"] = "ceríeis", ["3"] = "ceriam" };
          sing = { ["1"] = "ceria", ["2"] = "cerias", ["3"] = "ceria" };
        };
        futu =
        {
          plur = { ["1"] = "ceremos", ["2"] = "cereis", ["3"] = "cerão" };
          sing = { ["1"] = "cerei", ["2"] = "cerás", ["3"] = "cerá" };
        };
        impf =
        {
          plur = { ["1"] = "cíamos", ["2"] = "cíeis", ["3"] = "ciam" };
          sing = { ["1"] = "cia", ["2"] = "cias", ["3"] = "cia" };
        };
        plpf =
        {
          plur = { ["1"] = "cêramos", ["2"] = "cêreis", ["3"] = "ceram" };
          sing = { ["1"] = "cera", ["2"] = "ceras", ["3"] = "cera" };
        };
        pres =
        {
          plur = { ["1"] = "cemos", ["2"] = "ceis", ["3"] = "cem" };
          sing = { ["1"] = "ço", ["2"] = "ces", ["3"] = "ce" };
        };
        pret =
        {
          plur = { ["1"] = "cemos", ["2"] = "cestes", ["3"] = "ceram" };
          sing = { ["1"] = "ci", ["2"] = "ceste", ["3"] = "ceu" };
        };
      };
      infn =
      {
        impe = "cer";
        pers =
        {
          plur = { ["1"] = "cermos", ["2"] = "cerdes", ["3"] = "cerem" };
          sing = { ["1"] = "cer", ["2"] = "ceres", ["3"] = "cer" };
        };
      };
      part_past =
      {
        plur = { f = "cidas", m = "cidos" };
        sing = { f = "cida", m = "cido" };
      };
      part_pres = "cendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "cermos", ["2"] = "cerdes", ["3"] = "cerem" };
          sing = { ["1"] = "cer", ["2"] = "ceres", ["3"] = "cer" };
        };
        impf =
        {
          plur = { ["1"] = "cêssemos", ["2"] = "cêsseis", ["3"] = "cessem" };
          sing = { ["1"] = "cesse", ["2"] = "cesses", ["3"] = "cesse" };
        };
        pres =
        {
          plur = { ["1"] = "çamos", ["2"] = "çais", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ças", ["3"] = "ça" };
        };
      };
    };
    suffix = "er";
    verb = "cer";
  };
  cir =
  {
    comments =
    {
      "The last letter ''c'' is changed to ''ç'', depending on the following vowel, resulting in these four possible sequences: -ça-, -ce-, -ci- and -ço-.";
    };
    examples = { "ressarcir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "çamos", ["2"] = "ci", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ce", ["3"] = "ça" };
        };
        negt =
        {
          plur = { ["1"] = "çamos", ["2"] = "çais", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ças", ["3"] = "ça" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ciríamos", ["2"] = "ciríeis", ["3"] = "ciriam" };
          sing = { ["1"] = "ciria", ["2"] = "cirias", ["3"] = "ciria" };
        };
        futu =
        {
          plur = { ["1"] = "ciremos", ["2"] = "cireis", ["3"] = "cirão" };
          sing = { ["1"] = "cirei", ["2"] = "cirás", ["3"] = "cirá" };
        };
        impf =
        {
          plur = { ["1"] = "cíamos", ["2"] = "cíeis", ["3"] = "ciam" };
          sing = { ["1"] = "cia", ["2"] = "cias", ["3"] = "cia" };
        };
        plpf =
        {
          plur = { ["1"] = "círamos", ["2"] = "círeis", ["3"] = "ciram" };
          sing = { ["1"] = "cira", ["2"] = "ciras", ["3"] = "cira" };
        };
        pres =
        {
          plur = { ["1"] = "cimos", ["2"] = "cis", ["3"] = "cem" };
          sing = { ["1"] = "ço", ["2"] = "ces", ["3"] = "ce" };
        };
        pret =
        {
          plur = { ["1"] = "cimos", ["2"] = "cistes", ["3"] = "ciram" };
          sing = { ["1"] = "ci", ["2"] = "ciste", ["3"] = "ciu" };
        };
      };
      infn =
      {
        impe = "cir";
        pers =
        {
          plur = { ["1"] = "cirmos", ["2"] = "cirdes", ["3"] = "cirem" };
          sing = { ["1"] = "cir", ["2"] = "cires", ["3"] = "cir" };
        };
      };
      part_past =
      {
        plur = { f = "cidas", m = "cidos" };
        sing = { f = "cida", m = "cido" };
      };
      part_pres = "cindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "cirmos", ["2"] = "cirdes", ["3"] = "cirem" };
          sing = { ["1"] = "cir", ["2"] = "cires", ["3"] = "cir" };
        };
        impf =
        {
          plur = { ["1"] = "císsemos", ["2"] = "císseis", ["3"] = "cissem" };
          sing = { ["1"] = "cisse", ["2"] = "cisses", ["3"] = "cisse" };
        };
        pres =
        {
          plur = { ["1"] = "çamos", ["2"] = "çais", ["3"] = "çam" };
          sing = { ["1"] = "ça", ["2"] = "ças", ["3"] = "ça" };
        };
      };
    };
    suffix = "ir";
    verb = "cir";
  };
  cobrir =
  {
    comments = { };
    examples = { "cobrir", "descobrir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "cubramos", ["2"] = "cobri", ["3"] = "cubram" };
          sing = { ["2"] = "cobre", ["3"] = "cubra" };
        };
        negt =
        {
          plur = { ["1"] = "cubramos", ["2"] = "cubrais", ["3"] = "cubram" };
          sing = { ["2"] = "cubras", ["3"] = "cubra" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "cobriríamos", ["2"] = "cobriríeis", ["3"] = "cobririam" };
          sing = { ["1"] = "cobriria", ["2"] = "cobririas", ["3"] = "cobriria" };
        };
        futu =
        {
          plur = { ["1"] = "cobriremos", ["2"] = "cobrireis", ["3"] = "cobrirão" };
          sing = { ["1"] = "cobrirei", ["2"] = "cobrirás", ["3"] = "cobrirá" };
        };
        impf =
        {
          plur = { ["1"] = "cobríamos", ["2"] = "cobríeis", ["3"] = "cobriam" };
          sing = { ["1"] = "cobria", ["2"] = "cobrias", ["3"] = "cobria" };
        };
        plpf =
        {
          plur = { ["1"] = "cobríramos", ["2"] = "cobríreis", ["3"] = "cobriram" };
          sing = { ["1"] = "cobrira", ["2"] = "cobriras", ["3"] = "cobrira" };
        };
        pres =
        {
          plur = { ["1"] = "cobrimos", ["2"] = "cobris", ["3"] = "cobrem" };
          sing = { ["1"] = "cubro", ["2"] = "cobres", ["3"] = "cobre" };
        };
        pret =
        {
          plur = { ["1"] = "cobrimos", ["2"] = "cobristes", ["3"] = "cobriram" };
          sing = { ["1"] = "cobri", ["2"] = "cobriste", ["3"] = "cobriu" };
        };
      };
      infn =
      {
        impe = "cobrir";
        pers =
        {
          plur = { ["1"] = "cobrirmos", ["2"] = "cobrirdes", ["3"] = "cobrirem" };
          sing = { ["1"] = "cobrir", ["2"] = "cobrires", ["3"] = "cobrir" };
        };
      };
      part_past =
      {
        plur = { f = "cobertas", m = "cobertos" };
        sing = { f = "coberta", m = "coberto" };
      };
      part_pres = "cobrindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "cobrirmos", ["2"] = "cobrirdes", ["3"] = "cobrirem" };
          sing = { ["1"] = "cobrir", ["2"] = "cobrires", ["3"] = "cobrir" };
        };
        impf =
        {
          plur = { ["1"] = "cobríssemos", ["2"] = "cobrísseis", ["3"] = "cobrissem" };
          sing = { ["1"] = "cobrisse", ["2"] = "cobrisses", ["3"] = "cobrisse" };
        };
        pres =
        {
          plur = { ["1"] = "cubramos", ["2"] = "cubrais", ["3"] = "cubram" };
          sing = { ["1"] = "cubra", ["2"] = "cubras", ["3"] = "cubra" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "cobrir";
  };
  colorir =
  {
    comments = { };
    defective = true;
    examples = { "colorir", "descolorir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "coloramos";
            ["2"] = "colori";
            ["3_defective"] = "coloram";
          };
          sing = { ["2_defective"] = "colore", ["3_defective"] = "colora" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "coloramos";
            ["2_defective"] = "colorais";
            ["3_defective"] = "coloram";
          };
          sing = { ["2_defective"] = "coloras", ["3_defective"] = "colora" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "coloriríamos";
            ["2"] = "coloriríeis";
            ["3"] = "coloririam";
          };
          sing = { ["1"] = "coloriria", ["2"] = "coloririas", ["3"] = "coloriria" };
        };
        futu =
        {
          plur = { ["1"] = "coloriremos", ["2"] = "colorireis", ["3"] = "colorirão" };
          sing = { ["1"] = "colorirei", ["2"] = "colorirás", ["3"] = "colorirá" };
        };
        impf =
        {
          plur = { ["1"] = "coloríamos", ["2"] = "coloríeis", ["3"] = "coloriam" };
          sing = { ["1"] = "coloria", ["2"] = "colorias", ["3"] = "coloria" };
        };
        plpf =
        {
          plur = { ["1"] = "coloríramos", ["2"] = "coloríreis", ["3"] = "coloriram" };
          sing = { ["1"] = "colorira", ["2"] = "coloriras", ["3"] = "colorira" };
        };
        pres =
        {
          plur = { ["1"] = "colorimos", ["2"] = "coloris", ["3_defective"] = "colorem" };
          sing =
          {
            ["1_defective"] = "coloro";
            ["2_defective"] = "colores";
            ["3_defective"] = "colore";
          };
        };
        pret =
        {
          plur = { ["1"] = "colorimos", ["2"] = "coloristes", ["3"] = "coloriram" };
          sing = { ["1"] = "colori", ["2"] = "coloriste", ["3"] = "coloriu" };
        };
      };
      infn =
      {
        impe = "colorir";
        pers =
        {
          plur = { ["1"] = "colorirmos", ["2"] = "colorirdes", ["3"] = "colorirem" };
          sing = { ["1"] = "colorir", ["2"] = "colorires", ["3"] = "colorir" };
        };
      };
      part_past =
      {
        plur = { f = "coloridas", m = "coloridos" };
        sing = { f = "colorida", m = "colorido" };
      };
      part_pres = "colorindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "colorirmos", ["2"] = "colorirdes", ["3"] = "colorirem" };
          sing = { ["1"] = "colorir", ["2"] = "colorires", ["3"] = "colorir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "coloríssemos";
            ["2"] = "colorísseis";
            ["3"] = "colorissem";
          };
          sing = { ["1"] = "colorisse", ["2"] = "colorisses", ["3"] = "colorisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "coloramos";
            ["2_defective"] = "colorais";
            ["3_defective"] = "coloram";
          };
          sing =
          {
            ["1_defective"] = "colora";
            ["2_defective"] = "coloras";
            ["3_defective"] = "colora";
          };
        };
      };
    };
    suffix = "ir";
    verb = "colorir";
  };
  completar =
  {
    comments = { };
    examples = { "completar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "completemos", ["2"] = "completai", ["3"] = "completem" };
          sing = { ["2"] = "completa", ["3"] = "complete" };
        };
        negt =
        {
          plur = { ["1"] = "completemos", ["2"] = "completeis", ["3"] = "completem" };
          sing = { ["2"] = "completes", ["3"] = "complete" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "completaríamos";
            ["2"] = "completaríeis";
            ["3"] = "completariam";
          };
          sing =
          {
            ["1"] = "completaria";
            ["2"] = "completarias";
            ["3"] = "completaria";
          };
        };
        futu =
        {
          plur =
          {
            ["1"] = "completaremos";
            ["2"] = "completareis";
            ["3"] = "completarão";
          };
          sing =
          {
            ["1"] = "completarei";
            ["2"] = "completarás";
            ["3"] = "completará";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "completávamos";
            ["2"] = "completáveis";
            ["3"] = "completavam";
          };
          sing = { ["1"] = "completava", ["2"] = "completavas", ["3"] = "completava" };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "completáramos";
            ["2"] = "completáreis";
            ["3"] = "completaram";
          };
          sing = { ["1"] = "completara", ["2"] = "completaras", ["3"] = "completara" };
        };
        pres =
        {
          plur = { ["1"] = "completamos", ["2"] = "completais", ["3"] = "completam" };
          sing = { ["1"] = "completo", ["2"] = "completas", ["3"] = "completa" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "completamos";
            ["1_alt"] = { "completámos" };
            ["2"] = "completastes";
            ["3"] = "completaram";
          };
          sing = { ["1"] = "completei", ["2"] = "completaste", ["3"] = "completou" };
        };
      };
      infn =
      {
        impe = "completar";
        pers =
        {
          plur =
          {
            ["1"] = "completarmos";
            ["2"] = "completardes";
            ["3"] = "completarem";
          };
          sing = { ["1"] = "completar", ["2"] = "completares", ["3"] = "completar" };
        };
      };
      long_part_past =
      {
        plur = { f = "completadas", m = "completados" };
        sing = { f = "completada", m = "completado" };
      };
      part_pres = "completando";
      short_part_past =
      {
        plur = { f = "completas", m = "completos" };
        sing = { f = "completa", m = "completo" };
      };
      subj =
      {
        futu =
        {
          plur =
          {
            ["1"] = "completarmos";
            ["2"] = "completardes";
            ["3"] = "completarem";
          };
          sing = { ["1"] = "completar", ["2"] = "completares", ["3"] = "completar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "completássemos";
            ["2"] = "completásseis";
            ["3"] = "completassem";
          };
          sing = { ["1"] = "completasse", ["2"] = "completasses", ["3"] = "asse" };
        };
        pres =
        {
          plur = { ["1"] = "completemos", ["2"] = "completeis", ["3"] = "completem" };
          sing = { ["1"] = "complete", ["2"] = "completes", ["3"] = "complete" };
        };
      };
    };
    suffix = "ar";
    verb = "completar";
  };
  crer =
  {
    comments = { };
    examples = { "crer", "descrer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "creiamos", ["2"] = "crede", ["3"] = "creiam" };
          sing = { ["1"] = "creia", ["2"] = "crê", ["3"] = "creia" };
        };
        negt =
        {
          plur = { ["1"] = "creiamos", ["2"] = "creiais", ["3"] = "creiam" };
          sing = { ["1"] = "creia", ["2"] = "creias", ["3"] = "creia" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "creríamos", ["2"] = "creríeis", ["3"] = "creriam" };
          sing = { ["1"] = "creria", ["2"] = "crerias", ["3"] = "creria" };
        };
        futu =
        {
          plur = { ["1"] = "creremos", ["2"] = "crereis", ["3"] = "crerão" };
          sing = { ["1"] = "crerei", ["2"] = "crerás", ["3"] = "crerá" };
        };
        impf =
        {
          plur = { ["1"] = "críamos", ["2"] = "críeis", ["3"] = "criam" };
          sing = { ["1"] = "cria", ["2"] = "crias", ["3"] = "cria" };
        };
        plpf =
        {
          plur = { ["1"] = "crêramos", ["2"] = "crêreis", ["3"] = "creram" };
          sing = { ["1"] = "crera", ["2"] = "creras", ["3"] = "crera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "cremos";
            ["2"] = "credes";
            ["3"] = "creem";
            ["3_obsolete"] = "crêem";
          };
          sing = { ["1"] = "creio", ["2"] = "crês", ["3"] = "crê" };
        };
        pret =
        {
          plur = { ["1"] = "cremos", ["2"] = "crestes", ["3"] = "creram" };
          sing = { ["1"] = "cri", ["2"] = "creste", ["3"] = "creu" };
        };
      };
      infn =
      {
        impe = "crer";
        pers =
        {
          plur = { ["1"] = "crermos", ["2"] = "crerdes", ["3"] = "crerem" };
          sing = { ["1"] = "crer", ["2"] = "creres", ["3"] = "crer" };
        };
      };
      part_past =
      {
        plur = { f = "cridas", m = "cridos" };
        sing = { f = "crida", m = "crido" };
      };
      part_pres = "crendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "crermos", ["2"] = "crerdes", ["3"] = "crerem" };
          sing = { ["1"] = "crer", ["2"] = "creres", ["3"] = "crer" };
        };
        impf =
        {
          plur = { ["1"] = "crêssemos", ["2"] = "crêsseis", ["3"] = "cressem" };
          sing = { ["1"] = "cresse", ["2"] = "cresses", ["3"] = "cresse" };
        };
        pres =
        {
          plur = { ["1"] = "creiamos", ["2"] = "creiais", ["3"] = "creiam" };
          sing = { ["1"] = "creia", ["2"] = "creias", ["3"] = "creia" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "crer";
  };
  ["çar"] =
  {
    comments =
    {
      "The last letter ''ç'' is changed to ''c'', depending on the following vowel, resulting in these three possible sequences: -ça-, -ce- and -ço-.";
    };
    examples = { "alcançar", "começar", "laçar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "cemos", ["2"] = "çai", ["3"] = "cem" };
          sing = { ["1"] = "ce", ["2"] = "ça", ["3"] = "ce" };
        };
        negt =
        {
          plur = { ["1"] = "cemos", ["2"] = "ceis", ["3"] = "cem" };
          sing = { ["1"] = "ce", ["2"] = "ces", ["3"] = "ce" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "çaríamos", ["2"] = "çaríeis", ["3"] = "çariam" };
          sing = { ["1"] = "çaria", ["2"] = "çarias", ["3"] = "çaria" };
        };
        futu =
        {
          plur = { ["1"] = "çaremos", ["2"] = "çareis", ["3"] = "çarão" };
          sing = { ["1"] = "çarei", ["2"] = "çarás", ["3"] = "çará" };
        };
        impf =
        {
          plur = { ["1"] = "çávamos", ["2"] = "çáveis", ["3"] = "çavam" };
          sing = { ["1"] = "çava", ["2"] = "çavas", ["3"] = "çava" };
        };
        plpf =
        {
          plur = { ["1"] = "çáramos", ["2"] = "çáreis", ["3"] = "çaram" };
          sing = { ["1"] = "çara", ["2"] = "çaras", ["3"] = "çara" };
        };
        pres =
        {
          plur = { ["1"] = "çamos", ["2"] = "çais", ["3"] = "çam" };
          sing = { ["1"] = "ço", ["2"] = "ças", ["3"] = "ça" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "çamos";
            ["1_alt"] = { "çámos" };
            ["2"] = "çastes";
            ["3"] = "çaram";
          };
          sing = { ["1"] = "cei", ["2"] = "çaste", ["3"] = "çou" };
        };
      };
      infn =
      {
        impe = "çar";
        pers =
        {
          plur = { ["1"] = "çarmos", ["2"] = "çardes", ["3"] = "çarem" };
          sing = { ["1"] = "çar", ["2"] = "çares", ["3"] = "çar" };
        };
      };
      part_past =
      {
        plur = { f = "çadas", m = "çados" };
        sing = { f = "çada", m = "çado" };
      };
      part_pres = "çando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "çarmos", ["2"] = "çardes", ["3"] = "çarem" };
          sing = { ["1"] = "çar", ["2"] = "çares", ["3"] = "çar" };
        };
        impf =
        {
          plur = { ["1"] = "çássemos", ["2"] = "çásseis", ["3"] = "çassem" };
          sing = { ["1"] = "çasse", ["2"] = "çasses", ["3"] = "çasse" };
        };
        pres =
        {
          plur = { ["1"] = "cemos", ["2"] = "ceis", ["3"] = "cem" };
          sing = { ["1"] = "ce", ["2"] = "ces", ["3"] = "ce" };
        };
      };
    };
    suffix = "ar";
    verb = "çar";
  };
  dar =
  {
    comments = { };
    examples = { "dar", "desdar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "demos", ["2"] = "dai", ["3"] = "dêem" };
          sing = { ["1"] = "dê", ["2"] = "dá", ["3"] = "dê" };
        };
        negt =
        {
          plur = { ["1"] = "demos", ["2"] = "deis", ["3"] = "dêem" };
          sing = { ["1"] = "dê", ["2"] = "dês", ["3"] = "dê" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "daríamos", ["2"] = "daríeis", ["3"] = "dariam" };
          sing = { ["1"] = "daria", ["2"] = "darias", ["3"] = "daria" };
        };
        futu =
        {
          plur = { ["1"] = "daremos", ["2"] = "dareis", ["3"] = "darão" };
          sing = { ["1"] = "darei", ["2"] = "darás", ["3"] = "dará" };
        };
        impf =
        {
          plur = { ["1"] = "dávamos", ["2"] = "dáveis", ["3"] = "davam" };
          sing = { ["1"] = "dava", ["2"] = "davas", ["3"] = "dava" };
        };
        plpf =
        {
          plur = { ["1"] = "déramos", ["2"] = "déreis", ["3"] = "deram" };
          sing = { ["1"] = "dera", ["2"] = "deras", ["3"] = "dera" };
        };
        pres =
        {
          plur = { ["1"] = "damos", ["2"] = "dais", ["3"] = "dão" };
          sing = { ["1"] = "dou", ["2"] = "dás", ["3"] = "dá" };
        };
        pret =
        {
          plur = { ["1"] = "demos", ["2"] = "destes", ["3"] = "deram" };
          sing = { ["1"] = "dei", ["2"] = "deste", ["3"] = "deu" };
        };
      };
      infn =
      {
        impe = "dar";
        pers =
        {
          plur = { ["1"] = "dermos", ["2"] = "derdes", ["3"] = "derem" };
          sing = { ["1"] = "der", ["2"] = "deres", ["3"] = "der" };
        };
      };
      part_past =
      {
        plur = { f = "dadas", m = "dados" };
        sing = { f = "dada", m = "dado" };
      };
      part_pres = "dando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "dermos", ["2"] = "derdes", ["3"] = "derem" };
          sing = { ["1"] = "der", ["2"] = "deres", ["3"] = "der" };
        };
        impf =
        {
          plur = { ["1"] = "déssemos", ["2"] = "désseis", ["3"] = "dessem" };
          sing = { ["1"] = "desse", ["2"] = "desses", ["3"] = "desse" };
        };
        pres =
        {
          plur = { ["1"] = "demos", ["2"] = "deis", ["3"] = "dêem" };
          sing = { ["1"] = "dê", ["2"] = "dês", ["3"] = "dê" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "dar";
  };
  delinquir =
  {
    comments =
    {
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced.";
    };
    examples = { "delinquir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "delinquamos";
            ["2"] = "delinqui";
            ["2_obsolete"] = "delinqüi";
            ["3"] = "delinquam";
          };
          sing =
          {
            ["1"] = "delinqua";
            ["2"] = "delinque";
            ["2_obsolete"] = "delinqüe";
            ["3"] = "delinqua";
          };
        };
        negt =
        {
          plur = { ["1"] = "delinquamos", ["2"] = "delinquais", ["3"] = "delinquam" };
          sing = { ["1"] = "delinqua", ["2"] = "delinquas", ["3"] = "delinqua" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "delinquiríamos";
            ["1_obsolete"] = "delinqüiríamos";
            ["2"] = "delinquiríeis";
            ["2_obsolete"] = "delinqüiríeis";
            ["3"] = "delinquiriam";
            ["3_obsolete"] = "delinqüiriam";
          };
          sing =
          {
            ["1"] = "delinquiria";
            ["1_obsolete"] = "delinqüiria";
            ["2"] = "delinquirias";
            ["2_obsolete"] = "delinqüirias";
            ["3"] = "delinquiria";
            ["3_obsolete"] = "delinqüiria";
          };
        };
        futu =
        {
          plur =
          {
            ["1"] = "delinquiremos";
            ["1_obsolete"] = "delinqüiremos";
            ["2"] = "delinquireis";
            ["2_obsolete"] = "delinqüireis";
            ["3"] = "delinquirão";
            ["3_obsolete"] = "delinqüirão";
          };
          sing =
          {
            ["1"] = "delinquirei";
            ["1_obsolete"] = "delinqüirei";
            ["2"] = "delinquirás";
            ["2_obsolete"] = "delinqüirás";
            ["3"] = "delinquirá";
            ["3_obsolete"] = "delinqüirá";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "delinquíamos";
            ["1_obsolete"] = "delinqüíamos";
            ["2"] = "delinquíeis";
            ["2_obsolete"] = "delinqüíeis";
            ["3"] = "delinquiam";
            ["3_obsolete"] = "delinqüiam";
          };
          sing =
          {
            ["1"] = "delinquia";
            ["1_obsolete"] = "delinqüia";
            ["2"] = "delinquias";
            ["2_obsolete"] = "delinqüias";
            ["3"] = "delinquia";
            ["3_obsolete"] = "delinqüia";
          };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "delinquíramos";
            ["1_obsolete"] = "delinqüíramos";
            ["2"] = "delinquíreis";
            ["2_obsolete"] = "delinqüíreis";
            ["3"] = "delinquiram";
            ["3_obsolete"] = "delinqüiram";
          };
          sing =
          {
            ["1"] = "delinquira";
            ["1_obsolete"] = "delinqüira";
            ["2"] = "delinquiras";
            ["2_obsolete"] = "delinqüiras";
            ["3"] = "delinquira";
            ["3_obsolete"] = "delinqüira";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "delinquimos";
            ["1_obsolete"] = "delinqüimos";
            ["2"] = "delinquis";
            ["2_obsolete"] = "delinqüis";
            ["3"] = "delinquem";
            ["3_obsolete"] = "delinqüem";
          };
          sing =
          {
            ["1"] = "delinquo";
            ["2"] = "delinques";
            ["2_obsolete"] = "delinqües";
            ["3"] = "delinque";
            ["3_obsolete"] = "delinqüe";
          };
        };
        pret =
        {
          plur =
          {
            ["1"] = "delinquimos";
            ["1_obsolete"] = "delinqüimos";
            ["2"] = "delinquistes";
            ["2_obsolete"] = "delinqüistes";
            ["3"] = "delinquiram";
            ["3_obsolete"] = "delinqüiram";
          };
          sing =
          {
            ["1"] = "delinqui";
            ["1_obsolete"] = "delinqüi";
            ["2"] = "delinquiste";
            ["2_obsolete"] = "delinqüiste";
            ["3"] = "delinquiu";
            ["3_obsolete"] = "delinqüiu";
          };
        };
      };
      infn =
      {
        impe = "delinqüir";
        pers =
        {
          plur =
          {
            ["1"] = "delinquirmos";
            ["1_obsolete"] = "delinqüirmos";
            ["2"] = "delinquirdes";
            ["2_obsolete"] = "delinqüirdes";
            ["3"] = "delinquirem";
            ["3_obsolete"] = "delinqüirem";
          };
          sing =
          {
            ["1"] = "delinquir";
            ["1_obsolete"] = "delinqüir";
            ["2"] = "delinquires";
            ["2_obsolete"] = "delinqüires";
            ["3"] = "delinquir";
            ["3_obsolete"] = "delinqüir";
          };
        };
      };
      part_past =
      {
        plur = { f = "delinqüidas", m = "delinqüidos" };
        sing = { f = "delinqüida", m = "delinqüido" };
      };
      part_pres = "delinqüindo";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1"] = "delinquirmos";
            ["1_obsolete"] = "delinqüirmos";
            ["2"] = "delinquirdes";
            ["2_obsolete"] = "delinqüirdes";
            ["3"] = "delinquirem";
            ["3_obsolete"] = "delinqüirem";
          };
          sing =
          {
            ["1"] = "delinquir";
            ["1_obsolete"] = "delinqüir";
            ["2"] = "delinquires";
            ["2_obsolete"] = "delinqüires";
            ["3"] = "delinquir";
            ["3_obsolete"] = "delinqüir";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "delinquíssemos";
            ["1_obsolete"] = "delinqüíssemos";
            ["2"] = "delinquísseis";
            ["2_obsolete"] = "delinqüísseis";
            ["3"] = "delinquissem";
            ["3_obsolete"] = "delinqüissem";
          };
          sing =
          {
            ["1"] = "delinquisse";
            ["1_obsolete"] = "delinqüisse";
            ["2"] = "delinquisses";
            ["2_obsolete"] = "delinqüisses";
            ["3"] = "delinquisse";
            ["3_obsolete"] = "delinqüisse";
          };
        };
        pres =
        {
          plur = { ["1"] = "delinquamos", ["2"] = "delinquais", ["3"] = "delinquam" };
          sing = { ["1"] = "delinqua", ["2"] = "delinquas", ["3"] = "delinqua" };
        };
      };
    };
    suffix = "ir";
    verb = "delinquir";
  };
  delir =
  {
    comments = { };
    defective = true;
    examples = { "delir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "deli" };
          sing = { ["2"] = "dele" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "deliríamos", ["2"] = "deliríeis", ["3"] = "deliriam" };
          sing = { ["1"] = "deliria", ["2"] = "delirias", ["3"] = "deliria" };
        };
        futu =
        {
          plur = { ["1"] = "deliremos", ["2"] = "delireis", ["3"] = "delirão" };
          sing = { ["1"] = "delirei", ["2"] = "delirás", ["3"] = "delirá" };
        };
        impf =
        {
          plur = { ["1"] = "delíamos", ["2"] = "delíeis", ["3"] = "deliam" };
          sing = { ["1"] = "delia", ["2"] = "delias", ["3"] = "delia" };
        };
        plpf =
        {
          plur = { ["1"] = "delíramos", ["2"] = "delíreis", ["3"] = "deliram" };
          sing = { ["1"] = "delira", ["2"] = "deliras", ["3"] = "delira" };
        };
        pres =
        {
          plur = { ["1"] = "delimos", ["2"] = "delis", ["3"] = "delem" };
          sing = { ["2"] = "deles", ["3"] = "dele" };
        };
        pret =
        {
          plur = { ["1"] = "delimos", ["2"] = "delistes", ["3"] = "deliram" };
          sing = { ["1"] = "deli", ["2"] = "deliste", ["3"] = "deliu" };
        };
      };
      infn =
      {
        impe = "delir";
        pers =
        {
          plur = { ["1"] = "delirmos", ["2"] = "delirdes", ["3"] = "delirem" };
          sing = { ["1"] = "delir", ["2"] = "delires", ["3"] = "delir" };
        };
      };
      part_past =
      {
        plur = { f = "delidas", m = "delidos" };
        sing = { f = "delida", m = "delido" };
      };
      part_pres = "delindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "delirmos", ["2"] = "delirdes", ["3"] = "delirem" };
          sing = { ["1"] = "delir", ["2"] = "delires", ["3"] = "delir" };
        };
        impf =
        {
          plur = { ["1"] = "delíssemos", ["2"] = "delísseis", ["3"] = "delissem" };
          sing = { ["1"] = "delisse", ["2"] = "delisses", ["3"] = "delisse" };
        };
      };
    };
    suffix = "ir";
    verb = "delir";
  };
  despir =
  {
    comments = { };
    defective = true;
    examples = { "despir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "despi" };
          sing = { ["2"] = "despe" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "despiríamos", ["2"] = "despiríeis", ["3"] = "despiriam" };
          sing = { ["1"] = "despiria", ["2"] = "despirias", ["3"] = "despiria" };
        };
        futu =
        {
          plur = { ["1"] = "despiremos", ["2"] = "despireis", ["3"] = "despirão" };
          sing = { ["1"] = "despirei", ["2"] = "despirás", ["3"] = "despirá" };
        };
        impf =
        {
          plur = { ["1"] = "despíamos", ["2"] = "despíeis", ["3"] = "despiam" };
          sing = { ["1"] = "despia", ["2"] = "despias", ["3"] = "despia" };
        };
        plpf =
        {
          plur = { ["1"] = "despíramos", ["2"] = "despíreis", ["3"] = "despiram" };
          sing = { ["1"] = "despira", ["2"] = "despiras", ["3"] = "despira" };
        };
        pres =
        {
          plur = { ["1"] = "despimos", ["2"] = "despis", ["3"] = "despem" };
          sing = { ["2"] = "despes", ["3"] = "despe" };
        };
        pret =
        {
          plur = { ["1"] = "despimos", ["2"] = "despistes", ["3"] = "despiram" };
          sing = { ["1"] = "despi", ["2"] = "despiste", ["3"] = "despiu" };
        };
      };
      infn =
      {
        impe = "despir";
        pers =
        {
          plur = { ["1"] = "despirmos", ["2"] = "despirdes", ["3"] = "despirem" };
          sing = { ["1"] = "despir", ["2"] = "despires", ["3"] = "despir" };
        };
      };
      part_past =
      {
        plur = { f = "despidas", m = "despidos" };
        sing = { f = "despida", m = "despido" };
      };
      part_pres = "despindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "despirmos", ["2"] = "despirdes", ["3"] = "despirem" };
          sing = { ["1"] = "despir", ["2"] = "despires", ["3"] = "despir" };
        };
        impf =
        {
          plur = { ["1"] = "despíssemos", ["2"] = "despísseis", ["3"] = "despissem" };
          sing = { ["1"] = "despisse", ["2"] = "despisses", ["3"] = "despisse" };
        };
      };
    };
    suffix = "ir";
    verb = "despir";
  };
  divertir =
  {
    comments = { };
    examples = { "divertir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "divirtamos", ["2"] = "diverti", ["3"] = "divirtam" };
          sing = { ["2"] = "diverte", ["3"] = "divirta" };
        };
        negt =
        {
          plur = { ["1"] = "divirtamos", ["2"] = "divirtais", ["3"] = "divirtam" };
          sing = { ["2"] = "divirtas", ["3"] = "divirta" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "divertiríamos";
            ["2"] = "divertiríeis";
            ["3"] = "divertiriam";
          };
          sing = { ["1"] = "divertiria", ["2"] = "divertirias", ["3"] = "divertiria" };
        };
        futu =
        {
          plur =
          {
            ["1"] = "divertiremos";
            ["2"] = "divertireis";
            ["3"] = "divertirão";
          };
          sing = { ["1"] = "divertirei", ["2"] = "divertirás", ["3"] = "divertirá" };
        };
        impf =
        {
          plur = { ["1"] = "divertíamos", ["2"] = "divertíeis", ["3"] = "divertiam" };
          sing = { ["1"] = "divertia", ["2"] = "divertias", ["3"] = "divertia" };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "divertíramos";
            ["2"] = "divertíreis";
            ["3"] = "divertiram";
          };
          sing = { ["1"] = "divertira", ["2"] = "divertiras", ["3"] = "divertira" };
        };
        pres =
        {
          plur = { ["1"] = "divertimos", ["2"] = "divertis", ["3"] = "divertem" };
          sing = { ["1"] = "divirto", ["2"] = "divertes", ["3"] = "diverte" };
        };
        pret =
        {
          plur = { ["1"] = "divertimos", ["2"] = "divertistes", ["3"] = "divertiram" };
          sing = { ["1"] = "diverti", ["2"] = "divertiste", ["3"] = "divertiu" };
        };
      };
      infn =
      {
        impe = "divertir";
        pers =
        {
          plur = { ["1"] = "divertirmos", ["2"] = "divertirdes", ["3"] = "divertirem" };
          sing = { ["1"] = "divertir", ["2"] = "divertires", ["3"] = "divertir" };
        };
      };
      part_past =
      {
        plur = { f = "divertidas", m = "divertidos" };
        sing = { f = "divertida", m = "divertido" };
      };
      part_pres = "divertindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "divertirmos", ["2"] = "divertirdes", ["3"] = "divertirem" };
          sing = { ["1"] = "divertir", ["2"] = "divertires", ["3"] = "divertir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "divertíssemos";
            ["2"] = "divertísseis";
            ["3"] = "divertissem";
          };
          sing = { ["1"] = "divertisse", ["2"] = "divertisses", ["3"] = "divertisse" };
        };
        pres =
        {
          plur = { ["1"] = "divirtamos", ["2"] = "divirtais", ["3"] = "divirtam" };
          sing = { ["1"] = "divirta", ["2"] = "divirtas", ["3"] = "divirta" };
        };
      };
    };
    suffix = "ir";
    verb = "divertir";
  };
  dizer =
  {
    abundant = true;
    comments = { };
    examples =
    {
      "bendizer";
      "condizer";
      "contradizer";
      "desdizer";
      "dizer";
      "maldizer";
      "predizer";
    };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "dizamos", ["2"] = "dizei", ["3"] = "digam" };
          sing =
          {
            ["1"] = "diga";
            ["2"] = "dize";
            ["2_alt"] = { "diz" };
            ["3"] = "diga";
          };
        };
        negt =
        {
          plur = { ["1"] = "digamos", ["2"] = "digais", ["3"] = "digam" };
          sing = { ["1"] = "diga", ["2"] = "digas", ["3"] = "diga" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "diríamos", ["2"] = "diríeis", ["3"] = "diriam" };
          sing = { ["1"] = "diria", ["2"] = "dirias", ["3"] = "diria" };
        };
        futu =
        {
          plur = { ["1"] = "diremos", ["2"] = "direis", ["3"] = "dirão" };
          sing = { ["1"] = "direi", ["2"] = "dirás", ["3"] = "dirá" };
        };
        impf =
        {
          plur = { ["1"] = "dizíamos", ["2"] = "dizíeis", ["3"] = "diziam" };
          sing = { ["1"] = "dizia", ["2"] = "dizias", ["3"] = "dizia" };
        };
        plpf =
        {
          plur = { ["1"] = "disséramos", ["2"] = "disséreis", ["3"] = "disseram" };
          sing = { ["1"] = "dissera", ["2"] = "disseras", ["3"] = "dissera" };
        };
        pres =
        {
          plur = { ["1"] = "dizemos", ["2"] = "dizeis", ["3"] = "dizem" };
          sing = { ["1"] = "digo", ["2"] = "dizes", ["3"] = "diz" };
        };
        pret =
        {
          plur = { ["1"] = "dissemos", ["2"] = "dissestes", ["3"] = "disseram" };
          sing = { ["1"] = "disse", ["2"] = "disseste", ["3"] = "disse" };
        };
      };
      infn =
      {
        impe = "dizer";
        pers =
        {
          plur = { ["1"] = "dizermos", ["2"] = "dizerdes", ["3"] = "dizerem" };
          sing = { ["1"] = "dizer", ["2"] = "dizeres", ["3"] = "dizer" };
        };
      };
      part_past =
      {
        plur = { f = "ditas", m = "ditos" };
        sing = { f = "dita", m = "dito" };
      };
      part_pres = "dizendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "dissermos", ["2"] = "disserdes", ["3"] = "disserem" };
          sing = { ["1"] = "disser", ["2"] = "disseres", ["3"] = "disser" };
        };
        impf =
        {
          plur = { ["1"] = "disséssemos", ["2"] = "dissésseis", ["3"] = "dissessem" };
          sing = { ["1"] = "dissesse", ["2"] = "dissesses", ["3"] = "dissesse" };
        };
        pres =
        {
          plur = { ["1"] = "digamos", ["2"] = "digais", ["3"] = "digam" };
          sing = { ["1"] = "diga", ["2"] = "digas", ["3"] = "diga" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "dizer";
  };
  doer =
  {
    comments =
    {
      "An obsolete treatment is to add a circumflex in one conjugated form.";
      "An acute accent is added to few conjugated forms.";
    };
    defective = true;
    examples = { "doer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "doamos";
            ["2_defective"] = "doei";
            ["3"] = "doam";
          };
          sing = { ["2_defective"] = "dói", ["3_defective"] = "doa" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "doamos";
            ["2_defective"] = "doais";
            ["3"] = "doam";
          };
          sing = { ["2_defective"] = "doas", ["3_defective"] = "doa" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1_defective"] = "doeríamos";
            ["2_defective"] = "doeríeis";
            ["3"] = "doeriam";
          };
          sing =
          {
            ["1_defective"] = "doeria";
            ["2_defective"] = "doerias";
            ["3"] = "doeria";
          };
        };
        futu =
        {
          plur =
          {
            ["1_defective"] = "doeremos";
            ["2_defective"] = "doereis";
            ["3"] = "doerão";
          };
          sing =
          {
            ["1_defective"] = "doerei";
            ["2_defective"] = "doerás";
            ["3"] = "doerá";
          };
        };
        impf =
        {
          plur =
          {
            ["1_defective"] = "doíamos";
            ["2_defective"] = "doíeis";
            ["3"] = "doíam";
          };
          sing =
          {
            ["1_defective"] = "doía";
            ["2_defective"] = "doías";
            ["3"] = "doía";
          };
        };
        plpf =
        {
          plur =
          {
            ["1_defective"] = "doêramos";
            ["2_defective"] = "doêreis";
            ["3"] = "doeram";
          };
          sing =
          {
            ["1_defective"] = "doera";
            ["2_defective"] = "doeras";
            ["3"] = "doera";
          };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "doemos";
            ["2_defective"] = "doeis";
            ["3"] = "doem";
          };
          sing = { ["1_defective"] = "doo", ["2_defective"] = "dóis", ["3"] = "dói" };
        };
        pret =
        {
          plur =
          {
            ["1_defective"] = "doemos";
            ["2_defective"] = "doestes";
            ["3"] = "doeram";
          };
          sing =
          {
            ["1_defective"] = "doí";
            ["2_defective"] = "doeste";
            ["3"] = "doeu";
          };
        };
      };
      infn =
      {
        impe = "doer";
        pers =
        {
          plur =
          {
            ["1_defective"] = "doermos";
            ["2_defective"] = "doerdes";
            ["3"] = "doerem";
          };
          sing =
          {
            ["1_defective"] = "doer";
            ["2_defective"] = "doeres";
            ["3"] = "doer";
          };
        };
      };
      part_past =
      {
        plur = { f = "doídas", m = "doídos" };
        sing = { f = "doída", m = "doído" };
      };
      part_pres = "doendo";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1_defective"] = "doermos";
            ["2_defective"] = "doerdes";
            ["3"] = "doerem";
          };
          sing =
          {
            ["1_defective"] = "doer";
            ["2_defective"] = "doeres";
            ["3"] = "doer";
          };
        };
        impf =
        {
          plur =
          {
            ["1_defective"] = "doêssemos";
            ["2_defective"] = "doêsseis";
            ["3"] = "doessem";
          };
          sing =
          {
            ["1_defective"] = "doesse";
            ["2_defective"] = "doesses";
            ["3"] = "doesse";
          };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "doamos";
            ["2_defective"] = "doais";
            ["3"] = "doam";
          };
          sing = { ["1_defective"] = "doa", ["2_defective"] = "doas", ["3"] = "doa" };
        };
      };
    };
    suffix = "er";
    verb = "doer";
  };
  dormir =
  {
    abundant = true;
    comments = { };
    examples = { };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "durmamos", ["2"] = "durmi", ["3"] = "durmam" };
          sing = { ["2"] = "dorme", ["3"] = "durma" };
        };
        negt =
        {
          plur = { ["1"] = "durmamos", ["2"] = "durmais", ["3"] = "durmam" };
          sing = { ["2"] = "durmas", ["3"] = "durma" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "dormiríamos", ["2"] = "dormiríeis", ["3"] = "dormiriam" };
          sing = { ["1"] = "dormiria", ["2"] = "dormirias", ["3"] = "dormiria" };
        };
        futu =
        {
          plur = { ["1"] = "dormiremos", ["2"] = "dormireis", ["3"] = "dormirão" };
          sing = { ["1"] = "dormirei", ["2"] = "dormirás", ["3"] = "dormirá" };
        };
        impf =
        {
          plur = { ["1"] = "dormíamos", ["2"] = "dormíeis", ["3"] = "dormiam" };
          sing = { ["1"] = "dormia", ["2"] = "dormias", ["3"] = "dormia" };
        };
        plpf =
        {
          plur = { ["1"] = "dormíramos", ["2"] = "dormíreis", ["3"] = "dormiram" };
          sing = { ["1"] = "dormira", ["2"] = "dormiras", ["3"] = "dormira" };
        };
        pres =
        {
          plur = { ["1"] = "dormimos", ["2"] = "dormis", ["3"] = "dormem" };
          sing = { ["1"] = "durmo", ["2"] = "dormes", ["3"] = "dorme" };
        };
        pret =
        {
          plur = { ["1"] = "dormimos", ["2"] = "dormistes", ["3"] = "dormiram" };
          sing = { ["1"] = "dormi", ["2"] = "dormiste", ["3"] = "dormiu" };
        };
      };
      infn =
      {
        impe = "dormir";
        pers =
        {
          plur = { ["1"] = "dormirmos", ["2"] = "dormirdes", ["3"] = "dormirem" };
          sing = { ["1"] = "dormir", ["2"] = "dormires", ["3"] = "dormir" };
        };
      };
      part_past =
      {
        plur = { f = "dormidas", m = "dormidos" };
        sing = { f = "dormida", m = "dormido" };
      };
      part_pres = "dormindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "dormirmos", ["2"] = "dormirdes", ["3"] = "dormirem" };
          sing = { ["1"] = "dormir", ["2"] = "dormires", ["3"] = "dormir" };
        };
        impf =
        {
          plur = { ["1"] = "dormíssemos", ["2"] = "dormísseis", ["3"] = "dormissem" };
          sing = { ["1"] = "dormisse", ["2"] = "dormisses", ["3"] = "dormisse" };
        };
        pres =
        {
          plur = { ["1"] = "durmamos", ["2"] = "durmais", ["3"] = "durmam" };
          sing = { ["1"] = "durma", ["2"] = "durmas", ["3"] = "durma" };
        };
      };
    };
    suffix = "ir";
    verb = "dormir";
  };
  ear =
  {
    comments = { "The last letter ''e'' is changed to ''ei'' when stressed." };
    examples = { "frear", "nomear", "semear" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "eemos", ["2"] = "eai", ["3"] = "eiem" };
          sing = { ["1"] = "eie", ["2"] = "eia", ["3"] = "eie" };
        };
        negt =
        {
          plur = { ["1"] = "eemos", ["2"] = "eeis", ["3"] = "eiem" };
          sing = { ["1"] = "eie", ["2"] = "eies", ["3"] = "eie" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "earíamos", ["2"] = "earíeis", ["3"] = "eariam" };
          sing = { ["1"] = "earia", ["2"] = "earias", ["3"] = "earia" };
        };
        futu =
        {
          plur = { ["1"] = "earemos", ["2"] = "eareis", ["3"] = "earão" };
          sing = { ["1"] = "earei", ["2"] = "earás", ["3"] = "eará" };
        };
        impf =
        {
          plur = { ["1"] = "eávamos", ["2"] = "eáveis", ["3"] = "eavam" };
          sing = { ["1"] = "eava", ["2"] = "eavas", ["3"] = "eava" };
        };
        plpf =
        {
          plur = { ["1"] = "eáramos", ["2"] = "eáreis", ["3"] = "earam" };
          sing = { ["1"] = "eara", ["2"] = "earas", ["3"] = "eara" };
        };
        pres =
        {
          plur = { ["1"] = "eamos", ["2"] = "eais", ["3"] = "eiam" };
          sing = { ["1"] = "eio", ["2"] = "eias", ["3"] = "eia" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "eamos";
            ["1_alt"] = { "eámos" };
            ["2"] = "eastes";
            ["3"] = "earam";
          };
          sing = { ["1"] = "eei", ["2"] = "easte", ["3"] = "eou" };
        };
      };
      infn =
      {
        impe = "ear";
        pers =
        {
          plur = { ["1"] = "earmos", ["2"] = "eardes", ["3"] = "earem" };
          sing = { ["1"] = "ear", ["2"] = "eares", ["3"] = "ear" };
        };
      };
      part_past =
      {
        plur = { f = "eadas", m = "eados" };
        sing = { f = "eada", m = "eado" };
      };
      part_pres = "eando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "earmos", ["2"] = "eardes", ["3"] = "earem" };
          sing = { ["1"] = "ear", ["2"] = "eares", ["3"] = "ear" };
        };
        impf =
        {
          plur = { ["1"] = "eássemos", ["2"] = "eásseis", ["3"] = "eassem" };
          sing = { ["1"] = "easse", ["2"] = "easses", ["3"] = "easse" };
        };
        pres =
        {
          plur = { ["1"] = "eemos", ["2"] = "eeis", ["3"] = "eiem" };
          sing = { ["1"] = "eie", ["2"] = "eies", ["3"] = "eie" };
        };
      };
    };
    suffix = "ar";
    verb = "ear";
  };
  edir =
  {
    comments = { };
    examples =
    {
      "medir";
      "pedir";
      "despedir";
      "espedir";
      "expedir";
      "impedir";
      "desimpedir";
    };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "eçamos", ["2"] = "edi", ["3"] = "eçam" };
          sing = { ["2"] = "ede", ["3"] = "eça" };
        };
        negt =
        {
          plur = { ["1"] = "eçamos", ["2"] = "eçais", ["3"] = "eçam" };
          sing = { ["2"] = "eças", ["3"] = "eça" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ediríamos", ["2"] = "ediríeis", ["3"] = "ediriam" };
          sing = { ["1"] = "ediria", ["2"] = "edirias", ["3"] = "ediria" };
        };
        futu =
        {
          plur = { ["1"] = "ediremos", ["2"] = "edireis", ["3"] = "edirão" };
          sing = { ["1"] = "edirei", ["2"] = "edirás", ["3"] = "edirá" };
        };
        impf =
        {
          plur = { ["1"] = "edíamos", ["2"] = "edíeis", ["3"] = "ediam" };
          sing = { ["1"] = "edia", ["2"] = "edias", ["3"] = "edia" };
        };
        plpf =
        {
          plur = { ["1"] = "edíramos", ["2"] = "edíreis", ["3"] = "ediram" };
          sing = { ["1"] = "edira", ["2"] = "ediras", ["3"] = "edira" };
        };
        pres =
        {
          plur = { ["1"] = "edimos", ["2"] = "edis", ["3"] = "edem" };
          sing = { ["1"] = "eço", ["2"] = "edes", ["3"] = "ede" };
        };
        pret =
        {
          plur = { ["1"] = "edimos", ["2"] = "edistes", ["3"] = "ediram" };
          sing = { ["1"] = "edi", ["2"] = "ediste", ["3"] = "ediu" };
        };
      };
      infn =
      {
        impe = "edir";
        pers =
        {
          plur = { ["1"] = "edirmos", ["2"] = "edirdes", ["3"] = "edirem" };
          sing = { ["1"] = "edir", ["2"] = "edires", ["3"] = "edir" };
        };
      };
      part_past =
      {
        plur = { f = "edidas", m = "edidos" };
        sing = { f = "edida", m = "edido" };
      };
      part_pres = "edindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "edirmos", ["2"] = "edirdes", ["3"] = "edirem" };
          sing = { ["1"] = "edir", ["2"] = "edires", ["3"] = "edir" };
        };
        impf =
        {
          plur = { ["1"] = "edíssemos", ["2"] = "edísseis", ["3"] = "edissem" };
          sing = { ["1"] = "edisse", ["2"] = "edisses", ["3"] = "edisse" };
        };
        pres =
        {
          plur = { ["1"] = "eçamos", ["2"] = "eçais", ["3"] = "eçam" };
          sing = { ["1"] = "eça", ["2"] = "eças", ["3"] = "eça" };
        };
      };
    };
    suffix = "edir";
    verb = "edir";
  };
  engolir =
  {
    comments = { };
    examples = { "engolir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "engulamos", ["2"] = "engoli", ["3"] = "engulam" };
          sing = { ["2"] = "engole", ["3"] = "engula" };
        };
        negt =
        {
          plur = { ["1"] = "engulamos", ["2"] = "engulais", ["3"] = "engulam" };
          sing = { ["2"] = "engulas", ["3"] = "engula" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "engoliríamos";
            ["2"] = "engoliríeis";
            ["3"] = "engoliriam";
          };
          sing = { ["1"] = "engoliria", ["2"] = "engolirias", ["3"] = "engoliria" };
        };
        futu =
        {
          plur = { ["1"] = "engoliremos", ["2"] = "engolireis", ["3"] = "engolirão" };
          sing = { ["1"] = "engolirei", ["2"] = "engolirás", ["3"] = "engolirá" };
        };
        impf =
        {
          plur = { ["1"] = "engolíamos", ["2"] = "engolíeis", ["3"] = "engoliam" };
          sing = { ["1"] = "engolia", ["2"] = "engolias", ["3"] = "engolia" };
        };
        plpf =
        {
          plur = { ["1"] = "engolíramos", ["2"] = "engolíreis", ["3"] = "engoliram" };
          sing = { ["1"] = "engolira", ["2"] = "engoliras", ["3"] = "engolira" };
        };
        pres =
        {
          plur = { ["1"] = "engolimos", ["2"] = "engolis", ["3"] = "engolem" };
          sing = { ["1"] = "engulo", ["2"] = "engoles", ["3"] = "engole" };
        };
        pret =
        {
          plur = { ["1"] = "engolimos", ["2"] = "engolistes", ["3"] = "engoliram" };
          sing = { ["1"] = "engoli", ["2"] = "engoliste", ["3"] = "engoliu" };
        };
      };
      infn =
      {
        impe = "engolir";
        pers =
        {
          plur = { ["1"] = "engolirmos", ["2"] = "engolirdes", ["3"] = "engolirem" };
          sing = { ["1"] = "engolir", ["2"] = "engolires", ["3"] = "engolir" };
        };
      };
      part_past =
      {
        plur = { f = "engolidas", m = "engolidos" };
        sing = { f = "engolida", m = "engolido" };
      };
      part_pres = "engolindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "engolirmos", ["2"] = "engolirdes", ["3"] = "engolirem" };
          sing = { ["1"] = "engolir", ["2"] = "engolires", ["3"] = "engolir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "engolíssemos";
            ["2"] = "engolísseis";
            ["3"] = "engolissem";
          };
          sing = { ["1"] = "engolisse", ["2"] = "engolisses", ["3"] = "engolisse" };
        };
        pres =
        {
          plur = { ["1"] = "engulamos", ["2"] = "engulais", ["3"] = "engulam" };
          sing = { ["1"] = "engula", ["2"] = "engulas", ["3"] = "engula" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "engolir";
  };
  entir =
  {
    comments = { };
    examples = { "sentir", "mentir", "consentir", "ressentir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "intamos", ["2"] = "enti", ["3"] = "intam" };
          sing = { ["2"] = "ente", ["3"] = "inta" };
        };
        negt =
        {
          plur = { ["1"] = "intamos", ["2"] = "intais", ["3"] = "intam" };
          sing = { ["2"] = "intas", ["3"] = "inta" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "entiríamos", ["2"] = "entiríeis", ["3"] = "entiriam" };
          sing = { ["1"] = "entiria", ["2"] = "entirias", ["3"] = "entiria" };
        };
        futu =
        {
          plur = { ["1"] = "entiremos", ["2"] = "entireis", ["3"] = "entirão" };
          sing = { ["1"] = "entirei", ["2"] = "entirás", ["3"] = "entirá" };
        };
        impf =
        {
          plur = { ["1"] = "entíamos", ["2"] = "entíeis", ["3"] = "entiam" };
          sing = { ["1"] = "entia", ["2"] = "entias", ["3"] = "entia" };
        };
        plpf =
        {
          plur = { ["1"] = "entíramos", ["2"] = "entíreis", ["3"] = "entiram" };
          sing = { ["1"] = "entira", ["2"] = "entiras", ["3"] = "entira" };
        };
        pres =
        {
          plur = { ["1"] = "entimos", ["2"] = "entis", ["3"] = "entem" };
          sing = { ["1"] = "into", ["2"] = "entes", ["3"] = "ente" };
        };
        pret =
        {
          plur = { ["1"] = "entimos", ["2"] = "entistes", ["3"] = "entiram" };
          sing = { ["1"] = "enti", ["2"] = "entiste", ["3"] = "entiu" };
        };
      };
      infn =
      {
        impe = "entir";
        pers =
        {
          plur = { ["1"] = "entirmos", ["2"] = "entirdes", ["3"] = "entirem" };
          sing = { ["1"] = "entir", ["2"] = "entires", ["3"] = "entir" };
        };
      };
      part_past =
      {
        plur = { f = "entidas", m = "entidos" };
        sing = { f = "entida", m = "entido" };
      };
      part_pres = "entindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "entirmos", ["2"] = "entirdes", ["3"] = "entirem" };
          sing = { ["1"] = "entir", ["2"] = "entires", ["3"] = "entir" };
        };
        impf =
        {
          plur = { ["1"] = "entíssemos", ["2"] = "entísseis", ["3"] = "entissem" };
          sing = { ["1"] = "entisse", ["2"] = "entisses", ["3"] = "entisse" };
        };
        pres =
        {
          plur = { ["1"] = "intamos", ["2"] = "intais", ["3"] = "intam" };
          sing = { ["1"] = "inta", ["2"] = "intas", ["3"] = "inta" };
        };
      };
    };
    suffix = "entir";
    verb = "entir";
  };
  entregar =
  {
    abundant = true;
    comments =
    {
      "The last letter ''g'' is changed to ''gu'', depending on the following vowel, resulting in these three possible sequences: -ga-, -gue- and -go-.";
      "There are additional, shorter past participles.";
    };
    examples = { "entregar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "entreguemos", ["2"] = "entregai", ["3"] = "entreguem" };
          sing = { ["1"] = "entregue", ["2"] = "entrega", ["3"] = "entregue" };
        };
        negt =
        {
          plur = { ["1"] = "entreguemos", ["2"] = "entregueis", ["3"] = "entreguem" };
          sing = { ["1"] = "entregue", ["2"] = "entregues", ["3"] = "entregue" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "entregaríamos";
            ["2"] = "entregaríeis";
            ["3"] = "entregariam";
          };
          sing = { ["1"] = "entregaria", ["2"] = "entregarias", ["3"] = "entregaria" };
        };
        futu =
        {
          plur =
          {
            ["1"] = "entregaremos";
            ["2"] = "entregareis";
            ["3"] = "entregarão";
          };
          sing = { ["1"] = "entregarei", ["2"] = "entregarás", ["3"] = "entregará" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "entregávamos";
            ["2"] = "entregáveis";
            ["3"] = "entregavam";
          };
          sing = { ["1"] = "entregava", ["2"] = "entregavas", ["3"] = "entregava" };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "entregáramos";
            ["2"] = "entregáreis";
            ["3"] = "entregaram";
          };
          sing = { ["1"] = "entregara", ["2"] = "entregaras", ["3"] = "entregara" };
        };
        pres =
        {
          plur = { ["1"] = "entregamos", ["2"] = "entregais", ["3"] = "entregam" };
          sing = { ["1"] = "entrego", ["2"] = "entregas", ["3"] = "entrega" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "entregamos";
            ["1_alt"] = { "entregámos" };
            ["2"] = "entregastes";
            ["3"] = "entregaram";
          };
          sing = { ["1"] = "entreguei", ["2"] = "entregaste", ["3"] = "entregou" };
        };
      };
      infn =
      {
        impe = "entregar";
        pers =
        {
          plur = { ["1"] = "entregarmos", ["2"] = "entregardes", ["3"] = "entregarem" };
          sing = { ["1"] = "entregar", ["2"] = "entregares", ["3"] = "entregar" };
        };
      };
      part_past =
      {
        plur = { f = "entregues", m = "entregues" };
        sing = { f = "entregue", m = "entregue" };
      };
      part_pres = "entregando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "entregarmos", ["2"] = "entregardes", ["3"] = "entregarem" };
          sing = { ["1"] = "entregar", ["2"] = "entregares", ["3"] = "entregar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "entregássemos";
            ["2"] = "entregásseis";
            ["3"] = "entregassem";
          };
          sing = { ["1"] = "entregasse", ["2"] = "entregasses", ["3"] = "entregasse" };
        };
        pres =
        {
          plur = { ["1"] = "entreguemos", ["2"] = "entregueis", ["3"] = "entreguem" };
          sing = { ["1"] = "entregue", ["2"] = "entregues", ["3"] = "entregue" };
        };
      };
    };
    suffix = "ar";
    verb = "entregar";
  };
  enxugar =
  {
    abundant = true;
    comments =
    {
      "The last letter ''g'' is changed to ''gu'', depending on the following vowel, resulting in these three possible sequences: -ga-, -gue- and -go-.";
      "There are additional, shorter past participles, the masculine singular ending in -uto.";
    };
    examples = { "enxugar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "enxuguemos", ["2"] = "enxugai", ["3"] = "enxuguem" };
          sing = { ["1"] = "enxugue", ["2"] = "enxuga", ["3"] = "enxugue" };
        };
        negt =
        {
          plur = { ["1"] = "enxuguemos", ["2"] = "enxugueis", ["3"] = "enxuguem" };
          sing = { ["1"] = "enxugue", ["2"] = "enxugues", ["3"] = "enxugue" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "enxugaríamos";
            ["2"] = "enxugaríeis";
            ["3"] = "enxugariam";
          };
          sing = { ["1"] = "enxugaria", ["2"] = "enxugarias", ["3"] = "enxugaria" };
        };
        futu =
        {
          plur = { ["1"] = "enxugaremos", ["2"] = "enxugareis", ["3"] = "enxugarão" };
          sing = { ["1"] = "enxugarei", ["2"] = "enxugarás", ["3"] = "enxugará" };
        };
        impf =
        {
          plur = { ["1"] = "enxugávamos", ["2"] = "enxugáveis", ["3"] = "enxugavam" };
          sing = { ["1"] = "enxugava", ["2"] = "enxugavas", ["3"] = "enxugava" };
        };
        plpf =
        {
          plur = { ["1"] = "enxugáramos", ["2"] = "enxugáreis", ["3"] = "enxugaram" };
          sing = { ["1"] = "enxugara", ["2"] = "enxugaras", ["3"] = "enxugara" };
        };
        pres =
        {
          plur = { ["1"] = "enxugamos", ["2"] = "enxugais", ["3"] = "enxugam" };
          sing = { ["1"] = "enxugo", ["2"] = "enxugas", ["3"] = "enxuga" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "enxugamos";
            ["1_alt"] = { "enxugámos" };
            ["2"] = "enxugastes";
            ["3"] = "enxugaram";
          };
          sing = { ["1"] = "enxuguei", ["2"] = "enxugaste", ["3"] = "enxugou" };
        };
      };
      infn =
      {
        impe = "enxugar";
        pers =
        {
          plur = { ["1"] = "enxugarmos", ["2"] = "enxugardes", ["3"] = "enxugarem" };
          sing = { ["1"] = "enxugar", ["2"] = "enxugares", ["3"] = "enxugar" };
        };
      };
      long_part_past =
      {
        plur = { f = "enxugadas", m = "enxugados" };
        sing = { f = "enxugada", m = "enxugado" };
      };
      part_pres = "enxugando";
      short_part_past =
      {
        plur = { f = "enxutas", m = "enxutos" };
        sing = { f = "enxuta", m = "enxuto" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "enxugarmos", ["2"] = "enxugardes", ["3"] = "enxugarem" };
          sing = { ["1"] = "enxugar", ["2"] = "enxugares", ["3"] = "enxugar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "enxugássemos";
            ["2"] = "enxugásseis";
            ["3"] = "enxugassem";
          };
          sing = { ["1"] = "enxugasse", ["2"] = "enxugasses", ["3"] = "enxugasse" };
        };
        pres =
        {
          plur = { ["1"] = "enxuguemos", ["2"] = "enxugueis", ["3"] = "enxuguem" };
          sing = { ["1"] = "enxugue", ["2"] = "enxugues", ["3"] = "enxugue" };
        };
      };
    };
    suffix = "ar";
    verb = "enxugar";
  };
  er =
  {
    comments = { };
    examples = { "comer", "mexer", "temer", "varrer", "vender" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "amos", ["2"] = "ei", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "e", ["3"] = "a" };
        };
        negt =
        {
          plur = { ["1"] = "amos", ["2"] = "ais", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "as", ["3"] = "a" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "eríamos", ["2"] = "eríeis", ["3"] = "eriam" };
          sing = { ["1"] = "eria", ["2"] = "erias", ["3"] = "eria" };
        };
        futu =
        {
          plur = { ["1"] = "eremos", ["2"] = "ereis", ["3"] = "erão" };
          sing = { ["1"] = "erei", ["2"] = "erás", ["3"] = "erá" };
        };
        impf =
        {
          plur = { ["1"] = "íamos", ["2"] = "íeis", ["3"] = "iam" };
          sing = { ["1"] = "ia", ["2"] = "ias", ["3"] = "ia" };
        };
        plpf =
        {
          plur = { ["1"] = "êramos", ["2"] = "êreis", ["3"] = "eram" };
          sing = { ["1"] = "era", ["2"] = "eras", ["3"] = "era" };
        };
        pres =
        {
          plur = { ["1"] = "emos", ["2"] = "eis", ["3"] = "em" };
          sing = { ["1"] = "o", ["2"] = "es", ["3"] = "e" };
        };
        pret =
        {
          plur = { ["1"] = "emos", ["2"] = "estes", ["3"] = "eram" };
          sing = { ["1"] = "i", ["2"] = "este", ["3"] = "eu" };
        };
      };
      infn =
      {
        impe = "er";
        pers =
        {
          plur = { ["1"] = "ermos", ["2"] = "erdes", ["3"] = "erem" };
          sing = { ["1"] = "er", ["2"] = "eres", ["3"] = "er" };
        };
      };
      part_past =
      {
        plur = { f = "idas", m = "idos" };
        sing = { f = "ida", m = "ido" };
      };
      part_pres = "endo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ermos", ["2"] = "erdes", ["3"] = "erem" };
          sing = { ["1"] = "er", ["2"] = "eres", ["3"] = "er" };
        };
        impf =
        {
          plur = { ["1"] = "êssemos", ["2"] = "êsseis", ["3"] = "essem" };
          sing = { ["1"] = "esse", ["2"] = "esses", ["3"] = "esse" };
        };
        pres =
        {
          plur = { ["1"] = "amos", ["2"] = "ais", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "as", ["3"] = "a" };
        };
      };
    };
    suffix = "er";
    verb = "er";
  };
  ergir =
  {
    abundant = true;
    comments =
    {
      "The last letter ''g'' is changed to ''j'', depending on the following vowel, resulting in these four possible sequences: -ja-, -ge-, -gi- and -jo-.";
      "There are additional, shorter past participles, the masculine singular ending in -erso.";
    };
    examples = { "aspergir", "emergir", "imergir", "submergir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "erjamos", ["2"] = "ergi", ["3"] = "erjam" };
          sing = { ["1"] = "erja", ["2"] = "erge", ["3"] = "erja" };
        };
        negt =
        {
          plur = { ["1"] = "erjamos", ["2"] = "erjais", ["3"] = "erjam" };
          sing = { ["1"] = "erja", ["2"] = "erjas", ["3"] = "erja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ergiríamos", ["2"] = "ergiríeis", ["3"] = "ergiriam" };
          sing = { ["1"] = "ergiria", ["2"] = "ergirias", ["3"] = "ergiria" };
        };
        futu =
        {
          plur = { ["1"] = "ergiremos", ["2"] = "ergireis", ["3"] = "ergirão" };
          sing = { ["1"] = "ergirei", ["2"] = "ergirás", ["3"] = "ergirá" };
        };
        impf =
        {
          plur = { ["1"] = "ergíamos", ["2"] = "ergíeis", ["3"] = "ergiam" };
          sing = { ["1"] = "ergia", ["2"] = "ergias", ["3"] = "ergia" };
        };
        plpf =
        {
          plur = { ["1"] = "ergíramos", ["2"] = "ergíreis", ["3"] = "ergiram" };
          sing = { ["1"] = "ergira", ["2"] = "ergiras", ["3"] = "ergira" };
        };
        pres =
        {
          plur = { ["1"] = "ergimos", ["2"] = "ergis", ["3"] = "ergem" };
          sing = { ["1"] = "erjo", ["2"] = "erges", ["3"] = "erge" };
        };
        pret =
        {
          plur = { ["1"] = "ergimos", ["2"] = "ergistes", ["3"] = "ergiram" };
          sing = { ["1"] = "ergi", ["2"] = "ergiste", ["3"] = "ergiu" };
        };
      };
      infn =
      {
        impe = "ergir";
        pers =
        {
          plur = { ["1"] = "ergirmos", ["2"] = "ergirdes", ["3"] = "ergirem" };
          sing = { ["1"] = "ergir", ["2"] = "ergires", ["3"] = "ergir" };
        };
      };
      part_past =
      {
        plur = { f = "ersas", m = "ersos" };
        sing = { f = "ergida", m = "erso" };
      };
      part_pres = "ergindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ergirmos", ["2"] = "ergirdes", ["3"] = "ergirem" };
          sing = { ["1"] = "ergir", ["2"] = "ergires", ["3"] = "ergir" };
        };
        impf =
        {
          plur = { ["1"] = "ergíssemos", ["2"] = "ergísseis", ["3"] = "ergissem" };
          sing = { ["1"] = "ergisse", ["2"] = "ergisses", ["3"] = "ergisse" };
        };
        pres =
        {
          plur = { ["1"] = "erjamos", ["2"] = "erjais", ["3"] = "erjam" };
          sing = { ["1"] = "erja", ["2"] = "erjas", ["3"] = "erja" };
        };
      };
    };
    suffix = "ir";
    verb = "ergir";
  };
  erir =
  {
    comments = { };
    examples = { "ferir", "conferir", "deferir", "ingerir", "inserir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "iramos", ["2"] = "eri", ["3"] = "iram" };
          sing = { ["1"] = "ira", ["2"] = "ere", ["3"] = "ira" };
        };
        negt =
        {
          plur = { ["1"] = "iramos", ["2"] = "irais", ["3"] = "iram" };
          sing = { ["1"] = "ira", ["2"] = "iras", ["3"] = "ira" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "eriríamos", ["2"] = "eriríeis", ["3"] = "eririam" };
          sing = { ["1"] = "eriria", ["2"] = "eririas", ["3"] = "eriria" };
        };
        futu =
        {
          plur = { ["1"] = "eriremos", ["2"] = "erireis", ["3"] = "erirão" };
          sing = { ["1"] = "erirei", ["2"] = "erirás", ["3"] = "erirá" };
        };
        impf =
        {
          plur = { ["1"] = "eríamos", ["2"] = "eríeis", ["3"] = "eriam" };
          sing = { ["1"] = "eria", ["2"] = "erias", ["3"] = "eria" };
        };
        plpf =
        {
          plur = { ["1"] = "eríramos", ["2"] = "eríreis", ["3"] = "eriram" };
          sing = { ["1"] = "erira", ["2"] = "eriras", ["3"] = "erira" };
        };
        pres =
        {
          plur = { ["1"] = "erimos", ["2"] = "eris", ["3"] = "erem" };
          sing = { ["1"] = "iro", ["2"] = "eres", ["3"] = "ere" };
        };
        pret =
        {
          plur = { ["1"] = "erimos", ["2"] = "eristes", ["3"] = "eriram" };
          sing = { ["1"] = "eri", ["2"] = "eriste", ["3"] = "eriu" };
        };
      };
      infn =
      {
        impe = "erir";
        pers =
        {
          plur = { ["1"] = "erirmos", ["2"] = "erirdes", ["3"] = "erirem" };
          sing = { ["1"] = "erir", ["2"] = "erires", ["3"] = "erir" };
        };
      };
      part_past =
      {
        plur = { f = "eridas", m = "eridos" };
        sing = { f = "erida", m = "erido" };
      };
      part_pres = "erindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "erirmos", ["2"] = "erirdes", ["3"] = "erirem" };
          sing = { ["1"] = "erir", ["2"] = "erires", ["3"] = "erir" };
        };
        impf =
        {
          plur = { ["1"] = "eríssemos", ["2"] = "erísseis", ["3"] = "erissem" };
          sing = { ["1"] = "erisse", ["2"] = "erisses", ["3"] = "erisse" };
        };
        pres =
        {
          plur = { ["1"] = "iramos", ["2"] = "irais", ["3"] = "iram" };
          sing = { ["1"] = "ira", ["2"] = "iras", ["3"] = "ira" };
        };
      };
    };
    irregular = false;
    suffix = "ir";
    verb = "erir";
  };
  ["erir-defective"] =
  {
    comments = { };
    examples = { "gerir", "ingerir", "digerir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1_defective"] = "iramos", ["2"] = "eri", ["3_defective"] = "iram" };
          sing = { ["2_defective"] = "ere", ["3_defective"] = "ira" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "iramos";
            ["2_defective"] = "irais";
            ["3_defective"] = "iram";
          };
          sing = { ["2_defective"] = "iras", ["3_defective"] = "ira" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "eriríamos", ["2"] = "eriríeis", ["3"] = "eririam" };
          sing = { ["1"] = "eriria", ["2"] = "eririas", ["3"] = "eriria" };
        };
        futu =
        {
          plur = { ["1"] = "eriremos", ["2"] = "erireis", ["3"] = "erirão" };
          sing = { ["1"] = "erirei", ["2"] = "erirás", ["3"] = "erirá" };
        };
        impf =
        {
          plur = { ["1"] = "eríamos", ["2"] = "eríeis", ["3"] = "eriam" };
          sing = { ["1"] = "eria", ["2"] = "erias", ["3"] = "eria" };
        };
        plpf =
        {
          plur = { ["1"] = "eríramos", ["2"] = "eríreis", ["3"] = "eriram" };
          sing = { ["1"] = "erira", ["2"] = "eriras", ["3"] = "erira" };
        };
        pres =
        {
          plur = { ["1"] = "erimos", ["2"] = "eris", ["3_defective"] = "erem" };
          sing =
          {
            ["1_defective"] = "iro";
            ["2_defective"] = "eres";
            ["3_defective"] = "ere";
          };
        };
        pret =
        {
          plur = { ["1"] = "erimos", ["2"] = "eristes", ["3"] = "eriram" };
          sing = { ["1"] = "eri", ["2"] = "eriste", ["3"] = "eriu" };
        };
      };
      infn =
      {
        impe = "erir";
        pers =
        {
          plur = { ["1"] = "erirmos", ["2"] = "erirdes", ["3"] = "erirem" };
          sing = { ["1"] = "erir", ["2"] = "erires", ["3"] = "erir" };
        };
      };
      part_past =
      {
        plur = { f = "eridas", m = "eridos" };
        sing = { f = "erida", m = "erido" };
      };
      part_pres = "erindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "erirmos", ["2"] = "erirdes", ["3"] = "erirem" };
          sing = { ["1"] = "erir", ["2"] = "erires", ["3"] = "erir" };
        };
        impf =
        {
          plur = { ["1"] = "eríssemos", ["2"] = "erísseis", ["3"] = "erissem" };
          sing = { ["1"] = "erisse", ["2"] = "erisses", ["3"] = "erisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "iramos";
            ["2_defective"] = "irais";
            ["3_defective"] = "iram";
          };
          sing =
          {
            ["1_defective"] = "ira";
            ["2_defective"] = "iras";
            ["3_defective"] = "ira";
          };
        };
      };
    };
    irregular = false;
    suffix = "ir";
    verb = "erir-defective";
  };
  estar =
  {
    comments = { };
    examples = { "estar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "estejamos", ["2"] = "estai", ["3"] = "estejam" };
          sing = { ["1"] = "esteja", ["2"] = "está", ["3"] = "esteja" };
        };
        negt =
        {
          plur = { ["1"] = "estejamos", ["2"] = "estejais", ["3"] = "estejam" };
          sing = { ["1"] = "esteja", ["2"] = "estejas", ["3"] = "esteja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "estaríamos", ["2"] = "estaríeis", ["3"] = "estariam" };
          sing = { ["1"] = "estaria", ["2"] = "estarias", ["3"] = "estaria" };
        };
        futu =
        {
          plur = { ["1"] = "estaremos", ["2"] = "estareis", ["3"] = "estarão" };
          sing = { ["1"] = "estarei", ["2"] = "estareis", ["3"] = "estará" };
        };
        impf =
        {
          plur = { ["1"] = "estávamos", ["2"] = "estáveis", ["3"] = "estávam" };
          sing = { ["1"] = "estava", ["2"] = "estavas", ["3"] = "estava" };
        };
        plpf =
        {
          plur = { ["1"] = "estivéramos", ["2"] = "estivéreis", ["3"] = "estiveram" };
          sing = { ["1"] = "estivera", ["2"] = "estiveras", ["3"] = "estivera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "estamos";
            ["1_alt"] = { "estámos" };
            ["2"] = "estais";
            ["3"] = "estão";
          };
          sing = { ["1"] = "estou", ["2"] = "estás", ["3"] = "está" };
        };
        pret =
        {
          plur = { ["1"] = "estivemos", ["2"] = "estivestes", ["3"] = "estiveram" };
          sing = { ["1"] = "estive", ["2"] = "estiveste", ["3"] = "esteve" };
        };
      };
      infn =
      {
        impe = "estar";
        pers =
        {
          plur = { ["1"] = "estarmos", ["2"] = "estardes", ["3"] = "estarem" };
          sing = { ["1"] = "estar", ["2"] = "estares", ["3"] = "estar" };
        };
      };
      part_past =
      {
        plur = { f = "estadas", m = "estados" };
        sing = { f = "estada", m = "estado" };
      };
      part_pres = "estando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "estivermos", ["2"] = "estiverdes", ["3"] = "estiverem" };
          sing = { ["1"] = "estiver", ["2"] = "estiveres", ["3"] = "estiver" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "estivéssemos";
            ["2"] = "estivéssies";
            ["3"] = "estivessem";
          };
          sing = { ["1"] = "estivesse", ["2"] = "estivesses", ["3"] = "estivesse" };
        };
        pres =
        {
          plur = { ["1"] = "estejamos", ["2"] = "estejais", ["3"] = "estejam" };
          sing = { ["1"] = "esteja", ["2"] = "estejas", ["3"] = "esteja" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "estar";
  };
  eter =
  {
    comments = { };
    examples = { "deter", "reter" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "etenhamos", ["2"] = "etende", ["3"] = "etenham" };
          sing = { ["1"] = "etenha", ["2"] = "etém", ["3"] = "etenha" };
        };
        negt =
        {
          plur = { ["1"] = "etenhamos", ["2"] = "etenhais", ["3"] = "etenham" };
          sing = { ["1"] = "etenha", ["2"] = "etenhas", ["3"] = "etenha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "eteríamos", ["2"] = "eteríeis", ["3"] = "eteriam" };
          sing = { ["1"] = "eteria", ["2"] = "eterias", ["3"] = "eteria" };
        };
        futu =
        {
          plur = { ["1"] = "eteremos", ["2"] = "etereis", ["3"] = "eterão" };
          sing = { ["1"] = "eterei", ["2"] = "eterás", ["3"] = "eterá" };
        };
        impf =
        {
          plur = { ["1"] = "etínhamos", ["2"] = "etínheis", ["3"] = "etinham" };
          sing = { ["1"] = "etinha", ["2"] = "etinhas", ["3"] = "etinha" };
        };
        plpf =
        {
          plur = { ["1"] = "etivéramos", ["2"] = "etivéreis", ["3"] = "etiveram" };
          sing = { ["1"] = "etivera", ["2"] = "etiveras", ["3"] = "etivera" };
        };
        pres =
        {
          plur = { ["1"] = "etemos", ["2"] = "etendes", ["3"] = "etêm" };
          sing = { ["1"] = "etenho", ["2"] = "eténs", ["3"] = "etém" };
        };
        pret =
        {
          plur = { ["1"] = "etivemos", ["2"] = "etivestes", ["3"] = "etiveram" };
          sing = { ["1"] = "etive", ["2"] = "etiveste", ["3"] = "eteve" };
        };
      };
      infn =
      {
        impe = "eter";
        pers =
        {
          plur = { ["1"] = "etermos", ["2"] = "eterdes", ["3"] = "eterem" };
          sing = { ["1"] = "eter", ["2"] = "eteres", ["3"] = "eter" };
        };
      };
      part_past =
      {
        plur = { f = "etidas", m = "etidos" };
        sing = { f = "etida", m = "etido" };
      };
      part_pres = "etendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "etivermos", ["2"] = "etiverdes", ["3"] = "etiverem" };
          sing = { ["1"] = "etiver", ["2"] = "etiveres", ["3"] = "etiver" };
        };
        impf =
        {
          plur = { ["1"] = "etivéssemos", ["2"] = "etivésseis", ["3"] = "etivessem" };
          sing = { ["1"] = "etivesse", ["2"] = "etivesses", ["3"] = "etivesse" };
        };
        pres =
        {
          plur = { ["1"] = "etenhamos", ["2"] = "etenhais", ["3"] = "etenham" };
          sing = { ["1"] = "etenha", ["2"] = "etenhas", ["3"] = "etenha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "eter";
  };
  etir =
  {
    comments = { };
    examples = { "competir", "repetir", "refletir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "itamos", ["2"] = "eti", ["3"] = "itam" };
          sing = { ["2"] = "ete", ["3"] = "ita" };
        };
        negt =
        {
          plur = { ["1"] = "itamos", ["2"] = "itais", ["3"] = "itam" };
          sing = { ["2"] = "itas", ["3"] = "ita" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "etiríamos", ["2"] = "etiríeis", ["3"] = "etiriam" };
          sing = { ["1"] = "etiria", ["2"] = "etirias", ["3"] = "etiria" };
        };
        futu =
        {
          plur = { ["1"] = "etiremos", ["2"] = "etireis", ["3"] = "etirão" };
          sing = { ["1"] = "etirei", ["2"] = "etirás", ["3"] = "etirá" };
        };
        impf =
        {
          plur = { ["1"] = "etíamos", ["2"] = "etíeis", ["3"] = "etiam" };
          sing = { ["1"] = "etia", ["2"] = "etias", ["3"] = "etia" };
        };
        plpf =
        {
          plur = { ["1"] = "etíramos", ["2"] = "etíreis", ["3"] = "etiram" };
          sing = { ["1"] = "etira", ["2"] = "etiras", ["3"] = "etira" };
        };
        pres =
        {
          plur = { ["1"] = "etimos", ["2"] = "etis", ["3"] = "etem" };
          sing = { ["1"] = "ito", ["2"] = "etes", ["3"] = "ete" };
        };
        pret =
        {
          plur = { ["1"] = "etimos", ["2"] = "etistes", ["3"] = "etiram" };
          sing = { ["1"] = "eti", ["2"] = "etiste", ["3"] = "etiu" };
        };
      };
      infn =
      {
        impe = "etir";
        pers =
        {
          plur = { ["1"] = "etirmos", ["2"] = "etirdes", ["3"] = "etirem" };
          sing = { ["1"] = "etir", ["2"] = "etires", ["3"] = "etir" };
        };
      };
      part_past =
      {
        plur = { f = "etidas", m = "etidos" };
        sing = { f = "etida", m = "etido" };
      };
      part_pres = "etindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "etirmos", ["2"] = "etirdes", ["3"] = "etirem" };
          sing = { ["1"] = "etir", ["2"] = "etires", ["3"] = "etir" };
        };
        impf =
        {
          plur = { ["1"] = "etíssemos", ["2"] = "etísseis", ["3"] = "etissem" };
          sing = { ["1"] = "etisse", ["2"] = "etisses", ["3"] = "etisse" };
        };
        pres =
        {
          plur = { ["1"] = "itamos", ["2"] = "itais", ["3"] = "itam" };
          sing = { ["1"] = "ita", ["2"] = "itas", ["3"] = "ita" };
        };
      };
    };
    suffix = "etir";
    verb = "etir";
  };
  explodir =
  {
    comments =
    {
      "When the suffix of a conjugated form starts with ''a'' or ''o'', this conjugated form does not exist. However, this rule may be ignored, so the verb can also be fully conjugated.";
    };
    defective = true;
    examples = { "abolir", "explodir", "latir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "explodamos";
            ["2"] = "explodi";
            ["3_defective"] = "explodam";
          };
          sing =
          {
            ["1_defective"] = "exploda";
            ["2"] = "explode";
            ["3_defective"] = "exploda";
          };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "explodamos";
            ["2_defective"] = "explodais";
            ["3_defective"] = "explodam";
          };
          sing =
          {
            ["1_defective"] = "exploda";
            ["2_defective"] = "explodas";
            ["3_defective"] = "exploda";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "explodiríamos";
            ["2"] = "explodiríeis";
            ["3"] = "explodiriam";
          };
          sing = { ["1"] = "explodiria", ["2"] = "explodirias", ["3"] = "explodiria" };
        };
        futu =
        {
          plur =
          {
            ["1"] = "explodiremos";
            ["2"] = "explodireis";
            ["3"] = "explodirão";
          };
          sing = { ["1"] = "explodirei", ["2"] = "explodirás", ["3"] = "explodirá" };
        };
        impf =
        {
          plur = { ["1"] = "explodíamos", ["2"] = "explodíeis", ["3"] = "explodiam" };
          sing = { ["1"] = "explodia", ["2"] = "explodias", ["3"] = "explodia" };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "explodíramos";
            ["2"] = "explodíreis";
            ["3"] = "explodiram";
          };
          sing = { ["1"] = "explodira", ["2"] = "explodiras", ["3"] = "explodira" };
        };
        pres =
        {
          plur = { ["1"] = "explodimos", ["2"] = "explodis", ["3"] = "explodem" };
          sing = { ["1_defective"] = "explodo", ["2"] = "explodes", ["3"] = "explode" };
        };
        pret =
        {
          plur = { ["1"] = "explodimos", ["2"] = "explodistes", ["3"] = "explodiram" };
          sing = { ["1"] = "explodi", ["2"] = "explodiste", ["3"] = "explodiu" };
        };
      };
      infn =
      {
        impe = "explodir";
        pers =
        {
          plur = { ["1"] = "explodirmos", ["2"] = "explodirdes", ["3"] = "explodirem" };
          sing = { ["1"] = "explodir", ["2"] = "explodires", ["3"] = "explodir" };
        };
      };
      part_past =
      {
        plur = { f = "explodidas", m = "explodidos" };
        sing = { f = "explodida", m = "explodido" };
      };
      part_pres = "explodindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "explodirmos", ["2"] = "explodirdes", ["3"] = "explodirem" };
          sing = { ["1"] = "explodir", ["2"] = "explodires", ["3"] = "explodir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "explodíssemos";
            ["2"] = "explodísseis";
            ["3"] = "explodissem";
          };
          sing = { ["1"] = "explodisse", ["2"] = "explodisses", ["3"] = "explodisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "explodamos";
            ["2_defective"] = "explodais";
            ["3_defective"] = "explodam";
          };
          sing =
          {
            ["1_defective"] = "exploda";
            ["2_defective"] = "explodas";
            ["3_defective"] = "exploda";
          };
        };
      };
    };
    suffix = "ir";
    verb = "explodir";
  };
  falir =
  {
    comments = { };
    defective = true;
    examples = { "falir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "fali" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "faliríamos", ["2"] = "faliríeis", ["3"] = "faliriam" };
          sing = { ["1"] = "faliria", ["2"] = "falirias", ["3"] = "faliria" };
        };
        futu =
        {
          plur = { ["1"] = "faliremos", ["2"] = "falireis", ["3"] = "falirão" };
          sing = { ["1"] = "falirei", ["2"] = "falirás", ["3"] = "falirá" };
        };
        impf =
        {
          plur = { ["1"] = "falíamos", ["2"] = "falíeis", ["3"] = "faliam" };
          sing = { ["1"] = "falia", ["2"] = "falias", ["3"] = "falia" };
        };
        plpf =
        {
          plur = { ["1"] = "falíramos", ["2"] = "falíreis", ["3"] = "faliram" };
          sing = { ["1"] = "falira", ["2"] = "faliras", ["3"] = "falira" };
        };
        pres =
        {
          plur = { ["1"] = "falimos", ["2"] = "falis" };
        };
        pret =
        {
          plur = { ["1"] = "falimos", ["2"] = "falistes", ["3"] = "faliram" };
          sing = { ["1"] = "fali", ["2"] = "faliste", ["3"] = "faliu" };
        };
      };
      infn =
      {
        impe = "falir";
        pers =
        {
          plur = { ["1"] = "falirmos", ["2"] = "falirdes", ["3"] = "falirem" };
          sing = { ["1"] = "falir", ["2"] = "falires", ["3"] = "falir" };
        };
      };
      part_past =
      {
        plur = { f = "falidas", m = "falidos" };
        sing = { f = "falida", m = "falido" };
      };
      part_pres = "falindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "falirmos", ["2"] = "falirdes", ["3"] = "falirem" };
          sing = { ["1"] = "falir", ["2"] = "falires", ["3"] = "falir" };
        };
        impf =
        {
          plur = { ["1"] = "falíssemos", ["2"] = "falísseis", ["3"] = "falissem" };
          sing = { ["1"] = "falisse", ["2"] = "falisses", ["3"] = "falisse" };
        };
      };
    };
    suffix = "ir";
    verb = "falir";
  };
  fazer =
  {
    abundant = true;
    comments = { };
    examples =
    {
      "afazer";
      "contrafazer";
      "desfazer";
      "fazer";
      "liquefazer";
      "perfazer";
      "rarefazer";
      "refazer";
      "satisfazer";
    };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "façamos", ["2"] = "fazei", ["3"] = "façam" };
          sing =
          {
            ["1"] = "faça";
            ["2"] = "faze";
            ["2_alt"] = { "faz" };
            ["3"] = "faça";
          };
        };
        negt =
        {
          plur = { ["1"] = "façamos", ["2"] = "façais", ["3"] = "façam" };
          sing = { ["1"] = "faça", ["2"] = "faças", ["3"] = "faça" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "faríamos", ["2"] = "faríeis", ["3"] = "fariam" };
          sing = { ["1"] = "faria", ["2"] = "farias", ["3"] = "faria" };
        };
        futu =
        {
          plur = { ["1"] = "faremos", ["2"] = "fareis", ["3"] = "farão" };
          sing = { ["1"] = "farei", ["2"] = "farás", ["3"] = "fará" };
        };
        impf =
        {
          plur = { ["1"] = "fazíamos", ["2"] = "fazíeis", ["3"] = "faziam" };
          sing = { ["1"] = "fazia", ["2"] = "fazias", ["3"] = "fazia" };
        };
        plpf =
        {
          plur = { ["1"] = "fizéramos", ["2"] = "fizéreis", ["3"] = "fizeram" };
          sing = { ["1"] = "fizera", ["2"] = "fizeras", ["3"] = "fizera" };
        };
        pres =
        {
          plur = { ["1"] = "fazemos", ["2"] = "fazeis", ["3"] = "fazem" };
          sing = { ["1"] = "faço", ["2"] = "fazes", ["3"] = "faz" };
        };
        pret =
        {
          plur = { ["1"] = "fizemos", ["2"] = "fizestes", ["3"] = "fizeram" };
          sing = { ["1"] = "fiz", ["2"] = "fizeste", ["3"] = "fez" };
        };
      };
      infn =
      {
        impe = "fazer";
        pers =
        {
          plur = { ["1"] = "fazermos", ["2"] = "fazerdes", ["3"] = "fazerem" };
          sing = { ["1"] = "fazer", ["2"] = "fazeres", ["3"] = "fazer" };
        };
      };
      part_past =
      {
        plur = { f = "feitas", m = "feitos" };
        sing = { f = "feita", m = "feito" };
      };
      part_pres = "fazendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "fizermos", ["2"] = "fizerdes", ["3"] = "fizerem" };
          sing = { ["1"] = "fizer", ["2"] = "fizeres", ["3"] = "fizer" };
        };
        impf =
        {
          plur = { ["1"] = "fizéssemos", ["2"] = "fizésseis", ["3"] = "fizessem" };
          sing = { ["1"] = "fizesse", ["2"] = "fizesses", ["3"] = "fizesse" };
        };
        pres =
        {
          plur = { ["1"] = "façamos", ["2"] = "façais", ["3"] = "façam" };
          sing = { ["1"] = "faça", ["2"] = "faças", ["3"] = "faça" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "fazer";
  };
  findar =
  {
    abundant = true;
    comments =
    {
      "There are additional, shorter past participles without the common letter sequence -ad-.";
    };
    examples = { "findar", "ganhar", "pasmar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "findemos", ["2"] = "findai", ["3"] = "findem" };
          sing = { ["1"] = "finde", ["2"] = "finda", ["3"] = "finde" };
        };
        negt =
        {
          plur = { ["1"] = "findemos", ["2"] = "findeis", ["3"] = "findem" };
          sing = { ["1"] = "finde", ["2"] = "findes", ["3"] = "finde" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "findaríamos", ["2"] = "findaríeis", ["3"] = "findariam" };
          sing = { ["1"] = "findaria", ["2"] = "findarias", ["3"] = "findaria" };
        };
        futu =
        {
          plur = { ["1"] = "findaremos", ["2"] = "findareis", ["3"] = "findarão" };
          sing = { ["1"] = "findarei", ["2"] = "findarás", ["3"] = "findará" };
        };
        impf =
        {
          plur = { ["1"] = "findávamos", ["2"] = "findáveis", ["3"] = "findavam" };
          sing = { ["1"] = "findava", ["2"] = "findavas", ["3"] = "findava" };
        };
        plpf =
        {
          plur = { ["1"] = "findáramos", ["2"] = "findáreis", ["3"] = "findaram" };
          sing = { ["1"] = "findara", ["2"] = "findaras", ["3"] = "findara" };
        };
        pres =
        {
          plur = { ["1"] = "findamos", ["2"] = "findais", ["3"] = "findam" };
          sing = { ["1"] = "findo", ["2"] = "findas", ["3"] = "finda" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "findamos";
            ["1_alt"] = { "findámos" };
            ["2"] = "findastes";
            ["3"] = "findaram";
          };
          sing = { ["1"] = "findei", ["2"] = "findaste", ["3"] = "findou" };
        };
      };
      infn =
      {
        impe = "findar";
        pers =
        {
          plur = { ["1"] = "findarmos", ["2"] = "findardes", ["3"] = "findarem" };
          sing = { ["1"] = "findar", ["2"] = "findares", ["3"] = "findar" };
        };
      };
      part_past =
      {
        plur = { f = "findas", m = "findos" };
        sing = { f = "finda", m = "findo" };
      };
      part_pres = "findando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "findarmos", ["2"] = "findardes", ["3"] = "findarem" };
          sing = { ["1"] = "findar", ["2"] = "findares", ["3"] = "findar" };
        };
        impf =
        {
          plur = { ["1"] = "findássemos", ["2"] = "findásseis", ["3"] = "findassem" };
          sing = { ["1"] = "findasse", ["2"] = "findasses", ["3"] = "findasse" };
        };
        pres =
        {
          plur = { ["1"] = "findemos", ["2"] = "findeis", ["3"] = "findem" };
          sing = { ["1"] = "finde", ["2"] = "findes", ["3"] = "finde" };
        };
      };
    };
    suffix = "ar";
    verb = "findar";
  };
  florir =
  {
    comments = { };
    defective = true;
    examples = { "florir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "flori" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "floriríamos", ["2"] = "floriríeis", ["3"] = "floririam" };
          sing = { ["1"] = "floriria", ["2"] = "floririas", ["3"] = "floriria" };
        };
        futu =
        {
          plur = { ["1"] = "floriremos", ["2"] = "florireis", ["3"] = "florirão" };
          sing = { ["1"] = "florirei", ["2"] = "florirás", ["3"] = "florirá" };
        };
        impf =
        {
          plur = { ["1"] = "floríamos", ["2"] = "floríeis", ["3"] = "floriam" };
          sing = { ["1"] = "floria", ["2"] = "florias", ["3"] = "floria" };
        };
        plpf =
        {
          plur = { ["1"] = "floríramos", ["2"] = "floríreis", ["3"] = "floriram" };
          sing = { ["1"] = "florira", ["2"] = "floriras", ["3"] = "florira" };
        };
        pres =
        {
          plur = { ["1"] = "florimos", ["2"] = "floris" };
        };
        pret =
        {
          plur = { ["1"] = "florimos", ["2"] = "floristes", ["3"] = "floriram" };
          sing = { ["1"] = "flori", ["2"] = "floriste", ["3"] = "floriu" };
        };
      };
      infn =
      {
        impe = "florir";
        pers =
        {
          plur = { ["1"] = "florirmos", ["2"] = "florirdes", ["3"] = "florirem" };
          sing = { ["1"] = "florir", ["2"] = "florires", ["3"] = "florir" };
        };
      };
      part_past =
      {
        plur = { f = "floridas", m = "floridos" };
        sing = { f = "florida", m = "florido" };
      };
      part_pres = "florindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "florirmos", ["2"] = "florirdes", ["3"] = "florirem" };
          sing = { ["1"] = "florir", ["2"] = "florires", ["3"] = "florir" };
        };
        impf =
        {
          plur = { ["1"] = "floríssemos", ["2"] = "florísseis", ["3"] = "florissem" };
          sing = { ["1"] = "florisse", ["2"] = "florisses", ["3"] = "florisse" };
        };
      };
    };
    suffix = "ir";
    verb = "florir";
  };
  folegar =
  {
    abundant = true;
    comments =
    {
      "The last letter ''g'' is changed to ''gu'', depending on the following vowel, resulting in these three possible sequences: -ga-, -gue- and -go-.";
      "An acute accent is added to the letter ''o'' in few conjugated forms.";
    };
    examples = { "resfolegar", "tresfolegar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "foleguemos", ["2"] = "folegai", ["3"] = "foleguem" };
          sing =
          {
            ["1"] = "fólegue";
            ["1_alt"] = { "folegue", "folgue" };
            ["2"] = "fólega";
            ["2_alt"] = { "folega", "folga" };
            ["3"] = "fólegue";
            ["3_alt"] = { "folegue", "folgue" };
          };
        };
        negt =
        {
          plur = { ["1"] = "foleguemos", ["2"] = "folegueis", ["3"] = "foleguem" };
          sing =
          {
            ["1"] = "fólegue";
            ["1_alt"] = { "folegue", "folgue" };
            ["2"] = "fólegues";
            ["2_alt"] = { "folegues", "folgues" };
            ["3"] = "fólegue";
            ["3_alt"] = { "folegue", "folgue" };
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "folegaríamos";
            ["2"] = "folegaríeis";
            ["3"] = "folegariam";
          };
          sing = { ["1"] = "folegaria", ["2"] = "folegarias", ["3"] = "folegaria" };
        };
        futu =
        {
          plur = { ["1"] = "folegaremos", ["2"] = "folegareis", ["3"] = "folegarão" };
          sing = { ["1"] = "folegarei", ["2"] = "folegarás", ["3"] = "folegará" };
        };
        impf =
        {
          plur = { ["1"] = "folegávamos", ["2"] = "folegáveis", ["3"] = "folegavam" };
          sing = { ["1"] = "folegava", ["2"] = "folegavas", ["3"] = "folegava" };
        };
        plpf =
        {
          plur = { ["1"] = "folegáramos", ["2"] = "folegáreis", ["3"] = "folegaram" };
          sing = { ["1"] = "folegara", ["2"] = "folegaras", ["3"] = "folegara" };
        };
        pres =
        {
          plur = { ["1"] = "folegamos", ["2"] = "folegais", ["3"] = "folegam" };
          sing =
          {
            ["1"] = "fólego";
            ["1_alt"] = { "folego", "folgo" };
            ["2"] = "fólegas";
            ["2_alt"] = { "folegas", "folgas" };
            ["3"] = "fólega";
            ["3_alt"] = { "folega", "folga" };
          };
        };
        pret =
        {
          plur =
          {
            ["1"] = "folegamos";
            ["1_alt"] = { "folegámos" };
            ["2"] = "folegastes";
            ["3"] = "folegaram";
          };
          sing = { ["1"] = "foleguei", ["2"] = "folegaste", ["3"] = "folegou" };
        };
      };
      infn =
      {
        impe = "folegar";
        pers =
        {
          plur = { ["1"] = "folegarmos", ["2"] = "folegardes", ["3"] = "folegarem" };
          sing = { ["1"] = "folegar", ["2"] = "folegares", ["3"] = "folegar" };
        };
      };
      part_past =
      {
        plur = { f = "folegadas", m = "folegados" };
        sing = { f = "folegada", m = "folegado" };
      };
      part_pres = "folegando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "folegarmos", ["2"] = "folegardes", ["3"] = "folegarem" };
          sing = { ["1"] = "folegar", ["2"] = "folegares", ["3"] = "folegar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "folegássemos";
            ["2"] = "folegásseis";
            ["3"] = "folegassem";
          };
          sing = { ["1"] = "folegasse", ["2"] = "folegasses", ["3"] = "folegasse" };
        };
        pres =
        {
          plur = { ["1"] = "foleguemos", ["2"] = "folegueis", ["3"] = "foleguem" };
          sing =
          {
            ["1"] = "fólegue";
            ["1_alt"] = { "folegue", "folgue" };
            ["2"] = "fólegues";
            ["2_alt"] = { "folegues", "folgues" };
            ["3"] = "fólegue";
            ["3_alt"] = { "folegue", "folgue" };
          };
        };
      };
    };
    suffix = "ar";
    verb = "folegar";
  };
  ganhar =
  {
    abundant = true;
    comments =
    {
      "There are additional, shorter past participles without the common letter sequence -ad-.";
    };
    examples = { "findar", "ganhar", "pasmar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ganhemos", ["2"] = "ganhai", ["3"] = "ganhem" };
          sing = { ["1"] = "ganhe", ["2"] = "ganha", ["3"] = "ganhe" };
        };
        negt =
        {
          plur = { ["1"] = "ganhemos", ["2"] = "ganheis", ["3"] = "ganhem" };
          sing = { ["1"] = "ganhe", ["2"] = "ganhes", ["3"] = "ganhe" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ganharíamos", ["2"] = "ganharíeis", ["3"] = "ganhariam" };
          sing = { ["1"] = "ganharia", ["2"] = "ganharias", ["3"] = "ganharia" };
        };
        futu =
        {
          plur = { ["1"] = "ganharemos", ["2"] = "ganhareis", ["3"] = "ganharão" };
          sing = { ["1"] = "ganharei", ["2"] = "ganharás", ["3"] = "ganhará" };
        };
        impf =
        {
          plur = { ["1"] = "ganhávamos", ["2"] = "ganháveis", ["3"] = "ganhavam" };
          sing = { ["1"] = "ganhava", ["2"] = "ganhavas", ["3"] = "ganhava" };
        };
        plpf =
        {
          plur = { ["1"] = "ganháramos", ["2"] = "ganháreis", ["3"] = "ganharam" };
          sing = { ["1"] = "ganhara", ["2"] = "ganharas", ["3"] = "ganhara" };
        };
        pres =
        {
          plur = { ["1"] = "ganhamos", ["2"] = "ganhais", ["3"] = "ganham" };
          sing = { ["1"] = "ganho", ["2"] = "ganhas", ["3"] = "ganha" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "ganhamos";
            ["1_alt"] = { "ganhámos" };
            ["2"] = "ganhastes";
            ["3"] = "ganharam";
          };
          sing = { ["1"] = "ganhei", ["2"] = "ganhaste", ["3"] = "ganhou" };
        };
      };
      infn =
      {
        impe = "ganhar";
        pers =
        {
          plur = { ["1"] = "ganharmos", ["2"] = "ganhardes", ["3"] = "ganharem" };
          sing = { ["1"] = "ganhar", ["2"] = "ganhares", ["3"] = "ganhar" };
        };
      };
      part_past =
      {
        plur = { f = "ganhas", m = "ganhos" };
        sing = { f = "ganha", m = "ganho" };
      };
      part_pres = "ganhando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ganharmos", ["2"] = "ganhardes", ["3"] = "ganharem" };
          sing = { ["1"] = "ganhar", ["2"] = "ganhares", ["3"] = "ganhar" };
        };
        impf =
        {
          plur = { ["1"] = "ganhássemos", ["2"] = "ganhásseis", ["3"] = "ganhassem" };
          sing = { ["1"] = "ganhasse", ["2"] = "ganhasses", ["3"] = "ganhasse" };
        };
        pres =
        {
          plur = { ["1"] = "ganhemos", ["2"] = "ganheis", ["3"] = "ganhem" };
          sing = { ["1"] = "ganhe", ["2"] = "ganhes", ["3"] = "ganhe" };
        };
      };
    };
    suffix = "ar";
    verb = "ganhar";
  };
  gar =
  {
    comments =
    {
      "The last letter ''g'' is changed to ''gu'', depending on the following vowel, resulting in these three possible sequences: -ga-, -gue- and -go-.";
    };
    examples = { "apagar", "cegar", "esmagar", "largar", "navegar", "resmungar", "sugar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "guemos", ["2"] = "gai", ["3"] = "guem" };
          sing = { ["1"] = "gue", ["2"] = "ga", ["3"] = "gue" };
        };
        negt =
        {
          plur = { ["1"] = "guemos", ["2"] = "gueis", ["3"] = "guem" };
          sing = { ["1"] = "gue", ["2"] = "gues", ["3"] = "gue" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "garíamos", ["2"] = "garíeis", ["3"] = "gariam" };
          sing = { ["1"] = "garia", ["2"] = "garias", ["3"] = "garia" };
        };
        futu =
        {
          plur = { ["1"] = "garemos", ["2"] = "gareis", ["3"] = "garão" };
          sing = { ["1"] = "garei", ["2"] = "garás", ["3"] = "gará" };
        };
        impf =
        {
          plur = { ["1"] = "gávamos", ["2"] = "gáveis", ["3"] = "gavam" };
          sing = { ["1"] = "gava", ["2"] = "gavas", ["3"] = "gava" };
        };
        plpf =
        {
          plur = { ["1"] = "gáramos", ["2"] = "gáreis", ["3"] = "garam" };
          sing = { ["1"] = "gara", ["2"] = "garas", ["3"] = "gara" };
        };
        pres =
        {
          plur = { ["1"] = "gamos", ["2"] = "gais", ["3"] = "gam" };
          sing = { ["1"] = "go", ["2"] = "gas", ["3"] = "ga" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "gamos";
            ["1_alt"] = { "gámos" };
            ["2"] = "gastes";
            ["3"] = "garam";
          };
          sing = { ["1"] = "guei", ["2"] = "gaste", ["3"] = "gou" };
        };
      };
      infn =
      {
        impe = "gar";
        pers =
        {
          plur = { ["1"] = "garmos", ["2"] = "gardes", ["3"] = "garem" };
          sing = { ["1"] = "gar", ["2"] = "gares", ["3"] = "gar" };
        };
      };
      part_past =
      {
        plur = { f = "gadas", m = "gados" };
        sing = { f = "gada", m = "gado" };
      };
      part_pres = "gando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "garmos", ["2"] = "gardes", ["3"] = "garem" };
          sing = { ["1"] = "gar", ["2"] = "gares", ["3"] = "gar" };
        };
        impf =
        {
          plur = { ["1"] = "gássemos", ["2"] = "gásseis", ["3"] = "gassem" };
          sing = { ["1"] = "gasse", ["2"] = "gasses", ["3"] = "gasse" };
        };
        pres =
        {
          plur = { ["1"] = "guemos", ["2"] = "gueis", ["3"] = "guem" };
          sing = { ["1"] = "gue", ["2"] = "gues", ["3"] = "gue" };
        };
      };
    };
    suffix = "ar";
    verb = "gar";
  };
  gastar =
  {
    comments = { };
    examples = { "gastar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "gastemos", ["2"] = "gastai", ["3"] = "gastem" };
          sing = { ["2"] = "gasta", ["3"] = "gaste" };
        };
        negt =
        {
          plur = { ["1"] = "gastemos", ["2"] = "gasteis", ["3"] = "gastem" };
          sing = { ["2"] = "gastes", ["3"] = "gaste" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "gastaríamos", ["2"] = "gastaríeis", ["3"] = "gastariam" };
          sing = { ["1"] = "gastaria", ["2"] = "gastarias", ["3"] = "gastaria" };
        };
        futu =
        {
          plur = { ["1"] = "gastaremos", ["2"] = "gastareis", ["3"] = "gastarão" };
          sing = { ["1"] = "gastarei", ["2"] = "gastarás", ["3"] = "gastará" };
        };
        impf =
        {
          plur = { ["1"] = "gastávamos", ["2"] = "gastáveis", ["3"] = "gastavam" };
          sing = { ["1"] = "gastava", ["2"] = "gastavas", ["3"] = "gastava" };
        };
        plpf =
        {
          plur = { ["1"] = "gastáramos", ["2"] = "gastáreis", ["3"] = "gastaram" };
          sing = { ["1"] = "gastara", ["2"] = "gastaras", ["3"] = "gastara" };
        };
        pres =
        {
          plur = { ["1"] = "gastamos", ["2"] = "gastais", ["3"] = "gastam" };
          sing = { ["1"] = "gasto", ["2"] = "gastas", ["3"] = "gasta" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "gastamos";
            ["1_alt"] = { "gastámos" };
            ["2"] = "gastastes";
            ["3"] = "gastaram";
          };
          sing = { ["1"] = "gastei", ["2"] = "gastaste", ["3"] = "gastou" };
        };
      };
      infn =
      {
        impe = "gastar";
        pers =
        {
          plur = { ["1"] = "gastarmos", ["2"] = "gastardes", ["3"] = "gastarem" };
          sing = { ["1"] = "gastar", ["2"] = "gastares", ["3"] = "gastar" };
        };
      };
      long_part_past =
      {
        plur = { f = "gastadas", m = "gastados" };
        sing = { f = "gastada", m = "gastado" };
      };
      part_pres = "gastando";
      short_part_past =
      {
        plur = { f = "gastas", m = "gastos" };
        sing = { f = "gasta", m = "gasto" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "gastarmos", ["2"] = "gastardes", ["3"] = "gastarem" };
          sing = { ["1"] = "gastar", ["2"] = "gastares", ["3"] = "gastar" };
        };
        impf =
        {
          plur = { ["1"] = "gastássemos", ["2"] = "gastásseis", ["3"] = "gastassem" };
          sing = { ["1"] = "gastasse", ["2"] = "gastasses", ["3"] = "gastasse" };
        };
        pres =
        {
          plur = { ["1"] = "gastemos", ["2"] = "gasteis", ["3"] = "gastem" };
          sing = { ["1"] = "gaste", ["2"] = "gastes", ["3"] = "gaste" };
        };
      };
    };
    suffix = "ar";
    verb = "gastar";
  };
  gear =
  {
    comments = { };
    defective = true;
    examples = { "gear" };
    forms =
    {
      indi =
      {
        cond =
        {
          sing = { ["3"] = "gearia" };
        };
        futu =
        {
          sing = { ["3"] = "geará" };
        };
        impf =
        {
          sing = { ["3"] = "geava" };
        };
        plpf =
        {
          sing = { ["3"] = "geara" };
        };
        pres =
        {
          sing = { ["3"] = "geia" };
        };
        pret =
        {
          sing = { ["3"] = "geou" };
        };
      };
      infn =
      {
        impe = "gear";
        pers =
        {
          sing = { ["3"] = "gear" };
        };
      };
      part_past =
      {
        plur = { f = "geadas", m = "geados" };
        sing = { f = "geada", m = "geado" };
      };
      part_pres = "geando";
      subj =
      {
        futu =
        {
          sing = { ["3"] = "gear" };
        };
        impf =
        {
          sing = { ["3"] = "geasse" };
        };
        pres =
        {
          sing = { ["3"] = "geie" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "gear";
  };
  ger =
  {
    comments =
    {
      "The last letter ''g'' is changed to ''j'', depending on the following vowel, resulting in these four possible sequences: -ja-, -ge-, -gi- and -jo-.";
    };
    examples = { "eleger", "proteger", "reger" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "jamos", ["2"] = "gei", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "ge", ["3"] = "ja" };
        };
        negt =
        {
          plur = { ["1"] = "jamos", ["2"] = "jais", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "jas", ["3"] = "ja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "geríamos", ["2"] = "geríeis", ["3"] = "geriam" };
          sing = { ["1"] = "geria", ["2"] = "gerias", ["3"] = "geria" };
        };
        futu =
        {
          plur = { ["1"] = "geremos", ["2"] = "gereis", ["3"] = "gerão" };
          sing = { ["1"] = "gerei", ["2"] = "gerás", ["3"] = "gerá" };
        };
        impf =
        {
          plur = { ["1"] = "gíamos", ["2"] = "gíeis", ["3"] = "giam" };
          sing = { ["1"] = "gia", ["2"] = "gias", ["3"] = "gia" };
        };
        plpf =
        {
          plur = { ["1"] = "gêramos", ["2"] = "gêreis", ["3"] = "geram" };
          sing = { ["1"] = "gera", ["2"] = "geras", ["3"] = "gera" };
        };
        pres =
        {
          plur = { ["1"] = "gemos", ["2"] = "geis", ["3"] = "gem" };
          sing = { ["1"] = "jo", ["2"] = "ges", ["3"] = "ge" };
        };
        pret =
        {
          plur = { ["1"] = "gemos", ["2"] = "gestes", ["3"] = "geram" };
          sing = { ["1"] = "gi", ["2"] = "geste", ["3"] = "geu" };
        };
      };
      infn =
      {
        impe = "ger";
        pers =
        {
          plur = { ["1"] = "germos", ["2"] = "gerdes", ["3"] = "gerem" };
          sing = { ["1"] = "ger", ["2"] = "geres", ["3"] = "ger" };
        };
      };
      part_past =
      {
        plur = { f = "gidas", m = "gidos" };
        sing = { f = "gida", m = "gido" };
      };
      part_pres = "gendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "germos", ["2"] = "gerdes", ["3"] = "gerem" };
          sing = { ["1"] = "ger", ["2"] = "geres", ["3"] = "ger" };
        };
        impf =
        {
          plur = { ["1"] = "gêssemos", ["2"] = "gêsseis", ["3"] = "gessem" };
          sing = { ["1"] = "gesse", ["2"] = "gesses", ["3"] = "gesse" };
        };
        pres =
        {
          plur = { ["1"] = "jamos", ["2"] = "jais", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "jas", ["3"] = "ja" };
        };
      };
    };
    suffix = "er";
    verb = "ger";
  };
  gir =
  {
    comments =
    {
      "The last letter ''g'' is changed to ''j'', depending on the following vowel, resulting in these four possible sequences: -ja-, -ge-, -gi- and -jo-.";
    };
    examples = { "agir", "exigir", "dirigir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "jamos", ["2"] = "gi", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "ge", ["3"] = "ja" };
        };
        negt =
        {
          plur = { ["1"] = "jamos", ["2"] = "jais", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "jas", ["3"] = "ja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "giríamos", ["2"] = "giríeis", ["3"] = "giriam" };
          sing = { ["1"] = "giria", ["2"] = "girias", ["3"] = "giria" };
        };
        futu =
        {
          plur = { ["1"] = "giremos", ["2"] = "gireis", ["3"] = "girão" };
          sing = { ["1"] = "girei", ["2"] = "girás", ["3"] = "girá" };
        };
        impf =
        {
          plur = { ["1"] = "gíamos", ["2"] = "gíeis", ["3"] = "giam" };
          sing = { ["1"] = "gia", ["2"] = "gias", ["3"] = "gia" };
        };
        plpf =
        {
          plur = { ["1"] = "gíramos", ["2"] = "gíreis", ["3"] = "giram" };
          sing = { ["1"] = "gira", ["2"] = "giras", ["3"] = "gira" };
        };
        pres =
        {
          plur = { ["1"] = "gimos", ["2"] = "gis", ["3"] = "gem" };
          sing = { ["1"] = "jo", ["2"] = "ges", ["3"] = "ge" };
        };
        pret =
        {
          plur = { ["1"] = "gimos", ["2"] = "gistes", ["3"] = "giram" };
          sing = { ["1"] = "gi", ["2"] = "giste", ["3"] = "giu" };
        };
      };
      infn =
      {
        impe = "gir";
        pers =
        {
          plur = { ["1"] = "girmos", ["2"] = "girdes", ["3"] = "girem" };
          sing = { ["1"] = "gir", ["2"] = "gires", ["3"] = "gir" };
        };
      };
      part_past =
      {
        plur = { f = "gidas", m = "gidos" };
        sing = { f = "gida", m = "gido" };
      };
      part_pres = "gindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "girmos", ["2"] = "girdes", ["3"] = "girem" };
          sing = { ["1"] = "gir", ["2"] = "gires", ["3"] = "gir" };
        };
        impf =
        {
          plur = { ["1"] = "gíssemos", ["2"] = "gísseis", ["3"] = "gissem" };
          sing = { ["1"] = "gisse", ["2"] = "gisses", ["3"] = "gisse" };
        };
        pres =
        {
          plur = { ["1"] = "jamos", ["2"] = "jais", ["3"] = "jam" };
          sing = { ["1"] = "ja", ["2"] = "jas", ["3"] = "ja" };
        };
      };
    };
    suffix = "ir";
    verb = "gir";
  };
  gredir =
  {
    comments = { };
    examples = { "progredir", "regredir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "gridamos", ["2"] = "gredi", ["3"] = "gridam" };
          sing = { ["2"] = "gride", ["3"] = "grida" };
        };
        negt =
        {
          plur = { ["1"] = "gridamos", ["2"] = "gridais", ["3"] = "gridam" };
          sing = { ["2"] = "gridas", ["3"] = "grida" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "grediríamos", ["2"] = "grediríeis", ["3"] = "grediriam" };
          sing = { ["1"] = "grediria", ["2"] = "gredirias", ["3"] = "grediria" };
        };
        futu =
        {
          plur = { ["1"] = "grediremos", ["2"] = "gredireis", ["3"] = "gredirão" };
          sing = { ["1"] = "gredirei", ["2"] = "gredirás", ["3"] = "gredirá" };
        };
        impf =
        {
          plur = { ["1"] = "gredíamos", ["2"] = "gredíeis", ["3"] = "grediam" };
          sing = { ["1"] = "gredia", ["2"] = "gredias", ["3"] = "gredia" };
        };
        plpf =
        {
          plur = { ["1"] = "gredíramos", ["2"] = "gredíreis", ["3"] = "grediram" };
          sing = { ["1"] = "gredira", ["2"] = "grediras", ["3"] = "gredira" };
        };
        pres =
        {
          plur = { ["1"] = "gredimos", ["2"] = "gredis", ["3"] = "gridem" };
          sing = { ["1"] = "grido", ["2"] = "grides", ["3"] = "gride" };
        };
        pret =
        {
          plur = { ["1"] = "gredimos", ["2"] = "gredistes", ["3"] = "grediram" };
          sing = { ["1"] = "gredi", ["2"] = "grediste", ["3"] = "grediu" };
        };
      };
      infn =
      {
        impe = "gredir";
        pers =
        {
          plur = { ["1"] = "gredirmos", ["2"] = "gredirdes", ["3"] = "gredirem" };
          sing = { ["1"] = "gredir", ["2"] = "gredires", ["3"] = "gredir" };
        };
      };
      part_past =
      {
        plur = { f = "gredidas", m = "gredidos" };
        sing = { f = "gredida", m = "gredido" };
      };
      part_pres = "gredindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "gredirmos", ["2"] = "gredirdes", ["3"] = "gredirem" };
          sing = { ["1"] = "gredir", ["2"] = "gredires", ["3"] = "gredir" };
        };
        impf =
        {
          plur = { ["1"] = "gredíssemos", ["2"] = "gredísseis", ["3"] = "gredissem" };
          sing = { ["1"] = "gredisse", ["2"] = "gredisses", ["3"] = "gredisse" };
        };
        pres =
        {
          plur = { ["1"] = "gridamos", ["2"] = "gridais", ["3"] = "gridam" };
          sing = { ["1"] = "grida", ["2"] = "gridas", ["3"] = "grida" };
        };
      };
    };
    suffix = "ir";
    verb = "gredir";
  };
  guer =
  {
    comments =
    {
      "The last letter ''gu'' is changed to ''g'', depending on the following vowel, resulting in these four possible sequences: -ga-, -gue-, -gui- and -go-.";
    };
    examples = { "erguer", "reerguer", "soerguer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "gamos", ["2"] = "guei", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gue", ["3"] = "ga" };
        };
        negt =
        {
          plur = { ["1"] = "gamos", ["2"] = "gais", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gas", ["3"] = "ga" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "gueríamos", ["2"] = "gueríeis", ["3"] = "gueriam" };
          sing = { ["1"] = "gueria", ["2"] = "guerias", ["3"] = "gueria" };
        };
        futu =
        {
          plur = { ["1"] = "gueremos", ["2"] = "guereis", ["3"] = "guerão" };
          sing = { ["1"] = "guerei", ["2"] = "guerás", ["3"] = "guerá" };
        };
        impf =
        {
          plur = { ["1"] = "guíamos", ["2"] = "guíeis", ["3"] = "guiam" };
          sing = { ["1"] = "guia", ["2"] = "guias", ["3"] = "guia" };
        };
        plpf =
        {
          plur = { ["1"] = "guêramos", ["2"] = "guêreis", ["3"] = "gueram" };
          sing = { ["1"] = "guera", ["2"] = "gueras", ["3"] = "guera" };
        };
        pres =
        {
          plur = { ["1"] = "guemos", ["2"] = "gueis", ["3"] = "guem" };
          sing = { ["1"] = "go", ["2"] = "gues", ["3"] = "gue" };
        };
        pret =
        {
          plur = { ["1"] = "guemos", ["2"] = "guestes", ["3"] = "gueram" };
          sing = { ["1"] = "gui", ["2"] = "gueste", ["3"] = "gueu" };
        };
      };
      infn =
      {
        impe = "guer";
        pers =
        {
          plur = { ["1"] = "guermos", ["2"] = "guerdes", ["3"] = "guerem" };
          sing = { ["1"] = "guer", ["2"] = "gueres", ["3"] = "guer" };
        };
      };
      part_past =
      {
        plur = { f = "guidas", m = "guidos" };
        sing = { f = "guida", m = "guido" };
      };
      part_pres = "guendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "guermos", ["2"] = "guerdes", ["3"] = "guerem" };
          sing = { ["1"] = "guer", ["2"] = "gueres", ["3"] = "guer" };
        };
        impf =
        {
          plur = { ["1"] = "guêssemos", ["2"] = "guêsseis", ["3"] = "guessem" };
          sing = { ["1"] = "guesse", ["2"] = "guesses", ["3"] = "guesse" };
        };
        pres =
        {
          plur = { ["1"] = "gamos", ["2"] = "gais", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gas", ["3"] = "ga" };
        };
      };
    };
    suffix = "er";
    verb = "guer";
  };
  guir =
  {
    comments =
    {
      "The last letter combination ''gu'' is changed to ''g'', depending on the following vowel, resulting in these four possible sequences: -ga-, -gue-, -gui- and -go-.";
    };
    examples = { "conseguir", "distinguir", "seguir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "gamos", ["2"] = "gui", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gue", ["3"] = "ga" };
        };
        negt =
        {
          plur = { ["1"] = "gamos", ["2"] = "gais", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gas", ["3"] = "ga" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "guiríamos", ["2"] = "guiríeis", ["3"] = "guiriam" };
          sing = { ["1"] = "guiria", ["2"] = "guirias", ["3"] = "guiria" };
        };
        futu =
        {
          plur = { ["1"] = "guiremos", ["2"] = "guireis", ["3"] = "guirão" };
          sing = { ["1"] = "guirei", ["2"] = "guirás", ["3"] = "guirá" };
        };
        impf =
        {
          plur = { ["1"] = "guíamos", ["2"] = "guíeis", ["3"] = "guiam" };
          sing = { ["1"] = "guia", ["2"] = "guias", ["3"] = "guia" };
        };
        plpf =
        {
          plur = { ["1"] = "guíramos", ["2"] = "guíreis", ["3"] = "guiram" };
          sing = { ["1"] = "guira", ["2"] = "guiras", ["3"] = "guira" };
        };
        pres =
        {
          plur = { ["1"] = "guimos", ["2"] = "guis", ["3"] = "guem" };
          sing = { ["1"] = "go", ["2"] = "gues", ["3"] = "gue" };
        };
        pret =
        {
          plur = { ["1"] = "guimos", ["2"] = "guistes", ["3"] = "guiram" };
          sing = { ["1"] = "gui", ["2"] = "guiste", ["3"] = "guiu" };
        };
      };
      infn =
      {
        impe = "guir";
        pers =
        {
          plur = { ["1"] = "guirmos", ["2"] = "guirdes", ["3"] = "guirem" };
          sing = { ["1"] = "guir", ["2"] = "guires", ["3"] = "guir" };
        };
      };
      part_past =
      {
        plur = { f = "guidas", m = "guidos" };
        sing = { f = "guida", m = "guido" };
      };
      part_pres = "guindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "guirmos", ["2"] = "guirdes", ["3"] = "guirem" };
          sing = { ["1"] = "guir", ["2"] = "guires", ["3"] = "guir" };
        };
        impf =
        {
          plur = { ["1"] = "guíssemos", ["2"] = "guísseis", ["3"] = "guissem" };
          sing = { ["1"] = "guisse", ["2"] = "guisses", ["3"] = "guisse" };
        };
        pres =
        {
          plur = { ["1"] = "gamos", ["2"] = "gais", ["3"] = "gam" };
          sing = { ["1"] = "ga", ["2"] = "gas", ["3"] = "ga" };
        };
      };
    };
    suffix = "ir";
    verb = "guir";
  };
  haver =
  {
    comments = { };
    examples = { "haver" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "hajamos", ["2"] = "havei", ["3"] = "hajam" };
          sing = { ["1"] = "haja", ["2"] = "há", ["3"] = "haja" };
        };
        negt =
        {
          plur = { ["1"] = "hajamos", ["2"] = "hajais", ["3"] = "hajam" };
          sing = { ["1"] = "haja", ["2"] = "hajas", ["3"] = "haja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "haveríamos", ["2"] = "haveríeis", ["3"] = "haveriam" };
          sing = { ["1"] = "haveria", ["2"] = "haverias", ["3"] = "haveria" };
        };
        futu =
        {
          plur = { ["1"] = "haveremos", ["2"] = "havereis", ["3"] = "haverão" };
          sing = { ["1"] = "haverei", ["2"] = "haverás", ["3"] = "haverá" };
        };
        impf =
        {
          plur = { ["1"] = "havíamos", ["2"] = "havíeis", ["3"] = "haviam" };
          sing = { ["1"] = "havia", ["2"] = "havias", ["3"] = "havia" };
        };
        plpf =
        {
          plur = { ["1"] = "houvéramos", ["2"] = "houvéreis", ["3"] = "houveram" };
          sing = { ["1"] = "houvera", ["2"] = "houveras", ["3"] = "houvera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "havemos";
            ["1_alt"] = { "hemos" };
            ["2"] = "haveis";
            ["2_alt"] = { "heis" };
            ["3"] = "hão";
          };
          sing = { ["1"] = "hei", ["2"] = "hás", ["3"] = "há" };
        };
        pret =
        {
          plur = { ["1"] = "houvemos", ["2"] = "houvestes", ["3"] = "houveram" };
          sing = { ["1"] = "houve", ["2"] = "houveste", ["3"] = "houve" };
        };
      };
      infn =
      {
        impe = "haver";
        pers =
        {
          plur = { ["1"] = "havermos", ["2"] = "haverdes", ["3"] = "haverem" };
          sing = { ["1"] = "haver", ["2"] = "haveres", ["3"] = "haver" };
        };
      };
      part_past =
      {
        plur = { f = "havidas", m = "havidos" };
        sing = { f = "havida", m = "havido" };
      };
      part_pres = "havendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "houvermos", ["2"] = "houverdes", ["3"] = "houverem" };
          sing = { ["1"] = "houver", ["2"] = "houveres", ["3"] = "houver" };
        };
        impf =
        {
          plur = { ["1"] = "houvéssemos", ["2"] = "houvésseis", ["3"] = "houvessem" };
          sing = { ["1"] = "houvesse", ["2"] = "houvesses", ["3"] = "houvesse" };
        };
        pres =
        {
          plur = { ["1"] = "hajamos", ["2"] = "hajais", ["3"] = "hajam" };
          sing = { ["1"] = "haja", ["2"] = "hajas", ["3"] = "haja" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "haver";
  };
  iar =
  {
    comments = { };
    examples = { "ansiar", "incendiar", "mediar", "odiar", "remediar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "iemos", ["2"] = "iai", ["3"] = "eiem" };
          sing = { ["1"] = "ie", ["2"] = "eia", ["3"] = "ie" };
        };
        negt =
        {
          plur = { ["1"] = "iemos", ["2"] = "ieis", ["3"] = "iem" };
          sing = { ["1"] = "ie", ["2"] = "ies", ["3"] = "ie" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "iaríamos", ["2"] = "iaríeis", ["3"] = "iariam" };
          sing = { ["1"] = "iaria", ["2"] = "iarias", ["3"] = "iaria" };
        };
        futu =
        {
          plur = { ["1"] = "iaremos", ["2"] = "iareis", ["3"] = "iarão" };
          sing = { ["1"] = "iarei", ["2"] = "iarás", ["3"] = "iará" };
        };
        impf =
        {
          plur = { ["1"] = "iávamos", ["2"] = "iáveis", ["3"] = "iavam" };
          sing = { ["1"] = "iava", ["2"] = "iavas", ["3"] = "iava" };
        };
        plpf =
        {
          plur = { ["1"] = "iáramos", ["2"] = "iáreis", ["3"] = "iaram" };
          sing = { ["1"] = "iara", ["2"] = "iaras", ["3"] = "iara" };
        };
        pres =
        {
          plur = { ["1"] = "iamos", ["2"] = "iais", ["3"] = "eiam" };
          sing = { ["1"] = "eio", ["2"] = "eias", ["3"] = "eia" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "iamos";
            ["1_alt"] = { "iámos" };
            ["2"] = "iastes";
            ["3"] = "iaram";
          };
          sing = { ["1"] = "iei", ["2"] = "iaste", ["3"] = "iou" };
        };
      };
      infn =
      {
        impe = "iar";
        pers =
        {
          plur = { ["1"] = "iarmos", ["2"] = "iardes", ["3"] = "iarem" };
          sing = { ["1"] = "iar", ["2"] = "iares", ["3"] = "iar" };
        };
      };
      part_past =
      {
        plur = { f = "iadas", m = "iados" };
        sing = { f = "iada", m = "iado" };
      };
      part_pres = "iando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "iarmos", ["2"] = "iardes", ["3"] = "iarem" };
          sing = { ["1"] = "iar", ["2"] = "iares", ["3"] = "iar" };
        };
        impf =
        {
          plur = { ["1"] = "iássemos", ["2"] = "iásseis", ["3"] = "iassem" };
          sing = { ["1"] = "iasse", ["2"] = "iasses", ["3"] = "iasse" };
        };
        pres =
        {
          plur = { ["1"] = "iemos", ["2"] = "ieis", ["3"] = "eiem" };
          sing = { ["1"] = "eie", ["2"] = "eies", ["3"] = "eie" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "iar";
  };
  ibir =
  {
    comments = { };
    examples = { "proibir", "coibir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "íbamos", ["2"] = "íbi", ["3"] = "íbam" };
          sing = { ["2"] = "íbe", ["3"] = "íba" };
        };
        negt =
        {
          plur = { ["1"] = "íbamos", ["2"] = "íbais", ["3"] = "íbam" };
          sing = { ["2"] = "íbas", ["3"] = "íba" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ibiríamos", ["2"] = "ibiríeis", ["3"] = "ibiriam" };
          sing = { ["1"] = "ibiria", ["2"] = "ibirias", ["3"] = "ibiria" };
        };
        futu =
        {
          plur = { ["1"] = "ibiremos", ["2"] = "ibireis", ["3"] = "ibirão" };
          sing = { ["1"] = "ibirei", ["2"] = "ibirás", ["3"] = "ibirá" };
        };
        impf =
        {
          plur = { ["1"] = "ibíamos", ["2"] = "ibíeis", ["3"] = "ibiam" };
          sing = { ["1"] = "ibia", ["2"] = "ibias", ["3"] = "ibia" };
        };
        plpf =
        {
          plur = { ["1"] = "ibíramos", ["2"] = "ibíreis", ["3"] = "ibiram" };
          sing = { ["1"] = "ibira", ["2"] = "ibiras", ["3"] = "ibira" };
        };
        pres =
        {
          plur = { ["1"] = "ibimos", ["2"] = "ibis", ["3"] = "íbem" };
          sing = { ["1"] = "íbo", ["2"] = "íbes", ["3"] = "íbe" };
        };
        pret =
        {
          plur = { ["1"] = "ibimos", ["2"] = "ibistes", ["3"] = "ibiram" };
          sing = { ["1"] = "ibi", ["2"] = "ibiste", ["3"] = "ibiu" };
        };
      };
      infn =
      {
        impe = "ibir";
        pers =
        {
          plur = { ["1"] = "ibirmos", ["2"] = "ibirdes", ["3"] = "ibirem" };
          sing = { ["1"] = "ibir", ["2"] = "ibires", ["3"] = "ibir" };
        };
      };
      part_past =
      {
        plur = { f = "ibidas", m = "ibidos" };
        sing = { f = "ibida", m = "ibido" };
      };
      part_pres = "ibindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ibirmos", ["2"] = "ibirdes", ["3"] = "ibirem" };
          sing = { ["1"] = "ibir", ["2"] = "ibires", ["3"] = "ibir" };
        };
        impf =
        {
          plur = { ["1"] = "ibíssemos", ["2"] = "ibísseis", ["3"] = "ibissem" };
          sing = { ["1"] = "ibisse", ["2"] = "ibisses", ["3"] = "ibisse" };
        };
        pres =
        {
          plur = { ["1"] = "íbamos", ["2"] = "íbais", ["3"] = "íbam" };
          sing = { ["1"] = "íba", ["2"] = "íbas", ["3"] = "íba" };
        };
      };
    };
    suffix = "ir";
    verb = "ibir";
  };
  incluir =
  {
    comments = { };
    examples = { "incluir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "incluamos", ["2"] = "incluí", ["3"] = "incluam" };
          sing = { ["2"] = "inclui", ["3"] = "inclua" };
        };
        negt =
        {
          plur = { ["1"] = "incluamos", ["2"] = "incluais", ["3"] = "incluam" };
          sing = { ["2"] = "incluas", ["3"] = "inclua" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "incluiríamos";
            ["2"] = "incluiríeis";
            ["3"] = "incluiriam";
          };
          sing = { ["1"] = "incluiria", ["2"] = "incluirias", ["3"] = "incluiria" };
        };
        futu =
        {
          plur = { ["1"] = "incluiremos", ["2"] = "incluireis", ["3"] = "incluirão" };
          sing = { ["1"] = "incluirei", ["2"] = "incluirás", ["3"] = "incluirá" };
        };
        impf =
        {
          plur = { ["1"] = "incluíamos", ["2"] = "incluíeis", ["3"] = "incluíam" };
          sing = { ["1"] = "incluía", ["2"] = "incluías", ["3"] = "incluía" };
        };
        plpf =
        {
          plur = { ["1"] = "incluíramos", ["2"] = "incluíreis", ["3"] = "incluíram" };
          sing = { ["1"] = "incluíra", ["2"] = "incluíras", ["3"] = "incluíra" };
        };
        pres =
        {
          plur = { ["1"] = "incluímos", ["2"] = "incluís", ["3"] = "incluem" };
          sing = { ["1"] = "incluo", ["2"] = "incluis", ["3"] = "inclui" };
        };
        pret =
        {
          plur = { ["1"] = "incluímos", ["2"] = "incluístes", ["3"] = "incluíram" };
          sing = { ["1"] = "incluí", ["2"] = "incluíste", ["3"] = "incluiu" };
        };
      };
      infn =
      {
        impe = "incluir";
        pers =
        {
          plur = { ["1"] = "incluirmos", ["2"] = "incluirdes", ["3"] = "incluírem" };
          sing = { ["1"] = "incluir", ["2"] = "incluíres", ["3"] = "incluir" };
        };
      };
      long_part_past =
      {
        plur = { f = "incluídas", m = "incluídos" };
        sing = { f = "incluída", m = "incluído" };
      };
      part_pres = "incluindo";
      short_part_past =
      {
        plur = { f = "inclusas", m = "inclusos" };
        sing = { f = "inclusa", m = "incluso" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "incluirmos", ["2"] = "incluirdes", ["3"] = "incluírem" };
          sing = { ["1"] = "incluir", ["2"] = "incluíres", ["3"] = "incluir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "incluíssemos";
            ["2"] = "incluísseis";
            ["3"] = "incluíssem";
          };
          sing = { ["1"] = "incluísse", ["2"] = "incluísses", ["3"] = "incluísse" };
        };
        pres =
        {
          plur = { ["1"] = "incluamos", ["2"] = "incluais", ["3"] = "incluam" };
          sing = { ["1"] = "inclua", ["2"] = "incluas", ["3"] = "inclua" };
        };
      };
    };
    suffix = "ir";
    verb = "incluir";
  };
  indenizar =
  {
    comments = { "An obsolete treatment is to add a letter ''m'' before the last ''n''." };
    examples = { "indenizar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "indenizemos";
            ["1_obsolete"] = "indemnizemos";
            ["2"] = "indenizai";
            ["2_obsolete"] = "indemnizai";
            ["3"] = "indenizem";
            ["3_obsolete"] = "indemnizem";
          };
          sing =
          {
            ["1"] = "indenize";
            ["1_obsolete"] = "indemnize";
            ["2"] = "indeniza";
            ["2_obsolete"] = "indemniza";
            ["3"] = "indenize";
            ["3_obsolete"] = "indemnize";
          };
        };
        negt =
        {
          plur =
          {
            ["1"] = "indenizemos";
            ["1_obsolete"] = "indemnizemos";
            ["2"] = "indenizeis";
            ["2_obsolete"] = "indemnizeis";
            ["3"] = "indenizem";
            ["3_obsolete"] = "indemnizem";
          };
          sing =
          {
            ["1"] = "indenize";
            ["1_obsolete"] = "indemnize";
            ["2"] = "indenizes";
            ["2_obsolete"] = "indemnizes";
            ["3"] = "indenize";
            ["3_obsolete"] = "indemnize";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "indenizaríamos";
            ["1_obsolete"] = "indemnizaríamos";
            ["2"] = "indenizaríeis";
            ["2_obsolete"] = "indemnizaríeis";
            ["3"] = "indenizariam";
            ["3_obsolete"] = "indemnizariam";
          };
          sing =
          {
            ["1"] = "indenizaria";
            ["1_obsolete"] = "indemnizaria";
            ["2"] = "indenizarias";
            ["2_obsolete"] = "indemnizarias";
            ["3"] = "indenizaria";
            ["3_obsolete"] = "indemnizaria";
          };
        };
        futu =
        {
          plur =
          {
            ["1"] = "indenizaremos";
            ["1_obsolete"] = "indemnizaremos";
            ["2"] = "indenizareis";
            ["2_obsolete"] = "indemnizareis";
            ["3"] = "indenizarão";
            ["3_obsolete"] = "indemnizarão";
          };
          sing =
          {
            ["1"] = "indenizarei";
            ["1_obsolete"] = "indemnizarei";
            ["2"] = "indenizarás";
            ["2_obsolete"] = "indemnizarás";
            ["3"] = "indenizará";
            ["3_obsolete"] = "indemnizará";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "indenizávamos";
            ["1_obsolete"] = "indemnizávamos";
            ["2"] = "indenizáveis";
            ["2_obsolete"] = "indemnizáveis";
            ["3"] = "indenizavam";
            ["3_obsolete"] = "indemnizavam";
          };
          sing =
          {
            ["1"] = "indenizava";
            ["1_obsolete"] = "indemnizava";
            ["2"] = "indenizavas";
            ["2_obsolete"] = "indemnizavas";
            ["3"] = "indenizava";
            ["3_obsolete"] = "indemnizava";
          };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "indenizáramos";
            ["1_obsolete"] = "indemnizáramos";
            ["2"] = "indenizáreis";
            ["2_obsolete"] = "indemnizáreis";
            ["3"] = "indenizaram";
            ["3_obsolete"] = "indemnizaram";
          };
          sing =
          {
            ["1"] = "indenizara";
            ["1_obsolete"] = "indemnizara";
            ["2"] = "indenizaras";
            ["2_obsolete"] = "indemnizaras";
            ["3"] = "indenizara";
            ["3_obsolete"] = "indemnizara";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "indenizamos";
            ["1_obsolete"] = "indemnizamos";
            ["2"] = "indenizais";
            ["2_obsolete"] = "indemnizais";
            ["3"] = "indenizam";
            ["3_obsolete"] = "indemnizam";
          };
          sing =
          {
            ["1"] = "indenizo";
            ["1_obsolete"] = "indemnizo";
            ["2"] = "indenizas";
            ["2_obsolete"] = "indemnizas";
            ["3"] = "indeniza";
            ["3_obsolete"] = "indemniza";
          };
        };
        pret =
        {
          plur =
          {
            ["1"] = "indenizamos";
            ["1_alt"] = { "indenizámos" };
            ["1_obsolete"] = "indemnizámos";
            ["2"] = "indenizastes";
            ["2_obsolete"] = "indemnizastes";
            ["3"] = "indenizaram";
            ["3_obsolete"] = "indemnizaram";
          };
          sing =
          {
            ["1"] = "indenizei";
            ["1_obsolete"] = "indemnizei";
            ["2"] = "indenizaste";
            ["2_obsolete"] = "indemnizaste";
            ["3"] = "indenizou";
            ["3_obsolete"] = "indemnizou";
          };
        };
      };
      infn =
      {
        impe = "indemnizar";
        pers =
        {
          plur =
          {
            ["1"] = "indenizarmos";
            ["1_obsolete"] = "indemnizarmos";
            ["2"] = "indenizardes";
            ["2_obsolete"] = "indemnizardes";
            ["3"] = "indenizarem";
            ["3_obsolete"] = "indemnizarem";
          };
          sing =
          {
            ["1"] = "indenizar";
            ["1_obsolete"] = "indemnizar";
            ["2"] = "indenizares";
            ["2_obsolete"] = "indemnizares";
            ["3"] = "indenizar";
            ["3_obsolete"] = "indemnizar";
          };
        };
      };
      part_past =
      {
        plur = { f = "indemnizadas", m = "indemnizados" };
        sing = { f = "indemnizada", m = "indemnizado" };
      };
      part_pres = "indemnizando";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1"] = "indenizarmos";
            ["1_obsolete"] = "indemnizarmos";
            ["2"] = "indenizardes";
            ["2_obsolete"] = "indemnizardes";
            ["3"] = "indenizarem";
            ["3_obsolete"] = "indemnizarem";
          };
          sing =
          {
            ["1"] = "indenizar";
            ["1_obsolete"] = "indemnizar";
            ["2"] = "indenizares";
            ["2_obsolete"] = "indemnizares";
            ["3"] = "indenizar";
            ["3_obsolete"] = "indemnizar";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "indenizássemos";
            ["1_obsolete"] = "indemnizássemos";
            ["2"] = "indenizásseis";
            ["2_obsolete"] = "indemnizásseis";
            ["3"] = "indenizassem";
            ["3_obsolete"] = "indemnizassem";
          };
          sing =
          {
            ["1"] = "indenizasse";
            ["1_obsolete"] = "indemnizasse";
            ["2"] = "indenizasses";
            ["2_obsolete"] = "indemnizasses";
            ["3"] = "indenizasse";
            ["3_obsolete"] = "indemnizasse";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "indenizemos";
            ["1_obsolete"] = "indemnizemos";
            ["2"] = "indenizeis";
            ["2_obsolete"] = "indemnizeis";
            ["3"] = "indenizem";
            ["3_obsolete"] = "indemnizem";
          };
          sing =
          {
            ["1"] = "indenize";
            ["1_obsolete"] = "indemnize";
            ["2"] = "indenizes";
            ["2_obsolete"] = "indemnizes";
            ["3"] = "indenize";
            ["3_obsolete"] = "indemnize";
          };
        };
      };
    };
    suffix = "ar";
    verb = "indenizar";
  };
  ir =
  {
    comments = { };
    examples = { "definir", "descobrir", "desistir", "garantir", "partir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "amos", ["2"] = "i", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "e", ["3"] = "a" };
        };
        negt =
        {
          plur = { ["1"] = "amos", ["2"] = "ais", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "as", ["3"] = "a" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "iríamos", ["2"] = "iríeis", ["3"] = "iriam" };
          sing = { ["1"] = "iria", ["2"] = "irias", ["3"] = "iria" };
        };
        futu =
        {
          plur = { ["1"] = "iremos", ["2"] = "ireis", ["3"] = "irão" };
          sing = { ["1"] = "irei", ["2"] = "irás", ["3"] = "irá" };
        };
        impf =
        {
          plur = { ["1"] = "íamos", ["2"] = "íeis", ["3"] = "iam" };
          sing = { ["1"] = "ia", ["2"] = "ias", ["3"] = "ia" };
        };
        plpf =
        {
          plur = { ["1"] = "íramos", ["2"] = "íreis", ["3"] = "iram" };
          sing = { ["1"] = "ira", ["2"] = "iras", ["3"] = "ira" };
        };
        pres =
        {
          plur = { ["1"] = "imos", ["2"] = "is", ["3"] = "em" };
          sing = { ["1"] = "o", ["2"] = "es", ["3"] = "e" };
        };
        pret =
        {
          plur = { ["1"] = "imos", ["2"] = "istes", ["3"] = "iram" };
          sing = { ["1"] = "i", ["2"] = "iste", ["3"] = "iu" };
        };
      };
      infn =
      {
        impe = "ir";
        pers =
        {
          plur = { ["1"] = "irmos", ["2"] = "irdes", ["3"] = "irem" };
          sing = { ["1"] = "ir", ["2"] = "ires", ["3"] = "ir" };
        };
      };
      part_past =
      {
        plur = { f = "idas", m = "idos" };
        sing = { f = "ida", m = "ido" };
      };
      part_pres = "indo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "irmos", ["2"] = "irdes", ["3"] = "irem" };
          sing = { ["1"] = "ir", ["2"] = "ires", ["3"] = "ir" };
        };
        impf =
        {
          plur = { ["1"] = "íssemos", ["2"] = "ísseis", ["3"] = "issem" };
          sing = { ["1"] = "isse", ["2"] = "isses", ["3"] = "isse" };
        };
        pres =
        {
          plur = { ["1"] = "amos", ["2"] = "ais", ["3"] = "am" };
          sing = { ["1"] = "a", ["2"] = "as", ["3"] = "a" };
        };
      };
    };
    suffix = "ir";
    verb = "ir";
  };
  ir2 =
  {
    abundant = false;
    comments = { };
    examples = { "ir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "vamos", ["2"] = "ide", ["3"] = "vão" };
          sing = { ["1"] = "vá", ["2"] = "vai", ["3"] = "vá" };
        };
        negt =
        {
          plur = { ["1"] = "vamos", ["2"] = "vades", ["3"] = "vão" };
          sing = { ["1"] = "vá", ["2"] = "vás", ["3"] = "vá" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "iríamos", ["2"] = "iríeis", ["3"] = "iriam" };
          sing = { ["1"] = "iria", ["2"] = "irias", ["3"] = "iria" };
        };
        futu =
        {
          plur = { ["1"] = "iremos", ["2"] = "ireis", ["3"] = "irão" };
          sing = { ["1"] = "irei", ["2"] = "irás", ["3"] = "irá" };
        };
        impf =
        {
          plur = { ["1"] = "íamos", ["2"] = "íeis", ["3"] = "iam" };
          sing = { ["1"] = "ia", ["2"] = "ias", ["3"] = "ia" };
        };
        plpf =
        {
          plur = { ["1"] = "fôramos", ["2"] = "fôreis", ["3"] = "foram" };
          sing = { ["1"] = "fora", ["2"] = "foras", ["3"] = "fora" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "vamos";
            ["1_alt"] = { "imos" };
            ["2"] = "ides";
            ["3"] = "vão";
          };
          sing = { ["1"] = "vou", ["2"] = "vais", ["3"] = "vai" };
        };
        pret =
        {
          plur = { ["1"] = "fomos", ["2"] = "fostes", ["3"] = "foram" };
          sing = { ["1"] = "fui", ["2"] = "foste", ["3"] = "foi" };
        };
      };
      infn =
      {
        impe = "ir";
        pers =
        {
          plur = { ["1"] = "irmos", ["2"] = "irdes", ["3"] = "irem" };
          sing = { ["1"] = "ir", ["2"] = "ires", ["3"] = "ir" };
        };
      };
      part_past =
      {
        plur = { f = "idas", m = "idos" };
        sing = { f = "ida", m = "ido" };
      };
      part_pres = "indo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "formos", ["2"] = "fordes", ["3"] = "forem" };
          sing = { ["1"] = "for", ["2"] = "fores", ["3"] = "for" };
        };
        impf =
        {
          plur = { ["1"] = "fôssemos", ["2"] = "fôsseis", ["3"] = "fossem" };
          sing = { ["1"] = "fosse", ["2"] = "fosses", ["3"] = "fosse" };
        };
        pres =
        {
          plur = { ["1"] = "vamos", ["2"] = "vades", ["3"] = "vão" };
          sing = { ["1"] = "vá", ["2"] = "vás", ["3"] = "vá" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "ir2";
  };
  latir =
  {
    comments =
    {
      "When the suffix of a conjugated form starts with ''a'' or ''o'', this conjugated form does not exist. However, this rule may be ignored, so the verb can also be fully conjugated.";
    };
    defective = true;
    examples = { "abolir", "explodir", "latir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "latamos";
            ["2"] = "lati";
            ["3_defective"] = "latam";
          };
          sing = { ["1_defective"] = "lata", ["2"] = "late", ["3_defective"] = "lata" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "latamos";
            ["2_defective"] = "latais";
            ["3_defective"] = "latam";
          };
          sing =
          {
            ["1_defective"] = "lata";
            ["2_defective"] = "latas";
            ["3_defective"] = "lata";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "latiríamos", ["2"] = "latiríeis", ["3"] = "latiriam" };
          sing = { ["1"] = "latiria", ["2"] = "latirias", ["3"] = "latiria" };
        };
        futu =
        {
          plur = { ["1"] = "latiremos", ["2"] = "latireis", ["3"] = "latirão" };
          sing = { ["1"] = "latirei", ["2"] = "latirás", ["3"] = "latirá" };
        };
        impf =
        {
          plur = { ["1"] = "latíamos", ["2"] = "latíeis", ["3"] = "latiam" };
          sing = { ["1"] = "latia", ["2"] = "latias", ["3"] = "latia" };
        };
        plpf =
        {
          plur = { ["1"] = "latíramos", ["2"] = "latíreis", ["3"] = "latiram" };
          sing = { ["1"] = "latira", ["2"] = "latiras", ["3"] = "latira" };
        };
        pres =
        {
          plur = { ["1"] = "latimos", ["2"] = "latis", ["3"] = "latem" };
          sing = { ["1_defective"] = "lato", ["2"] = "lates", ["3"] = "late" };
        };
        pret =
        {
          plur = { ["1"] = "latimos", ["2"] = "latistes", ["3"] = "latiram" };
          sing = { ["1"] = "lati", ["2"] = "latiste", ["3"] = "latiu" };
        };
      };
      infn =
      {
        impe = "latir";
        pers =
        {
          plur = { ["1"] = "latirmos", ["2"] = "latirdes", ["3"] = "latirem" };
          sing = { ["1"] = "latir", ["2"] = "latires", ["3"] = "latir" };
        };
      };
      part_past =
      {
        plur = { f = "latidas", m = "latidos" };
        sing = { f = "latida", m = "latido" };
      };
      part_pres = "latindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "latirmos", ["2"] = "latirdes", ["3"] = "latirem" };
          sing = { ["1"] = "latir", ["2"] = "latires", ["3"] = "latir" };
        };
        impf =
        {
          plur = { ["1"] = "latíssemos", ["2"] = "latísseis", ["3"] = "latissem" };
          sing = { ["1"] = "latisse", ["2"] = "latisses", ["3"] = "latisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "latamos";
            ["2_defective"] = "latais";
            ["3_defective"] = "latam";
          };
          sing =
          {
            ["1_defective"] = "lata";
            ["2_defective"] = "latas";
            ["3_defective"] = "lata";
          };
        };
      };
    };
    suffix = "ir";
    verb = "latir";
  };
  ler =
  {
    comments = { };
    examples = { "ler", "reler" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "leiamos", ["2"] = "lede", ["3"] = "leiam" };
          sing = { ["1"] = "leia", ["2"] = "lê", ["3"] = "leia" };
        };
        negt =
        {
          plur = { ["1"] = "leiamos", ["2"] = "leiais", ["3"] = "leiam" };
          sing = { ["1"] = "leia", ["2"] = "leias", ["3"] = "leia" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "leríamos", ["2"] = "leríeis", ["3"] = "leriam" };
          sing = { ["1"] = "leria", ["2"] = "lerias", ["3"] = "leria" };
        };
        futu =
        {
          plur = { ["1"] = "leremos", ["2"] = "lereis", ["3"] = "lerão" };
          sing = { ["1"] = "lerei", ["2"] = "lerás", ["3"] = "lerá" };
        };
        impf =
        {
          plur = { ["1"] = "líamos", ["2"] = "líeis", ["3"] = "liam" };
          sing = { ["1"] = "lia", ["2"] = "lias", ["3"] = "lia" };
        };
        plpf =
        {
          plur = { ["1"] = "lêramos", ["2"] = "lêreis", ["3"] = "leram" };
          sing = { ["1"] = "lera", ["2"] = "leras", ["3"] = "lera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "lemos";
            ["2"] = "ledes";
            ["3"] = "leem";
            ["3_obsolete"] = "lêem";
          };
          sing = { ["1"] = "leio", ["2"] = "lês", ["3"] = "lê" };
        };
        pret =
        {
          plur = { ["1"] = "lemos", ["2"] = "lestes", ["3"] = "leram" };
          sing = { ["1"] = "li", ["2"] = "leste", ["3"] = "leu" };
        };
      };
      infn =
      {
        impe = "ler";
        pers =
        {
          plur = { ["1"] = "lermos", ["2"] = "lerdes", ["3"] = "lerem" };
          sing = { ["1"] = "ler", ["2"] = "leres", ["3"] = "ler" };
        };
      };
      part_past =
      {
        plur = { f = "lidas", m = "lidos" };
        sing = { f = "lida", m = "lido" };
      };
      part_pres = "lendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "lermos", ["2"] = "lerdes", ["3"] = "lerem" };
          sing = { ["1"] = "ler", ["2"] = "leres", ["3"] = "ler" };
        };
        impf =
        {
          plur = { ["1"] = "lêssemos", ["2"] = "lêsseis", ["3"] = "lessem" };
          sing = { ["1"] = "lesse", ["2"] = "lesses", ["3"] = "lesse" };
        };
        pres =
        {
          plur = { ["1"] = "leiamos", ["2"] = "leiais", ["3"] = "leiam" };
          sing = { ["1"] = "leia", ["2"] = "leias", ["3"] = "leia" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "ler";
  };
  matar =
  {
    comments = { };
    examples = { "matar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "matemos", ["2"] = "matai", ["3"] = "matem" };
          sing = { ["2"] = "mata", ["3"] = "mate" };
        };
        negt =
        {
          plur = { ["1"] = "matemos", ["2"] = "mateis", ["3"] = "matem" };
          sing = { ["2"] = "mates", ["3"] = "mate" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "mataríamos", ["2"] = "mataríeis", ["3"] = "matariam" };
          sing = { ["1"] = "mataria", ["2"] = "matarias", ["3"] = "mataria" };
        };
        futu =
        {
          plur = { ["1"] = "mataremos", ["2"] = "matareis", ["3"] = "matarão" };
          sing = { ["1"] = "matarei", ["2"] = "matarás", ["3"] = "matará" };
        };
        impf =
        {
          plur = { ["1"] = "matávamos", ["2"] = "matáveis", ["3"] = "matavam" };
          sing = { ["1"] = "matava", ["2"] = "matavas", ["3"] = "matava" };
        };
        plpf =
        {
          plur = { ["1"] = "matáramos", ["2"] = "matáreis", ["3"] = "mataram" };
          sing = { ["1"] = "matara", ["2"] = "mataras", ["3"] = "matara" };
        };
        pres =
        {
          plur = { ["1"] = "matamos", ["2"] = "matais", ["3"] = "matam" };
          sing = { ["1"] = "mato", ["2"] = "matas", ["3"] = "mata" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "matamos";
            ["1_alt"] = { "matámos" };
            ["2"] = "matastes";
            ["3"] = "mataram";
          };
          sing = { ["1"] = "matei", ["2"] = "mataste", ["3"] = "matou" };
        };
      };
      infn =
      {
        impe = "matar";
        pers =
        {
          plur = { ["1"] = "matarmos", ["2"] = "matardes", ["3"] = "matarem" };
          sing = { ["1"] = "matar", ["2"] = "matares", ["3"] = "matar" };
        };
      };
      long_part_past =
      {
        plur = { f = "matadas", m = "matados" };
        sing = { f = "matada", m = "matado" };
      };
      part_pres = "matando";
      short_part_past =
      {
        plur = { f = "mortas", m = "mortos" };
        sing = { f = "morta", m = "morto" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "matarmos", ["2"] = "matardes", ["3"] = "matarem" };
          sing = { ["1"] = "matar", ["2"] = "matares", ["3"] = "matar" };
        };
        impf =
        {
          plur = { ["1"] = "matássemos", ["2"] = "matásseis", ["3"] = "matassem" };
          sing = { ["1"] = "matasse", ["2"] = "matasses", ["3"] = "matasse" };
        };
        pres =
        {
          plur = { ["1"] = "matemos", ["2"] = "mateis", ["3"] = "matem" };
          sing = { ["1"] = "mate", ["2"] = "mates", ["3"] = "mate" };
        };
      };
    };
    suffix = "ar";
    verb = "matar";
  };
  minguar =
  {
    comments =
    {
      "An acute accent is added in the stem of a few conjugated forms.";
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced.";
    };
    examples = { "aguar", "apropinquar", "desaguar", "enxaguar", "minguar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "minguemos";
            ["1_obsolete"] = "mingüemos";
            ["2"] = "minguai";
            ["3"] = "mínguem";
            ["3_obsolete"] = "míngüem";
          };
          sing =
          {
            ["1"] = "míngue";
            ["1_obsolete"] = "míngüe";
            ["2"] = "mingua";
            ["3"] = "míngue";
            ["3_obsolete"] = "míngüe";
          };
        };
        negt =
        {
          plur =
          {
            ["1"] = "minguemos";
            ["1_obsolete"] = "mingüemos";
            ["2"] = "mingueis";
            ["2_obsolete"] = "mingüeis";
            ["3"] = "mínguem";
            ["3_obsolete"] = "míngüem";
          };
          sing =
          {
            ["1"] = "míngue";
            ["1_obsolete"] = "míngüe";
            ["2"] = "míngues";
            ["2_obsolete"] = "míngües";
            ["3"] = "míngue";
            ["3_obsolete"] = "míngüe";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "minguaríamos";
            ["2"] = "minguaríeis";
            ["3"] = "minguariam";
          };
          sing = { ["1"] = "minguaria", ["2"] = "minguarias", ["3"] = "minguaria" };
        };
        futu =
        {
          plur = { ["1"] = "minguaremos", ["2"] = "minguareis", ["3"] = "minguarão" };
          sing = { ["1"] = "minguarei", ["2"] = "minguarás", ["3"] = "minguará" };
        };
        impf =
        {
          plur = { ["1"] = "minguávamos", ["2"] = "minguáveis", ["3"] = "minguavam" };
          sing = { ["1"] = "minguava", ["2"] = "minguavas", ["3"] = "minguava" };
        };
        plpf =
        {
          plur = { ["1"] = "minguáramos", ["2"] = "minguáreis", ["3"] = "minguaram" };
          sing = { ["1"] = "minguara", ["2"] = "minguaras", ["3"] = "minguara" };
        };
        pres =
        {
          plur = { ["1"] = "minguamos", ["2"] = "minguais", ["3"] = "mínguam" };
          sing = { ["1"] = "mínguo", ["2"] = "mínguas", ["3"] = "míngua" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "minguamos";
            ["1_alt"] = { "minguámos" };
            ["2"] = "minguastes";
            ["3"] = "minguaram";
          };
          sing =
          {
            ["1"] = "minguei";
            ["1_obsolete"] = "mingüei";
            ["2"] = "minguaste";
            ["3"] = "minguou";
          };
        };
      };
      infn =
      {
        impe = "minguar";
        pers =
        {
          plur = { ["1"] = "minguarmos", ["2"] = "minguardes", ["3"] = "minguarem" };
          sing = { ["1"] = "minguar", ["2"] = "minguares", ["3"] = "minguar" };
        };
      };
      part_past =
      {
        plur = { f = "minguadas", m = "minguados" };
        sing = { f = "minguada", m = "minguado" };
      };
      part_pres = "minguando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "minguarmos", ["2"] = "minguardes", ["3"] = "minguarem" };
          sing = { ["1"] = "minguar", ["2"] = "minguares", ["3"] = "minguar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "minguássemos";
            ["2"] = "minguásseis";
            ["3"] = "minguassem";
          };
          sing = { ["1"] = "minguasse", ["2"] = "minguasses", ["3"] = "minguasse" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "minguemos";
            ["1_obsolete"] = "mingüemos";
            ["2"] = "mingueis";
            ["2_obsolete"] = "mingüeis";
            ["3"] = "míngüem";
            ["3_obsolete"] = "mínguem";
          };
          sing =
          {
            ["1"] = "míngue";
            ["1_obsolete"] = "míngüe";
            ["2"] = "míngues";
            ["2_obsolete"] = "míngües";
            ["3"] = "míngue";
            ["3_obsolete"] = "míngüe";
          };
        };
      };
    };
    suffix = "ar";
    verb = "minguar";
  };
  mobiliar =
  {
    comments = { "An acute accent is added to few conjugated forms." };
    examples = { "mobiliar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "mobiliemos", ["2"] = "mobiliai", ["3"] = "mobiliem" };
          sing = { ["1"] = "mobilie", ["2"] = "mobilia", ["3"] = "mobilie" };
        };
        negt =
        {
          plur = { ["1"] = "mobiliemos", ["2"] = "mobilieis", ["3"] = "mobiliem" };
          sing = { ["1"] = "mobilie", ["2"] = "mobilies", ["3"] = "mobilie" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "mobiliaríamos";
            ["2"] = "mobiliaríeis";
            ["3"] = "mobiliariam";
          };
          sing = { ["1"] = "mobiliaria", ["2"] = "mobiliarias", ["3"] = "mobiliaria" };
        };
        futu =
        {
          plur =
          {
            ["1"] = "mobiliaremos";
            ["2"] = "mobiliareis";
            ["3"] = "mobiliarão";
          };
          sing = { ["1"] = "mobiliarei", ["2"] = "mobiliarás", ["3"] = "mobiliará" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "mobiliávamos";
            ["2"] = "mobiliáveis";
            ["3"] = "mobiliavam";
          };
          sing = { ["1"] = "mobiliava", ["2"] = "mobiliavas", ["3"] = "mobiliava" };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "mobiliáramos";
            ["2"] = "mobiliáreis";
            ["3"] = "mobiliaram";
          };
          sing = { ["1"] = "mobiliara", ["2"] = "mobiliaras", ["3"] = "mobiliara" };
        };
        pres =
        {
          plur = { ["1"] = "mobiliamos", ["2"] = "mobiliais", ["3"] = "mobiliam" };
          sing = { ["1"] = "mobílio", ["2"] = "mobílias", ["3"] = "mobília" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "mobiliamos";
            ["1_alt"] = { "mobiliámos" };
            ["2"] = "mobiliastes";
            ["3"] = "mobiliaram";
          };
          sing = { ["1"] = "mobiliei", ["2"] = "mobiliaste", ["3"] = "mobiliou" };
        };
      };
      infn =
      {
        impe = "mobiliar";
        pers =
        {
          plur = { ["1"] = "mobiliarmos", ["2"] = "mobiliardes", ["3"] = "mobiliarem" };
          sing = { ["1"] = "mobiliar", ["2"] = "mobiliares", ["3"] = "mobiliar" };
        };
      };
      part_past =
      {
        plur = { f = "mobiliadas", m = "mobiliados" };
        sing = { f = "mobiliada", m = "mobiliado" };
      };
      part_pres = "mobiliando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "mobiliarmos", ["2"] = "mobiliardes", ["3"] = "mobiliarem" };
          sing = { ["1"] = "mobiliar", ["2"] = "mobiliares", ["3"] = "mobiliar" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "mobiliássemos";
            ["2"] = "mobiliásseis";
            ["3"] = "mobiliassem";
          };
          sing = { ["1"] = "mobiliasse", ["2"] = "mobiliasses", ["3"] = "mobiliasse" };
        };
        pres =
        {
          plur = { ["1"] = "mobiliemos", ["2"] = "mobilieis", ["3"] = "mobiliem" };
          sing = { ["1"] = "mobílie", ["2"] = "mobílies", ["3"] = "mobílie" };
        };
      };
    };
    suffix = "ar";
    verb = "mobiliar";
  };
  morrer =
  {
    comments = { };
    examples = { "morrer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "morramos", ["2"] = "morrei", ["3"] = "morram" };
          sing = { ["2"] = "morre", ["3"] = "morra" };
        };
        negt =
        {
          plur = { ["1"] = "morramos", ["2"] = "morrais", ["3"] = "morram" };
          sing = { ["2"] = "morras", ["3"] = "morra" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "morreríamos", ["2"] = "morreríeis", ["3"] = "morreriam" };
          sing = { ["1"] = "morreria", ["2"] = "morrerias", ["3"] = "morreria" };
        };
        futu =
        {
          plur = { ["1"] = "morreremos", ["2"] = "morrereis", ["3"] = "morrerão" };
          sing = { ["1"] = "morrerei", ["2"] = "morrerás", ["3"] = "morrerá" };
        };
        impf =
        {
          plur = { ["1"] = "morríamos", ["2"] = "morríeis", ["3"] = "morriam" };
          sing = { ["1"] = "morria", ["2"] = "morrias", ["3"] = "morria" };
        };
        plpf =
        {
          plur = { ["1"] = "morrêramos", ["2"] = "morrêreis", ["3"] = "morreram" };
          sing = { ["1"] = "morrera", ["2"] = "morreras", ["3"] = "morrera" };
        };
        pres =
        {
          plur = { ["1"] = "morremos", ["2"] = "morreis", ["3"] = "morrem" };
          sing = { ["1"] = "morro", ["2"] = "morres", ["3"] = "morre" };
        };
        pret =
        {
          plur = { ["1"] = "morremos", ["2"] = "morrestes", ["3"] = "morreram" };
          sing = { ["1"] = "morri", ["2"] = "morreste", ["3"] = "morreu" };
        };
      };
      infn =
      {
        impe = "morrer";
        pers =
        {
          plur = { ["1"] = "morrermos", ["2"] = "morrerdes", ["3"] = "morrerem" };
          sing = { ["1"] = "morrer", ["2"] = "morreres", ["3"] = "morrer" };
        };
      };
      long_part_past =
      {
        plur = { f = "morridas", m = "morridos" };
        sing = { f = "morrida", m = "morrido" };
      };
      part_pres = "morrendo";
      short_part_past =
      {
        plur = { f = "mortas", m = "mortos" };
        sing = { f = "morta", m = "morto" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "morrermos", ["2"] = "morrerdes", ["3"] = "morrerem" };
          sing = { ["1"] = "morrer", ["2"] = "morreres", ["3"] = "morrer" };
        };
        impf =
        {
          plur = { ["1"] = "morrêssemos", ["2"] = "morrêsseis", ["3"] = "morressem" };
          sing = { ["1"] = "morresse", ["2"] = "morresses", ["3"] = "morresse" };
        };
        pres =
        {
          plur = { ["1"] = "morramos", ["2"] = "morrais", ["3"] = "morram" };
          sing = { ["1"] = "morra", ["2"] = "morras", ["3"] = "morra" };
        };
      };
    };
    suffix = "er";
    verb = "morrer";
  };
  munir =
  {
    comments = { };
    defective = true;
    examples = { "munir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "muni" };
          sing = { ["2"] = "mune" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "muniríamos", ["2"] = "muniríeis", ["3"] = "muniriam" };
          sing = { ["1"] = "muniria", ["2"] = "munirias", ["3"] = "muniria" };
        };
        futu =
        {
          plur = { ["1"] = "muniremos", ["2"] = "munireis", ["3"] = "munirão" };
          sing = { ["1"] = "munirei", ["2"] = "munirás", ["3"] = "munirá" };
        };
        impf =
        {
          plur = { ["1"] = "muníamos", ["2"] = "muníeis", ["3"] = "muniam" };
          sing = { ["1"] = "munia", ["2"] = "munias", ["3"] = "munia" };
        };
        plpf =
        {
          plur = { ["1"] = "muníramos", ["2"] = "muníreis", ["3"] = "muniram" };
          sing = { ["1"] = "munira", ["2"] = "muniras", ["3"] = "munira" };
        };
        pres =
        {
          plur = { ["1"] = "munimos", ["2"] = "munis", ["3"] = "munem" };
          sing = { ["2"] = "munes", ["3"] = "mune" };
        };
        pret =
        {
          plur = { ["1"] = "munimos", ["2"] = "munistes", ["3"] = "muniram" };
          sing = { ["1"] = "muni", ["2"] = "muniste", ["3"] = "muniu" };
        };
      };
      infn =
      {
        impe = "munir";
        pers =
        {
          plur = { ["1"] = "munirmos", ["2"] = "munirdes", ["3"] = "munirem" };
          sing = { ["1"] = "munir", ["2"] = "munires", ["3"] = "munir" };
        };
      };
      part_past =
      {
        plur = { f = "munidas", m = "munidos" };
        sing = { f = "munida", m = "munido" };
      };
      part_pres = "munindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "munirmos", ["2"] = "munirdes", ["3"] = "munirem" };
          sing = { ["1"] = "munir", ["2"] = "munires", ["3"] = "munir" };
        };
        impf =
        {
          plur = { ["1"] = "muníssemos", ["2"] = "munísseis", ["3"] = "munissem" };
          sing = { ["1"] = "munisse", ["2"] = "munisses", ["3"] = "munisse" };
        };
      };
    };
    suffix = "ir";
    verb = "munir";
  };
  nder =
  {
    abundant = true;
    comments = { "There are additional, shorter past participles." };
    examples = { "acender", "prender" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ndamos", ["2"] = "ndei", ["3"] = "ndam" };
          sing = { ["1"] = "nda", ["2"] = "nde", ["3"] = "nda" };
        };
        negt =
        {
          plur = { ["1"] = "ndamos", ["2"] = "ndais", ["3"] = "ndam" };
          sing = { ["1"] = "nda", ["2"] = "ndas", ["3"] = "nda" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "nderíamos", ["2"] = "nderíeis", ["3"] = "nderiam" };
          sing = { ["1"] = "nderia", ["2"] = "nderias", ["3"] = "nderia" };
        };
        futu =
        {
          plur = { ["1"] = "nderemos", ["2"] = "ndereis", ["3"] = "nderão" };
          sing = { ["1"] = "nderei", ["2"] = "nderás", ["3"] = "nderá" };
        };
        impf =
        {
          plur = { ["1"] = "ndíamos", ["2"] = "ndíeis", ["3"] = "ndiam" };
          sing = { ["1"] = "ndia", ["2"] = "ndias", ["3"] = "ndia" };
        };
        plpf =
        {
          plur = { ["1"] = "ndêramos", ["2"] = "ndêreis", ["3"] = "nderam" };
          sing = { ["1"] = "ndera", ["2"] = "nderas", ["3"] = "ndera" };
        };
        pres =
        {
          plur = { ["1"] = "ndemos", ["2"] = "ndeis", ["3"] = "ndem" };
          sing = { ["1"] = "ndo", ["2"] = "ndes", ["3"] = "nde" };
        };
        pret =
        {
          plur = { ["1"] = "ndemos", ["2"] = "ndestes", ["3"] = "nderam" };
          sing = { ["1"] = "ndi", ["2"] = "ndeste", ["3"] = "ndeu" };
        };
      };
      infn =
      {
        impe = "nder";
        pers =
        {
          plur = { ["1"] = "ndermos", ["2"] = "nderdes", ["3"] = "nderem" };
          sing = { ["1"] = "nder", ["2"] = "nderes", ["3"] = "nder" };
        };
      };
      part_past =
      {
        plur = { f = "sas", m = "sos" };
        sing = { f = "sa", m = "so" };
      };
      part_pres = "ndendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ndermos", ["2"] = "nderdes", ["3"] = "nderem" };
          sing = { ["1"] = "nder", ["2"] = "nderes", ["3"] = "nder" };
        };
        impf =
        {
          plur = { ["1"] = "ndêssemos", ["2"] = "ndêsseis", ["3"] = "ndessem" };
          sing = { ["1"] = "ndesse", ["2"] = "ndesses", ["3"] = "ndesse" };
        };
        pres =
        {
          plur = { ["1"] = "ndamos", ["2"] = "ndais", ["3"] = "ndam" };
          sing = { ["1"] = "nda", ["2"] = "ndas", ["3"] = "nda" };
        };
      };
    };
    suffix = "er";
    verb = "nder";
  };
  nevar =
  {
    comments = { };
    defective = true;
    examples = { "nevar" };
    forms =
    {
      indi =
      {
        cond =
        {
          sing = { ["3"] = "nevaria" };
        };
        futu =
        {
          sing = { ["3"] = "nevará" };
        };
        impf =
        {
          sing = { ["3"] = "nevava" };
        };
        plpf =
        {
          sing = { ["3"] = "nevara" };
        };
        pres =
        {
          sing = { ["3"] = "neva" };
        };
        pret =
        {
          sing = { ["3"] = "nevou" };
        };
      };
      infn =
      {
        impe = "nevar";
        pers =
        {
          sing = { ["3"] = "nevar" };
        };
      };
      part_past =
      {
        plur = { f = "nevadas", m = "nevados" };
        sing = { f = "nevada", m = "nevado" };
      };
      part_pres = "nevando";
      subj =
      {
        futu =
        {
          sing = { ["3"] = "nevar" };
        };
        impf =
        {
          sing = { ["3"] = "nevasse" };
        };
        pres =
        {
          sing = { ["3"] = "neve" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "nevar";
  };
  oar =
  {
    comments = { "An obsolete treatment is to add a circumflex in one conjugated form." };
    examples = { "abençoar", "coroar", "enjoar", "perdoar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "oemos", ["2"] = "oai", ["3"] = "oem" };
          sing = { ["1"] = "oe", ["2"] = "oa", ["3"] = "oe" };
        };
        negt =
        {
          plur = { ["1"] = "oemos", ["2"] = "oeis", ["3"] = "oem" };
          sing = { ["1"] = "oe", ["2"] = "oes", ["3"] = "oe" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "oaríamos", ["2"] = "oaríeis", ["3"] = "oariam" };
          sing = { ["1"] = "oaria", ["2"] = "oarias", ["3"] = "oaria" };
        };
        futu =
        {
          plur = { ["1"] = "oaremos", ["2"] = "oareis", ["3"] = "oarão" };
          sing = { ["1"] = "oarei", ["2"] = "oarás", ["3"] = "oará" };
        };
        impf =
        {
          plur = { ["1"] = "oávamos", ["2"] = "oáveis", ["3"] = "oavam" };
          sing = { ["1"] = "oava", ["2"] = "oavas", ["3"] = "oava" };
        };
        plpf =
        {
          plur = { ["1"] = "oáramos", ["2"] = "oáreis", ["3"] = "oaram" };
          sing = { ["1"] = "oara", ["2"] = "oaras", ["3"] = "oara" };
        };
        pres =
        {
          plur = { ["1"] = "oamos", ["2"] = "oais", ["3"] = "oam" };
          sing = { ["1"] = "oo", ["1_obsolete"] = "ôo", ["2"] = "oas", ["3"] = "oa" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "oamos";
            ["1_alt"] = { "oámos" };
            ["2"] = "oastes";
            ["3"] = "oaram";
          };
          sing = { ["1"] = "oei", ["2"] = "oaste", ["3"] = "oou" };
        };
      };
      infn =
      {
        impe = "oar";
        pers =
        {
          plur = { ["1"] = "oarmos", ["2"] = "oardes", ["3"] = "oarem" };
          sing = { ["1"] = "oar", ["2"] = "oares", ["3"] = "oar" };
        };
      };
      part_past =
      {
        plur = { f = "oadas", m = "oados" };
        sing = { f = "oada", m = "oado" };
      };
      part_pres = "oando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "oarmos", ["2"] = "oardes", ["3"] = "oarem" };
          sing = { ["1"] = "oar", ["2"] = "oares", ["3"] = "oar" };
        };
        impf =
        {
          plur = { ["1"] = "oássemos", ["2"] = "oásseis", ["3"] = "oassem" };
          sing = { ["1"] = "oasse", ["2"] = "oasses", ["3"] = "oasse" };
        };
        pres =
        {
          plur = { ["1"] = "oemos", ["2"] = "oeis", ["3"] = "oem" };
          sing = { ["1"] = "oe", ["2"] = "oes", ["3"] = "oe" };
        };
      };
    };
    suffix = "ar";
    verb = "oar";
  };
  oer =
  {
    comments =
    {
      "An obsolete treatment is to add a circumflex in one conjugated form.";
      "An acute accent is added to few conjugated forms.";
    };
    examples = { "doer", "moer", "roer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "oamos", ["2"] = "oei", ["3"] = "oam" };
          sing = { ["1"] = "oa", ["2"] = "ói", ["3"] = "oa" };
        };
        negt =
        {
          plur = { ["1"] = "oamos", ["2"] = "oais", ["3"] = "oam" };
          sing = { ["1"] = "oa", ["2"] = "oas", ["3"] = "oa" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "oeríamos", ["2"] = "oeríeis", ["3"] = "oeriam" };
          sing = { ["1"] = "oeria", ["2"] = "oerias", ["3"] = "oeria" };
        };
        futu =
        {
          plur = { ["1"] = "oeremos", ["2"] = "oereis", ["3"] = "oerão" };
          sing = { ["1"] = "oerei", ["2"] = "oerás", ["3"] = "oerá" };
        };
        impf =
        {
          plur = { ["1"] = "oíamos", ["2"] = "oíeis", ["3"] = "oíam" };
          sing = { ["1"] = "oía", ["2"] = "oías", ["3"] = "oía" };
        };
        plpf =
        {
          plur = { ["1"] = "oêramos", ["2"] = "oêreis", ["3"] = "oeram" };
          sing = { ["1"] = "oera", ["2"] = "oeras", ["3"] = "oera" };
        };
        pres =
        {
          plur = { ["1"] = "oemos", ["2"] = "oeis", ["3"] = "oem" };
          sing = { ["1"] = "oo", ["1_obsolete"] = "ôo", ["2"] = "óis", ["3"] = "ói" };
        };
        pret =
        {
          plur = { ["1"] = "oemos", ["2"] = "oestes", ["3"] = "oeram" };
          sing = { ["1"] = "oí", ["2"] = "oeste", ["3"] = "oeu" };
        };
      };
      infn =
      {
        impe = "oer";
        pers =
        {
          plur = { ["1"] = "oermos", ["2"] = "oerdes", ["3"] = "oerem" };
          sing = { ["1"] = "oer", ["2"] = "oeres", ["3"] = "oer" };
        };
      };
      part_past =
      {
        plur = { f = "oídas", m = "oídos" };
        sing = { f = "oída", m = "oído" };
      };
      part_pres = "oendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "oermos", ["2"] = "oerdes", ["3"] = "oerem" };
          sing = { ["1"] = "oer", ["2"] = "oeres", ["3"] = "oer" };
        };
        impf =
        {
          plur = { ["1"] = "oêssemos", ["2"] = "oêsseis", ["3"] = "oessem" };
          sing = { ["1"] = "oesse", ["2"] = "oesses", ["3"] = "oesse" };
        };
        pres =
        {
          plur = { ["1"] = "oamos", ["2"] = "oais", ["3"] = "oam" };
          sing = { ["1"] = "oa", ["2"] = "oas", ["3"] = "oa" };
        };
      };
    };
    suffix = "er";
    verb = "oer";
  };
  oiar =
  {
    comments = { "An acute accent is added to the letter ''o'' in few conjugated forms." };
    examples = { "apoiar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "oiemos", ["2"] = "oiai", ["3"] = "óiem" };
          sing = { ["1"] = "óie", ["2"] = "óia", ["3"] = "óie" };
        };
        negt =
        {
          plur = { ["1"] = "oiemos", ["2"] = "oieis", ["3"] = "óiem" };
          sing = { ["1"] = "óie", ["2"] = "oies", ["3"] = "óie" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "oiaríamos", ["2"] = "oiaríeis", ["3"] = "oiariam" };
          sing = { ["1"] = "oiaria", ["2"] = "oiarias", ["3"] = "oiaria" };
        };
        futu =
        {
          plur = { ["1"] = "oiaremos", ["2"] = "oiareis", ["3"] = "oiarão" };
          sing = { ["1"] = "oiarei", ["2"] = "oiarás", ["3"] = "oiará" };
        };
        impf =
        {
          plur = { ["1"] = "oiávamos", ["2"] = "oiáveis", ["3"] = "oiavam" };
          sing = { ["1"] = "oiava", ["2"] = "oiavas", ["3"] = "oiava" };
        };
        plpf =
        {
          plur = { ["1"] = "oiáramos", ["2"] = "oiáreis", ["3"] = "oiaram" };
          sing = { ["1"] = "oiara", ["2"] = "oiaras", ["3"] = "oiara" };
        };
        pres =
        {
          plur = { ["1"] = "oiamos", ["2"] = "oiais", ["3"] = "óiam" };
          sing = { ["1"] = "óio", ["2"] = "óias", ["3"] = "óia" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "oiamos";
            ["1_alt"] = { "oiámos" };
            ["2"] = "oiastes";
            ["3"] = "oiaram";
          };
          sing = { ["1"] = "oiei", ["2"] = "oiaste", ["3"] = "oiou" };
        };
      };
      infn =
      {
        impe = "oiar";
        pers =
        {
          plur = { ["1"] = "oiarmos", ["2"] = "oiardes", ["3"] = "oiarem" };
          sing = { ["1"] = "oiar", ["2"] = "oiares", ["3"] = "oiar" };
        };
      };
      part_past =
      {
        plur = { f = "oiadas", m = "oiados" };
        sing = { f = "oiada", m = "oiado" };
      };
      part_pres = "oiando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "oiarmos", ["2"] = "oiardes", ["3"] = "oiarem" };
          sing = { ["1"] = "oiar", ["2"] = "oiares", ["3"] = "oiar" };
        };
        impf =
        {
          plur = { ["1"] = "oiássemos", ["2"] = "oiásseis", ["3"] = "oiassem" };
          sing = { ["1"] = "oiasse", ["2"] = "oiasses", ["3"] = "oiasse" };
        };
        pres =
        {
          plur = { ["1"] = "oiemos", ["2"] = "oieis", ["3"] = "oiem" };
          sing = { ["1"] = "oie", ["2"] = "oies", ["3"] = "oie" };
        };
      };
    };
    suffix = "ar";
    verb = "oiar";
  };
  olir =
  {
    comments = { "Forms whose suffix’s thematic vowel isn’t " };
    defective = true;
    examples = { "abolir", "explodir", "demolir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1_defective"] = "ulamos", ["2"] = "oli", ["3_defective"] = "ulam" };
          sing = { ["2_defective"] = "ole", ["3_defective"] = "ula" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "ulamos";
            ["2_defective"] = "ulais";
            ["3_defective"] = "ulam";
          };
          sing = { ["2_defective"] = "ulas", ["3_defective"] = "ula" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "oliríamos", ["2"] = "oliríeis", ["3"] = "oliriam" };
          sing = { ["1"] = "oliria", ["2"] = "olirias", ["3"] = "oliria" };
        };
        futu =
        {
          plur = { ["1"] = "oliremos", ["2"] = "olireis", ["3"] = "olirão" };
          sing = { ["1"] = "olirei", ["2"] = "olirás", ["3"] = "olirá" };
        };
        impf =
        {
          plur = { ["1"] = "olíamos", ["2"] = "olíeis", ["3"] = "oliam" };
          sing = { ["1"] = "olia", ["2"] = "olias", ["3"] = "olia" };
        };
        plpf =
        {
          plur = { ["1"] = "olíramos", ["2"] = "olíreis", ["3"] = "oliram" };
          sing = { ["1"] = "olira", ["2"] = "oliras", ["3"] = "olira" };
        };
        pres =
        {
          plur = { ["1"] = "olimos", ["2"] = "olis", ["3_defective"] = "olem" };
          sing =
          {
            ["1_defective"] = "ulo";
            ["2_defective"] = "oles";
            ["3_defective"] = "ole";
          };
        };
        pret =
        {
          plur = { ["1"] = "olimos", ["2"] = "olistes", ["3"] = "oliram" };
          sing = { ["1"] = "oli", ["2"] = "oliste", ["3"] = "oliu" };
        };
      };
      infn =
      {
        impe = "olir";
        pers =
        {
          plur = { ["1"] = "olirmos", ["2"] = "olirdes", ["3"] = "olirem" };
          sing = { ["1"] = "olir", ["2"] = "olires", ["3"] = "olir" };
        };
      };
      part_past =
      {
        plur = { f = "olidas", m = "olidos" };
        sing = { f = "olida", m = "olido" };
      };
      part_pres = "olindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "olirmos", ["2"] = "olirdes", ["3"] = "olirem" };
          sing = { ["1"] = "olir", ["2"] = "olires", ["3"] = "olir" };
        };
        impf =
        {
          plur = { ["1"] = "olíssemos", ["2"] = "olísseis", ["3"] = "olissem" };
          sing = { ["1"] = "olisse", ["2"] = "olisses", ["3"] = "olisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "ulamos";
            ["2_defective"] = "ulais";
            ["3_defective"] = "ulam";
          };
          sing =
          {
            ["1_defective"] = "ula";
            ["2_defective"] = "ulas";
            ["3_defective"] = "ula";
          };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "olir";
  };
  ouvir =
  {
    comments = { };
    examples = { "ouvir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ouçamos", ["2"] = "ouçais", ["3"] = "ouçam" };
          sing = { ["2"] = "ouça", ["3"] = "ouças" };
        };
        negt =
        {
          plur = { ["1"] = "ouçamos", ["2"] = "ouçais", ["3"] = "ouçam" };
          sing = { ["2"] = "ouças", ["3"] = "ouça" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ouviríamos", ["2"] = "ouviríeis", ["3"] = "ouviriam" };
          sing = { ["1"] = "ouviria", ["2"] = "ouvirias", ["3"] = "ouviria" };
        };
        futu =
        {
          plur = { ["1"] = "ouviremos", ["2"] = "ouvireis", ["3"] = "ouvirão" };
          sing = { ["1"] = "ouvirei", ["2"] = "ouvirás", ["3"] = "ouvirá" };
        };
        impf =
        {
          plur = { ["1"] = "ouvíamos", ["2"] = "ouvíeis", ["3"] = "ouviam" };
          sing = { ["1"] = "ouvia", ["2"] = "ouvias", ["3"] = "ouvia" };
        };
        plpf =
        {
          plur = { ["1"] = "ouvíramos", ["2"] = "ouvíreis", ["3"] = "ouviram" };
          sing = { ["1"] = "ouvira", ["2"] = "ouviras", ["3"] = "ouvira" };
        };
        pres =
        {
          plur = { ["1"] = "ouvimos", ["2"] = "ouvis", ["3"] = "ouvem" };
          sing = { ["1"] = "ouço", ["2"] = "ouves", ["3"] = "ouve" };
        };
        pret =
        {
          plur = { ["1"] = "ouvimos", ["2"] = "ouvistes", ["3"] = "ouviram" };
          sing = { ["1"] = "ouvi", ["2"] = "ouviste", ["3"] = "ouviu" };
        };
      };
      infn =
      {
        impe = "ouvir";
        pers =
        {
          plur = { ["1"] = "ouvirmos", ["2"] = "ouvirdes", ["3"] = "ouvirem" };
          sing = { ["1"] = "ouvir", ["2"] = "ouvires", ["3"] = "ouvir" };
        };
      };
      part_past =
      {
        plur = { f = "ouvidas", m = "ouvidos" };
        sing = { f = "ouvida", m = "ouvido" };
      };
      part_pres = "ouvindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ouvirmos", ["2"] = "ouvirdes", ["3"] = "ouvirem" };
          sing = { ["1"] = "ouvir", ["2"] = "ouvires", ["3"] = "ouvir" };
        };
        impf =
        {
          plur = { ["1"] = "ouvíssemos", ["2"] = "ouvísseis", ["3"] = "ouvissem" };
          sing = { ["1"] = "ouvisse", ["2"] = "ouvisses", ["3"] = "ouvisse" };
        };
        pres =
        {
          plur = { ["1"] = "ouçamos", ["2"] = "ouçais", ["3"] = "ouçam" };
          sing = { ["1"] = "ouça", ["2"] = "ouças", ["3"] = "ouça" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "ouvir";
  };
  pagar =
  {
    abundant = true;
    comments =
    {
      "The last letter ''g'' is changed to ''gu'', depending on the following vowel, resulting in these three possible sequences: -ga-, -gue- and -go-.";
      "There are additional, shorter past participles without the common letter sequence -ad-.";
    };
    examples = { "pagar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "paguemos", ["2"] = "pagai", ["3"] = "paguem" };
          sing = { ["1"] = "pague", ["2"] = "paga", ["3"] = "pague" };
        };
        negt =
        {
          plur = { ["1"] = "paguemos", ["2"] = "pagueis", ["3"] = "paguem" };
          sing = { ["1"] = "pague", ["2"] = "pagues", ["3"] = "pague" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "pagaríamos", ["2"] = "pagaríeis", ["3"] = "pagariam" };
          sing = { ["1"] = "pagaria", ["2"] = "pagarias", ["3"] = "pagaria" };
        };
        futu =
        {
          plur = { ["1"] = "pagaremos", ["2"] = "pagareis", ["3"] = "pagarão" };
          sing = { ["1"] = "pagarei", ["2"] = "pagarás", ["3"] = "pagará" };
        };
        impf =
        {
          plur = { ["1"] = "pagávamos", ["2"] = "pagáveis", ["3"] = "pagavam" };
          sing = { ["1"] = "pagava", ["2"] = "pagavas", ["3"] = "pagava" };
        };
        plpf =
        {
          plur = { ["1"] = "pagáramos", ["2"] = "pagáreis", ["3"] = "pagaram" };
          sing = { ["1"] = "pagara", ["2"] = "pagaras", ["3"] = "pagara" };
        };
        pres =
        {
          plur = { ["1"] = "pagamos", ["2"] = "pagais", ["3"] = "pagam" };
          sing = { ["1"] = "pago", ["2"] = "pagas", ["3"] = "paga" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "pagamos";
            ["1_alt"] = { "pagámos" };
            ["2"] = "pagastes";
            ["3"] = "pagaram";
          };
          sing = { ["1"] = "paguei", ["2"] = "pagaste", ["3"] = "pagou" };
        };
      };
      infn =
      {
        impe = "pagar";
        pers =
        {
          plur = { ["1"] = "pagarmos", ["2"] = "pagardes", ["3"] = "pagarem" };
          sing = { ["1"] = "pagar", ["2"] = "pagares", ["3"] = "pagar" };
        };
      };
      part_past =
      {
        plur = { f = "pagas", m = "pagos" };
        sing = { f = "paga", m = "pago" };
      };
      part_pres = "pagando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pagarmos", ["2"] = "pagardes", ["3"] = "pagarem" };
          sing = { ["1"] = "pagar", ["2"] = "pagares", ["3"] = "pagar" };
        };
        impf =
        {
          plur = { ["1"] = "pagássemos", ["2"] = "pagásseis", ["3"] = "pagassem" };
          sing = { ["1"] = "pagasse", ["2"] = "pagasses", ["3"] = "pagasse" };
        };
        pres =
        {
          plur = { ["1"] = "paguemos", ["2"] = "pagueis", ["3"] = "paguem" };
          sing = { ["1"] = "pague", ["2"] = "pagues", ["3"] = "pague" };
        };
      };
    };
    suffix = "ar";
    verb = "pagar";
  };
  parar =
  {
    comments = { "An obsolete treatment is to add an acute accent in one conjugated form." };
    examples = { "parar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "paremos", ["2"] = "parai", ["3"] = "parem" };
          sing =
          {
            ["1"] = "pare";
            ["2"] = "para";
            ["2_obsolete"] = "pára";
            ["3"] = "pare";
          };
        };
        negt =
        {
          plur = { ["1"] = "paremos", ["2"] = "pareis", ["3"] = "parem" };
          sing = { ["1"] = "pare", ["2"] = "pares", ["3"] = "pare" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "pararíamos", ["2"] = "pararíeis", ["3"] = "parariam" };
          sing = { ["1"] = "pararia", ["2"] = "pararias", ["3"] = "pararia" };
        };
        futu =
        {
          plur = { ["1"] = "pararemos", ["2"] = "parareis", ["3"] = "pararão" };
          sing = { ["1"] = "pararei", ["2"] = "pararás", ["3"] = "parará" };
        };
        impf =
        {
          plur = { ["1"] = "parávamos", ["2"] = "paráveis", ["3"] = "paravam" };
          sing = { ["1"] = "parava", ["2"] = "paravas", ["3"] = "parava" };
        };
        plpf =
        {
          plur = { ["1"] = "paráramos", ["2"] = "paráreis", ["3"] = "pararam" };
          sing = { ["1"] = "parara", ["2"] = "pararas", ["3"] = "parara" };
        };
        pres =
        {
          plur = { ["1"] = "paramos", ["2"] = "parais", ["3"] = "param" };
          sing = { ["1"] = "paro", ["2"] = "paras", ["3"] = "para" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "paramos";
            ["1_alt"] = { "parámos" };
            ["2"] = "parastes";
            ["3"] = "pararam";
          };
          sing = { ["1"] = "parei", ["2"] = "paraste", ["3"] = "parou" };
        };
      };
      infn =
      {
        impe = "parar";
        pers =
        {
          plur = { ["1"] = "pararmos", ["2"] = "parardes", ["3"] = "pararem" };
          sing = { ["1"] = "parar", ["2"] = "parares", ["3"] = "parar" };
        };
      };
      part_past =
      {
        plur = { f = "paradas", m = "parados" };
        sing = { f = "parada", m = "parado" };
      };
      part_pres = "parando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pararmos", ["2"] = "parardes", ["3"] = "pararem" };
          sing = { ["1"] = "parar", ["2"] = "parares", ["3"] = "parar" };
        };
        impf =
        {
          plur = { ["1"] = "parássemos", ["2"] = "parásseis", ["3"] = "parassem" };
          sing = { ["1"] = "parasse", ["2"] = "parasses", ["3"] = "parasse" };
        };
        pres =
        {
          plur = { ["1"] = "paremos", ["2"] = "pareis", ["3"] = "parem" };
          sing = { ["1"] = "pare", ["2"] = "pares", ["3"] = "pare" };
        };
      };
    };
    suffix = "ar";
    verb = "parar";
  };
  parir =
  {
    comments = { };
    examples = { "parir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "paramos";
            ["1_defective_alt"] = { "pairamos" };
            ["2"] = "pari";
            ["3_defective"] = "param";
            ["3_defective_alt"] = { "pairem" };
          };
          sing =
          {
            ["2_defective"] = "pare";
            ["3_defective"] = "para";
            ["3_defective_alt"] = { "paira" };
          };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "paramos";
            ["1_defective_alt"] = { "pairamos" };
            ["2_defective"] = "parais";
            ["2_defective_alt"] = { "pairais" };
            ["3_defective"] = "param";
            ["3_defective_alt"] = { "pairem" };
          };
          sing =
          {
            ["2_defective"] = "paras";
            ["2_defective_alt"] = { "pairas" };
            ["3_defective"] = "para";
            ["3_defective_alt"] = { "paira" };
          };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "pariríamos", ["2"] = "pariríeis", ["3"] = "paririam" };
          sing = { ["1"] = "pariria", ["2"] = "paririas", ["3"] = "pariria" };
        };
        futu =
        {
          plur = { ["1"] = "pariremos", ["2"] = "parireis", ["3"] = "parirão" };
          sing = { ["1"] = "parirei", ["2"] = "parirás", ["3"] = "parirá" };
        };
        impf =
        {
          plur = { ["1"] = "paríamos", ["2"] = "paríeis", ["3"] = "pariam" };
          sing = { ["1"] = "paria", ["2"] = "parias", ["3"] = "paria" };
        };
        plpf =
        {
          plur = { ["1"] = "paríramos", ["2"] = "paríreis", ["3"] = "pariram" };
          sing = { ["1"] = "parira", ["2"] = "pariras", ["3"] = "parira" };
        };
        pres =
        {
          plur = { ["1"] = "parimos", ["2"] = "paris", ["3_defective"] = "parem" };
          sing =
          {
            ["1_defective"] = "paro";
            ["1_defective_alt"] = { "pairo" };
            ["2_defective"] = "pares";
            ["3_defective"] = "pare";
          };
        };
        pret =
        {
          plur = { ["1"] = "parimos", ["2"] = "paristes", ["3"] = "pariram" };
          sing = { ["1"] = "pari", ["2"] = "pariste", ["3"] = "pariu" };
        };
      };
      infn =
      {
        impe = "parir";
        pers =
        {
          plur = { ["1"] = "parirmos", ["2"] = "parirdes", ["3"] = "parirem" };
          sing = { ["1"] = "parir", ["2"] = "parires", ["3"] = "parir" };
        };
      };
      part_past =
      {
        plur = { f = "paridas", m = "paridos" };
        sing = { f = "parida", m = "parido" };
      };
      part_pres = "parindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "parirmos", ["2"] = "parirdes", ["3"] = "parirem" };
          sing = { ["1"] = "parir", ["2"] = "parires", ["3"] = "parir" };
        };
        impf =
        {
          plur = { ["1"] = "paríssemos", ["2"] = "parísseis", ["3"] = "parissem" };
          sing = { ["1"] = "parisse", ["2"] = "parisses", ["3"] = "parisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "paramos";
            ["2_defective"] = "parais";
            ["3_defective"] = "param";
          };
          sing =
          {
            ["1_defective"] = "para";
            ["1_defective_alt"] = { "paira", "pairamos" };
            ["2_defective"] = "paras";
            ["2_defective_alt"] = { "pairas", "pairais" };
            ["3_defective"] = "para";
            ["3_defective_alt"] = { "paira", "pairem" };
          };
        };
      };
    };
    irregular = false;
    suffix = "ir";
    verb = "parir";
  };
  pasmar =
  {
    abundant = true;
    comments =
    {
      "There are additional, shorter past participles without the common letter sequence -ad-.";
    };
    examples = { "findar", "ganhar", "pasmar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "pasmemos", ["2"] = "pasmai", ["3"] = "pasmem" };
          sing = { ["1"] = "pasme", ["2"] = "pasma", ["3"] = "pasme" };
        };
        negt =
        {
          plur = { ["1"] = "pasmemos", ["2"] = "pasmeis", ["3"] = "pasmem" };
          sing = { ["1"] = "pasme", ["2"] = "pasmes", ["3"] = "pasme" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "pasmaríamos", ["2"] = "pasmaríeis", ["3"] = "pasmariam" };
          sing = { ["1"] = "pasmaria", ["2"] = "pasmarias", ["3"] = "pasmaria" };
        };
        futu =
        {
          plur = { ["1"] = "pasmaremos", ["2"] = "pasmareis", ["3"] = "pasmarão" };
          sing = { ["1"] = "pasmarei", ["2"] = "pasmarás", ["3"] = "pasmará" };
        };
        impf =
        {
          plur = { ["1"] = "pasmávamos", ["2"] = "pasmáveis", ["3"] = "pasmavam" };
          sing = { ["1"] = "pasmava", ["2"] = "pasmavas", ["3"] = "pasmava" };
        };
        plpf =
        {
          plur = { ["1"] = "pasmáramos", ["2"] = "pasmáreis", ["3"] = "pasmaram" };
          sing = { ["1"] = "pasmara", ["2"] = "pasmaras", ["3"] = "pasmara" };
        };
        pres =
        {
          plur = { ["1"] = "pasmamos", ["2"] = "pasmais", ["3"] = "pasmam" };
          sing = { ["1"] = "pasmo", ["2"] = "pasmas", ["3"] = "pasma" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "pasmamos";
            ["1_alt"] = { "pasmámos" };
            ["2"] = "pasmastes";
            ["3"] = "pasmaram";
          };
          sing = { ["1"] = "pasmei", ["2"] = "pasmaste", ["3"] = "pasmou" };
        };
      };
      infn =
      {
        impe = "pasmar";
        pers =
        {
          plur = { ["1"] = "pasmarmos", ["2"] = "pasmardes", ["3"] = "pasmarem" };
          sing = { ["1"] = "pasmar", ["2"] = "pasmares", ["3"] = "pasmar" };
        };
      };
      part_past =
      {
        plur = { f = "pasmas", m = "pasmos" };
        sing = { f = "pasma", m = "pasmo" };
      };
      part_pres = "pasmando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pasmarmos", ["2"] = "pasmardes", ["3"] = "pasmarem" };
          sing = { ["1"] = "pasmar", ["2"] = "pasmares", ["3"] = "pasmar" };
        };
        impf =
        {
          plur = { ["1"] = "pasmássemos", ["2"] = "pasmásseis", ["3"] = "pasmassem" };
          sing = { ["1"] = "pasmasse", ["2"] = "pasmasses", ["3"] = "pasmasse" };
        };
        pres =
        {
          plur = { ["1"] = "pasmemos", ["2"] = "pasmeis", ["3"] = "pasmem" };
          sing = { ["1"] = "pasme", ["2"] = "pasmes", ["3"] = "pasme" };
        };
      };
    };
    suffix = "ar";
    verb = "pasmar";
  };
  perder =
  {
    comments = { };
    examples = { "perder" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "percamos", ["2"] = "perdei", ["3"] = "percam" };
          sing = { ["1"] = "perca", ["2"] = "perde", ["3"] = "perca" };
        };
        negt =
        {
          plur = { ["1"] = "percamos", ["2"] = "percais", ["3"] = "percam" };
          sing = { ["1"] = "perca", ["2"] = "percas", ["3"] = "perca" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "perderíamos", ["2"] = "perderíeis", ["3"] = "perderiam" };
          sing = { ["1"] = "perderia", ["2"] = "perderias", ["3"] = "perderia" };
        };
        futu =
        {
          plur = { ["1"] = "perderemos", ["2"] = "perdereis", ["3"] = "perderão" };
          sing = { ["1"] = "perderei", ["2"] = "perderás", ["3"] = "perderá" };
        };
        impf =
        {
          plur = { ["1"] = "perdíamos", ["2"] = "perdíeis", ["3"] = "perdiam" };
          sing = { ["1"] = "perdia", ["2"] = "perdias", ["3"] = "perdia" };
        };
        plpf =
        {
          plur = { ["1"] = "perdêramos", ["2"] = "perdêreis", ["3"] = "perderam" };
          sing = { ["1"] = "perdera", ["2"] = "perderas", ["3"] = "perdera" };
        };
        pres =
        {
          plur = { ["1"] = "perdemos", ["2"] = "perdeis", ["3"] = "perdem" };
          sing = { ["1"] = "perco", ["2"] = "perdes", ["3"] = "perde" };
        };
        pret =
        {
          plur = { ["1"] = "perdemos", ["2"] = "perdestes", ["3"] = "perderam" };
          sing = { ["1"] = "perdi", ["2"] = "perdeste", ["3"] = "perdeu" };
        };
      };
      infn =
      {
        impe = "perder";
        pers =
        {
          plur = { ["1"] = "perdermos", ["2"] = "perderdes", ["3"] = "perderem" };
          sing = { ["1"] = "perder", ["2"] = "perderes", ["3"] = "perder" };
        };
      };
      part_past =
      {
        plur = { f = "perdidas", m = "perdidos" };
        sing = { f = "perdida", m = "perdido" };
      };
      part_pres = "perdendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "perdermos", ["2"] = "perderdes", ["3"] = "perderem" };
          sing = { ["1"] = "perder", ["2"] = "perderes", ["3"] = "perder" };
        };
        impf =
        {
          plur = { ["1"] = "perdêssemos", ["2"] = "perdêsseis", ["3"] = "perdessem" };
          sing = { ["1"] = "perdesse", ["2"] = "perdesses", ["3"] = "perdesse" };
        };
        pres =
        {
          plur = { ["1"] = "percamos", ["2"] = "percais", ["3"] = "percam" };
          sing = { ["1"] = "perca", ["2"] = "percas", ["3"] = "perca" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "perder";
  };
  poder =
  {
    comments = { };
    examples = { "poder" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "possamos", ["2"] = "podei", ["3"] = "possam" };
          sing = { ["1"] = "possa", ["2"] = "pode", ["3"] = "possa" };
        };
        negt =
        {
          plur = { ["1"] = "possamos", ["2"] = "possais", ["3"] = "possam" };
          sing = { ["1"] = "possa", ["2"] = "possas", ["3"] = "possa" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "poderíamos", ["2"] = "poderíeis", ["3"] = "poderiam" };
          sing = { ["1"] = "poderia", ["2"] = "poderias", ["3"] = "poderia" };
        };
        futu =
        {
          plur = { ["1"] = "poderemos", ["2"] = "podereis", ["3"] = "poderão" };
          sing = { ["1"] = "poderei", ["2"] = "poderás", ["3"] = "poderá" };
        };
        impf =
        {
          plur = { ["1"] = "podíamos", ["2"] = "podíeis", ["3"] = "podiam" };
          sing = { ["1"] = "podia", ["2"] = "podias", ["3"] = "podia" };
        };
        plpf =
        {
          plur = { ["1"] = "pudéramos", ["2"] = "pudéreis", ["3"] = "puderam" };
          sing = { ["1"] = "pudera", ["2"] = "puderas", ["3"] = "pudera" };
        };
        pres =
        {
          plur = { ["1"] = "podemos", ["2"] = "podeis", ["3"] = "podem" };
          sing = { ["1"] = "posso", ["2"] = "podes", ["3"] = "pode" };
        };
        pret =
        {
          plur = { ["1"] = "pudemos", ["2"] = "pudestes", ["3"] = "puderam" };
          sing = { ["1"] = "pude", ["2"] = "pudeste", ["3"] = "pôde" };
        };
      };
      infn =
      {
        impe = "poder";
        pers =
        {
          plur = { ["1"] = "pudermos", ["2"] = "puderdes", ["3"] = "puderem" };
          sing = { ["1"] = "puder", ["2"] = "puderes", ["3"] = "puder" };
        };
      };
      part_past =
      {
        plur = { f = "podidas", m = "podidos" };
        sing = { f = "podida", m = "podido" };
      };
      part_pres = "podendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pudermos", ["2"] = "puderdes", ["3"] = "puderem" };
          sing = { ["1"] = "puder", ["2"] = "puderes", ["3"] = "puder" };
        };
        impf =
        {
          plur = { ["1"] = "pudéssemos", ["2"] = "pudésseis", ["3"] = "pudessem" };
          sing = { ["1"] = "pudesse", ["2"] = "pudesses", ["3"] = "pudesse" };
        };
        pres =
        {
          plur = { ["1"] = "possamos", ["2"] = "possais", ["3"] = "possam" };
          sing = { ["1"] = "possa", ["2"] = "possas", ["3"] = "possa" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "poder";
  };
  polir =
  {
    comments = { };
    defective = true;
    examples = { "polir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "poli" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "poliríamos", ["2"] = "poliríeis", ["3"] = "poliriam" };
          sing = { ["1"] = "poliria", ["2"] = "polirias", ["3"] = "poliria" };
        };
        futu =
        {
          plur = { ["1"] = "poliremos", ["2"] = "polireis", ["3"] = "polirão" };
          sing = { ["1"] = "polirei", ["2"] = "polirás", ["3"] = "polirá" };
        };
        impf =
        {
          plur = { ["1"] = "políamos", ["2"] = "políeis", ["3"] = "poliam" };
          sing = { ["1"] = "polia", ["2"] = "polias", ["3"] = "polia" };
        };
        plpf =
        {
          plur = { ["1"] = "políramos", ["2"] = "políreis", ["3"] = "poliram" };
          sing = { ["1"] = "polira", ["2"] = "poliras", ["3"] = "polira" };
        };
        pres =
        {
          plur = { ["1"] = "polimos", ["2"] = "polis" };
        };
        pret =
        {
          plur = { ["1"] = "polimos", ["2"] = "polistes", ["3"] = "poliram" };
          sing = { ["1"] = "poli", ["2"] = "poliste", ["3"] = "poliu" };
        };
      };
      infn =
      {
        impe = "polir";
        pers =
        {
          plur = { ["1"] = "polirmos", ["2"] = "polirdes", ["3"] = "polirem" };
          sing = { ["1"] = "polir", ["2"] = "polires", ["3"] = "polir" };
        };
      };
      part_past =
      {
        plur = { f = "polidas", m = "polidos" };
        sing = { f = "polida", m = "polido" };
      };
      part_pres = "polindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "polirmos", ["2"] = "polirdes", ["3"] = "polirem" };
          sing = { ["1"] = "polir", ["2"] = "polires", ["3"] = "polir" };
        };
        impf =
        {
          plur = { ["1"] = "políssemos", ["2"] = "polísseis", ["3"] = "polissem" };
          sing = { ["1"] = "polisse", ["2"] = "polisses", ["3"] = "polisse" };
        };
      };
    };
    suffix = "ir";
    verb = "polir";
  };
  por =
  {
    comments =
    {
      "Despite the infinite form ending in -or, this verb is considered as part of the -er group.";
      "This verb is derived from ''pôr'' and conjugated almost exactly like it. The only exceptions are some infinitive forms or ''pôr'', which contain a circumflex (^).";
    };
    examples = { "compor", "dispor", "opor", "propor", "supor" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponde", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "põe", ["3"] = "ponha" };
        };
        negt =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponhais", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "ponhas", ["3"] = "ponha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "poríamos", ["2"] = "poríeis", ["3"] = "poriam" };
          sing = { ["1"] = "poria", ["2"] = "porias", ["3"] = "poria" };
        };
        futu =
        {
          plur = { ["1"] = "poremos", ["2"] = "poreis", ["3"] = "porão" };
          sing = { ["1"] = "porei", ["2"] = "porás", ["3"] = "porá" };
        };
        impf =
        {
          plur = { ["1"] = "púnhamos", ["2"] = "púnheis", ["3"] = "punham" };
          sing = { ["1"] = "punha", ["2"] = "punhas", ["3"] = "punha" };
        };
        plpf =
        {
          plur = { ["1"] = "puséramos", ["2"] = "puséreis", ["3"] = "puseram" };
          sing = { ["1"] = "pusera", ["2"] = "puseras", ["3"] = "pusera" };
        };
        pres =
        {
          plur = { ["1"] = "pomos", ["2"] = "pondes", ["3"] = "põem" };
          sing = { ["1"] = "ponho", ["2"] = "pões", ["3"] = "põe" };
        };
        pret =
        {
          plur = { ["1"] = "pusemos", ["2"] = "pusestes", ["3"] = "puseram" };
          sing = { ["1"] = "pus", ["2"] = "puseste", ["3"] = "pôs" };
        };
      };
      infn =
      {
        impe = "por";
        pers =
        {
          plur = { ["1"] = "pormos", ["2"] = "pordes", ["3"] = "porem" };
          sing = { ["1"] = "por", ["2"] = "pores", ["3"] = "por" };
        };
      };
      part_past =
      {
        plur = { f = "postas", m = "postos" };
        sing = { f = "posta", m = "posto" };
      };
      part_pres = "pondo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pusermos", ["2"] = "puserdes", ["3"] = "puserem" };
          sing = { ["1"] = "puser", ["2"] = "puseres", ["3"] = "puser" };
        };
        impf =
        {
          plur = { ["1"] = "puséssemos", ["2"] = "pusésseis", ["3"] = "pusessem" };
          sing = { ["1"] = "pusesse", ["2"] = "pusesses", ["3"] = "pusesse" };
        };
        pres =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponhais", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "ponhas", ["3"] = "ponha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "por";
  };
  ["pôr"] =
  {
    comments =
    {
      "Despite the infinite form ending in -or, this verb is considered as part of the -er group.";
      "All verbs derived from ''pôr'' are conjugated almost exactly like it. The only exceptions are some infinitive forms, which do not contain a circumflex (^).";
    };
    examples = { "pôr" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponde", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "põe", ["3"] = "ponha" };
        };
        negt =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponhais", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "ponhas", ["3"] = "ponha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "poríamos", ["2"] = "poríeis", ["3"] = "poriam" };
          sing = { ["1"] = "poria", ["2"] = "porias", ["3"] = "poria" };
        };
        futu =
        {
          plur = { ["1"] = "poremos", ["2"] = "poreis", ["3"] = "porão" };
          sing = { ["1"] = "porei", ["2"] = "porás", ["3"] = "porá" };
        };
        impf =
        {
          plur = { ["1"] = "púnhamos", ["2"] = "púnheis", ["3"] = "punham" };
          sing = { ["1"] = "punha", ["2"] = "punhas", ["3"] = "punha" };
        };
        plpf =
        {
          plur = { ["1"] = "puséramos", ["2"] = "puséreis", ["3"] = "puseram" };
          sing = { ["1"] = "pusera", ["2"] = "puseras", ["3"] = "pusera" };
        };
        pres =
        {
          plur = { ["1"] = "pomos", ["2"] = "pondes", ["3"] = "põem" };
          sing = { ["1"] = "ponho", ["2"] = "pões", ["3"] = "põe" };
        };
        pret =
        {
          plur = { ["1"] = "pusemos", ["2"] = "pusestes", ["3"] = "puseram" };
          sing = { ["1"] = "pus", ["2"] = "puseste", ["3"] = "pôs" };
        };
      };
      infn =
      {
        impe = "pôr";
        pers =
        {
          plur = { ["1"] = "pormos", ["2"] = "pordes", ["3"] = "porem" };
          sing = { ["1"] = "pôr", ["2"] = "pores", ["3"] = "pôr" };
        };
      };
      part_past =
      {
        plur = { f = "postas", m = "postos" };
        sing = { f = "posta", m = "posto" };
      };
      part_pres = "pondo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "pusermos", ["2"] = "puserdes", ["3"] = "puserem" };
          sing = { ["1"] = "puser", ["2"] = "puseres", ["3"] = "puser" };
        };
        impf =
        {
          plur = { ["1"] = "puséssemos", ["2"] = "pusésseis", ["3"] = "pusessem" };
          sing = { ["1"] = "pusesse", ["2"] = "pusesses", ["3"] = "pusesse" };
        };
        pres =
        {
          plur = { ["1"] = "ponhamos", ["2"] = "ponhais", ["3"] = "ponham" };
          sing = { ["1"] = "ponha", ["2"] = "ponhas", ["3"] = "ponha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "pôr";
  };
  prazer =
  {
    comments =
    {
      "Only conjugates in the third-person singular (except imperative) and impersonal forms.";
    };
    defective = true;
    examples = { "prazer", "aprazer", "desprazer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "prazamos";
            ["2_defective"] = "prazei";
            ["3_defective"] = "prazam";
          };
          sing =
          {
            ["2_defective"] = "praz";
            ["2_defective_alt"] = { "praze" };
            ["3_defective"] = "praza";
          };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "prazamos";
            ["2_defective"] = "prazais";
            ["3_defective"] = "prazam";
          };
          sing = { ["2_defective"] = "prazas", ["3_defective"] = "praz" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1_defective"] = "prazeríamos";
            ["2_defective"] = "prazeríeis";
            ["3_defective"] = "prazeriam";
          };
          sing =
          {
            ["1_defective"] = "prazeria";
            ["2_defective"] = "prazerias";
            ["3"] = "prazeria";
          };
        };
        futu =
        {
          plur =
          {
            ["1_defective"] = "prazeremos";
            ["2_defective"] = "prazereis";
            ["3_defective"] = "prazerão";
          };
          sing =
          {
            ["1_defective"] = "prazerei";
            ["2_defective"] = "prazerás";
            ["3"] = "prazerá";
          };
        };
        impf =
        {
          plur =
          {
            ["1_defective"] = "prazíamos";
            ["2_defective"] = "prazíeis";
            ["3_defective"] = "praziam";
          };
          sing =
          {
            ["1_defective"] = "prazia";
            ["2_defective"] = "prazias";
            ["3"] = "prazia";
          };
        };
        plpf =
        {
          plur =
          {
            ["1_defective"] = "prouvéramos";
            ["2_defective"] = "prouvéreis";
            ["3_defective"] = "prouveram";
          };
          sing =
          {
            ["1_defective"] = "prouvera";
            ["2_defective"] = "prouveras";
            ["3"] = "prouvera";
          };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "prazemos";
            ["2_defective"] = "prazeis";
            ["3_defective"] = "prazem";
          };
          sing =
          {
            ["1_defective"] = "prazo";
            ["2_defective"] = "prazes";
            ["3"] = "praz";
          };
        };
        pret =
        {
          plur =
          {
            ["1_defective"] = "prouvemos";
            ["2_defective"] = "prouvestes";
            ["3_defective"] = "prouveram";
          };
          sing =
          {
            ["1_defective"] = "prouve";
            ["2_defective"] = "prouveste";
            ["3"] = "prouve";
          };
        };
      };
      infn =
      {
        impe = "prazer";
        pers =
        {
          plur =
          {
            ["1_defective"] = "prazermos";
            ["2_defective"] = "prazerdes";
            ["3_defective"] = "prazerem";
          };
          sing =
          {
            ["1_defective"] = "prazer";
            ["2_defective"] = "prazeres";
            ["3"] = "prazer";
          };
        };
      };
      part_past =
      {
        plur = { f = "prazidas", m = "prazidos" };
        sing = { f = "prazida", m = "prazido" };
      };
      part_pres = "prazendo";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1_defective"] = "prouvermos";
            ["2_defective"] = "prouverdes";
            ["3_defective"] = "prouverem";
          };
          sing =
          {
            ["1_defective"] = "prouver";
            ["2_defective"] = "prouveres";
            ["3"] = "prouver";
          };
        };
        impf =
        {
          plur =
          {
            ["1_defective"] = "prouvéssemos";
            ["2_defective"] = "prouvésseis";
            ["3_defective"] = "prouvessem";
          };
          sing =
          {
            ["1_defective"] = "prouvesse";
            ["2_defective"] = "prouvesses";
            ["3"] = "prouvesse";
          };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "prazamos";
            ["2_defective"] = "prazais";
            ["3_defective"] = "prazam";
          };
          sing =
          {
            ["1_defective"] = "praza";
            ["2_defective"] = "prazas";
            ["3"] = "praza";
          };
        };
      };
    };
    suffix = "er";
    verb = "prazer";
  };
  primir =
  {
    abundant = true;
    comments =
    {
      "There are additional, shorter past participles, the masculine singular ending in -presso.";
    };
    examples = { "exprimir", "imprimir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "primamos", ["2"] = "primi", ["3"] = "primam" };
          sing = { ["1"] = "prima", ["2"] = "prime", ["3"] = "prima" };
        };
        negt =
        {
          plur = { ["1"] = "primamos", ["2"] = "primais", ["3"] = "primam" };
          sing = { ["1"] = "prima", ["2"] = "primas", ["3"] = "prima" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "primiríamos", ["2"] = "primiríeis", ["3"] = "primiriam" };
          sing = { ["1"] = "primiria", ["2"] = "primirias", ["3"] = "primiria" };
        };
        futu =
        {
          plur = { ["1"] = "primiremos", ["2"] = "primireis", ["3"] = "primirão" };
          sing = { ["1"] = "primirei", ["2"] = "primirás", ["3"] = "primirá" };
        };
        impf =
        {
          plur = { ["1"] = "primíamos", ["2"] = "primíeis", ["3"] = "primiam" };
          sing = { ["1"] = "primia", ["2"] = "primias", ["3"] = "primia" };
        };
        plpf =
        {
          plur = { ["1"] = "primíramos", ["2"] = "primíreis", ["3"] = "primiram" };
          sing = { ["1"] = "primira", ["2"] = "primiras", ["3"] = "primira" };
        };
        pres =
        {
          plur = { ["1"] = "primimos", ["2"] = "primis", ["3"] = "primem" };
          sing = { ["1"] = "primo", ["2"] = "primes", ["3"] = "prime" };
        };
        pret =
        {
          plur = { ["1"] = "primimos", ["2"] = "primistes", ["3"] = "primiram" };
          sing = { ["1"] = "primi", ["2"] = "primiste", ["3"] = "primiu" };
        };
      };
      infn =
      {
        impe = "primir";
        pers =
        {
          plur = { ["1"] = "primirmos", ["2"] = "primirdes", ["3"] = "primirem" };
          sing = { ["1"] = "primir", ["2"] = "primires", ["3"] = "primir" };
        };
      };
      part_past =
      {
        plur = { f = "primidas", m = "primidos" };
        sing = { f = "primida", m = "primido" };
      };
      part_pres = "primindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "primirmos", ["2"] = "primirdes", ["3"] = "primirem" };
          sing = { ["1"] = "primir", ["2"] = "primires", ["3"] = "primir" };
        };
        impf =
        {
          plur = { ["1"] = "primíssemos", ["2"] = "primísseis", ["3"] = "primissem" };
          sing = { ["1"] = "primisse", ["2"] = "primisses", ["3"] = "primisse" };
        };
        pres =
        {
          plur = { ["1"] = "primamos", ["2"] = "primais", ["3"] = "primam" };
          sing = { ["1"] = "prima", ["2"] = "primas", ["3"] = "prima" };
        };
      };
    };
    suffix = "ir";
    verb = "primir";
  };
  prover =
  {
    comments = { };
    examples = { "prover" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "provejamos", ["2"] = "provede", ["3"] = "provejam" };
          sing = { ["2"] = "provê", ["3"] = "proveja" };
        };
        negt =
        {
          plur = { ["1"] = "provejamos", ["2"] = "provejais", ["3"] = "provejam" };
          sing = { ["2"] = "provejas", ["3"] = "proveja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "proveríamos", ["2"] = "proveríeis", ["3"] = "proveriam" };
          sing = { ["1"] = "proveria", ["2"] = "proverias", ["3"] = "proveria" };
        };
        futu =
        {
          plur = { ["1"] = "proveremos", ["2"] = "provereis", ["3"] = "proverão" };
          sing = { ["1"] = "proverei", ["2"] = "proverás", ["3"] = "proverá" };
        };
        impf =
        {
          plur = { ["1"] = "províamos", ["2"] = "províeis", ["3"] = "proviam" };
          sing = { ["1"] = "provia", ["2"] = "provias", ["3"] = "provia" };
        };
        plpf =
        {
          plur = { ["1"] = "provêramos", ["2"] = "provêreis", ["3"] = "proveram" };
          sing = { ["1"] = "provera", ["2"] = "proveras", ["3"] = "provera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "provemos";
            ["2"] = "provedes";
            ["3"] = "proveem";
            ["3_obsolete"] = "provêem";
          };
          sing = { ["1"] = "provejo", ["2"] = "provês", ["3"] = "provê" };
        };
        pret =
        {
          plur = { ["1"] = "provemos", ["2"] = "provestes", ["3"] = "proveram" };
          sing = { ["1"] = "provi", ["2"] = "proviste", ["3"] = "proveu" };
        };
      };
      infn =
      {
        impe = "prover";
        pers =
        {
          plur = { ["1"] = "provermos", ["2"] = "proverdes", ["3"] = "proverem" };
          sing = { ["1"] = "prover", ["2"] = "proveres", ["3"] = "prover" };
        };
      };
      part_past =
      {
        plur = { f = "providas", m = "providos" };
        sing = { f = "provida", m = "provido" };
      };
      part_pres = "provendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "provermos", ["2"] = "proverdes", ["3"] = "proverem" };
          sing = { ["1"] = "prover", ["2"] = "proveres", ["3"] = "prover" };
        };
        impf =
        {
          plur = { ["1"] = "provêssemos", ["2"] = "provêsseis", ["3"] = "provessem" };
          sing = { ["1"] = "provesse", ["2"] = "provesses", ["3"] = "provesse" };
        };
        pres =
        {
          plur = { ["1"] = "provejamos", ["2"] = "provejais", ["3"] = "provejam" };
          sing = { ["1"] = "proveja", ["2"] = "provejas", ["3"] = "proveja" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "prover";
  };
  querer =
  {
    abundant = true;
    comments = { };
    examples = { "querer", "requerer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "queiramos", ["2"] = "querei", ["3"] = "queiram" };
          sing = { ["1"] = "queira", ["2"] = "quere", ["3"] = "queira" };
        };
        negt =
        {
          plur = { ["1"] = "queiramos", ["2"] = "queirais", ["3"] = "queiram" };
          sing = { ["1"] = "queira", ["2"] = "queiras", ["3"] = "queira" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "quereríamos", ["2"] = "quereríeis", ["3"] = "quereriam" };
          sing = { ["1"] = "quereria", ["2"] = "quererias", ["3"] = "quereria" };
        };
        futu =
        {
          plur = { ["1"] = "quereremos", ["2"] = "querereis", ["3"] = "quererão" };
          sing = { ["1"] = "quererei", ["2"] = "quererás", ["3"] = "quererá" };
        };
        impf =
        {
          plur = { ["1"] = "queríamos", ["2"] = "queríeis", ["3"] = "queriam" };
          sing = { ["1"] = "queria", ["2"] = "querias", ["3"] = "queria" };
        };
        plpf =
        {
          plur = { ["1"] = "quiséramos", ["2"] = "quiséreis", ["3"] = "quiseram" };
          sing = { ["1"] = "quisera", ["2"] = "quiseras", ["3"] = "quisera" };
        };
        pres =
        {
          plur = { ["1"] = "queremos", ["2"] = "quereis", ["3"] = "querem" };
          sing =
          {
            ["1"] = "quero";
            ["2"] = "queres";
            ["3"] = "quer";
            ["3_alt"] = { "quere" };
          };
        };
        pret =
        {
          plur = { ["1"] = "quisemos", ["2"] = "quisestes", ["3"] = "quiseram" };
          sing = { ["1"] = "quis", ["2"] = "quiseste", ["3"] = "quis" };
        };
      };
      infn =
      {
        impe = "querer";
        pers =
        {
          plur = { ["1"] = "querermos", ["2"] = "quererdes", ["3"] = "quererem" };
          sing = { ["1"] = "querer", ["2"] = "quereres", ["3"] = "querer" };
        };
      };
      part_past =
      {
        plur = { f = "queridas", m = "queridos" };
        sing = { f = "querida", m = "querido" };
      };
      part_pres = "querendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "quisermos", ["2"] = "quiserdes", ["3"] = "quiserem" };
          sing = { ["1"] = "quiser", ["2"] = "quiseres", ["3"] = "quiser" };
        };
        impf =
        {
          plur = { ["1"] = "quiséssemos", ["2"] = "quisésseis", ["3"] = "quisessem" };
          sing = { ["1"] = "quisesse", ["2"] = "quisesses", ["3"] = "quisesse" };
        };
        pres =
        {
          plur = { ["1"] = "queiramos", ["2"] = "queirais", ["3"] = "queiram" };
          sing = { ["1"] = "queira", ["2"] = "queiras", ["3"] = "queira" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "querer";
  };
  remir =
  {
    comments = { };
    defective = true;
    examples = { "remir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1_defective"] = "redimamos";
            ["1_obsolete"] = "rimamos";
            ["2"] = "remi";
            ["3_defective"] = "redimam";
            ["3_obsolete"] = "rimam";
          };
          sing =
          {
            ["2_defective"] = "redime";
            ["2_obsolete"] = "rime";
            ["3_defective"] = "redima";
            ["3_obsolete"] = "rima";
          };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "redimamos";
            ["1_obsolete"] = "rimamos";
            ["2_defective"] = "redimais";
            ["2_obsolete"] = "rimais";
            ["3_defective"] = "redimam";
            ["3_obsolete"] = "rimam";
          };
          sing =
          {
            ["2_defective"] = "redimas";
            ["2_obsolete"] = "rimas";
            ["3_defective"] = "redima";
            ["3_obsolete"] = "rima";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "remiríamos", ["2"] = "remiríeis", ["3"] = "remiriam" };
          sing = { ["1"] = "remiria", ["2"] = "remirias", ["3"] = "remiria" };
        };
        futu =
        {
          plur = { ["1"] = "remiremos", ["2"] = "remireis", ["3"] = "remirão" };
          sing = { ["1"] = "remirei", ["2"] = "remirás", ["3"] = "remirá" };
        };
        impf =
        {
          plur = { ["1"] = "remíamos", ["2"] = "remíeis", ["3"] = "remiam" };
          sing = { ["1"] = "remia", ["2"] = "remias", ["3"] = "remia" };
        };
        plpf =
        {
          plur = { ["1"] = "remíramos", ["2"] = "remíreis", ["3"] = "remiram" };
          sing = { ["1"] = "remira", ["2"] = "remiras", ["3"] = "remira" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "remimos";
            ["2"] = "remis";
            ["3_defective"] = "redimem";
            ["3_obsolete"] = "rimem";
          };
          sing =
          {
            ["1_defective"] = "redimo";
            ["1_obsolete"] = "rimo";
            ["2_defective"] = "redimes";
            ["2_obsolete"] = "rimes";
            ["3_defective"] = "redime";
            ["3_obsolete"] = "rime";
          };
        };
        pret =
        {
          plur = { ["1"] = "remimos", ["2"] = "remistes", ["3"] = "remiram" };
          sing = { ["1"] = "remi", ["2"] = "remiste", ["3"] = "remiu" };
        };
      };
      infn =
      {
        impe = "remir";
        pers =
        {
          plur = { ["1"] = "remirmos", ["2"] = "remirdes", ["3"] = "remirem" };
          sing = { ["1"] = "remir", ["2"] = "remires", ["3"] = "remir" };
        };
      };
      part_past =
      {
        plur = { f = "remidas", m = "remidos" };
        sing = { f = "remida", m = "remido" };
      };
      part_pres = "remindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "remirmos", ["2"] = "remirdes", ["3"] = "remirem" };
          sing = { ["1"] = "remir", ["2"] = "remires", ["3"] = "remir" };
        };
        impf =
        {
          plur = { ["1"] = "remíssemos", ["2"] = "remísseis", ["3"] = "remissem" };
          sing = { ["1"] = "remisse", ["2"] = "remisses", ["3"] = "remisse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "redimamos";
            ["1_obsolete"] = "rimamos";
            ["2_defective"] = "redimais";
            ["2_obsolete"] = "rimais";
            ["3_defective"] = "redimam";
            ["3_obsolete"] = "rimam";
          };
          sing =
          {
            ["1_defective"] = "redima";
            ["1_obsolete"] = "rima";
            ["2_defective"] = "redimas";
            ["2_obsolete"] = "rimas";
            ["3_defective"] = "redima";
            ["3_obsolete"] = "rima";
          };
        };
      };
    };
    suffix = "ir";
    verb = "remir";
  };
  repelir =
  {
    comments = { };
    examples = { "repelir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "repilamos", ["2"] = "repeli", ["3"] = "repilam" };
          sing = { ["2"] = "repele", ["3"] = "repila" };
        };
        negt =
        {
          plur = { ["1"] = "repilamos", ["2"] = "repilais", ["3"] = "repilam" };
          sing = { ["2"] = "repilas", ["3"] = "repila" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "repeliríamos";
            ["2"] = "repeliríeis";
            ["3"] = "repeliriam";
          };
          sing = { ["1"] = "repeliria", ["2"] = "repelirias", ["3"] = "repeliria" };
        };
        futu =
        {
          plur = { ["1"] = "repeliremos", ["2"] = "repelireis", ["3"] = "repelirão" };
          sing = { ["1"] = "repelirei", ["2"] = "repelirás", ["3"] = "repelirá" };
        };
        impf =
        {
          plur = { ["1"] = "repelíamos", ["2"] = "repelíeis", ["3"] = "repeliam" };
          sing = { ["1"] = "repelia", ["2"] = "repelias", ["3"] = "repelia" };
        };
        plpf =
        {
          plur = { ["1"] = "repelíramos", ["2"] = "repelíreis", ["3"] = "repeliram" };
          sing = { ["1"] = "repelira", ["2"] = "repeliras", ["3"] = "repelira" };
        };
        pres =
        {
          plur = { ["1"] = "repelimos", ["2"] = "repelis", ["3"] = "repelem" };
          sing = { ["1"] = "repilo", ["2"] = "repeles", ["3"] = "repele" };
        };
        pret =
        {
          plur = { ["1"] = "repelimos", ["2"] = "repelistes", ["3"] = "repeliram" };
          sing = { ["1"] = "repeli", ["2"] = "repeliste", ["3"] = "repeliu" };
        };
      };
      infn =
      {
        impe = "repelir";
        pers =
        {
          plur = { ["1"] = "repelirmos", ["2"] = "repelirdes", ["3"] = "repelirem" };
          sing = { ["1"] = "repelir", ["2"] = "repelires", ["3"] = "repelir" };
        };
      };
      long_part_past =
      {
        plur = { f = "repelidas", m = "repelidos" };
        sing = { f = "repelida", m = "repelido" };
      };
      part_pres = "repelindo";
      short_part_past =
      {
        plur = { f = "repulsas", m = "repulsos" };
        sing = { f = "repulsa", m = "repulso" };
      };
      subj =
      {
        futu =
        {
          plur = { ["1"] = "repelirmos", ["2"] = "repelirdes", ["3"] = "repelirem" };
          sing = { ["1"] = "repelir", ["2"] = "repelires", ["3"] = "repelir" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "repelíssemos";
            ["2"] = "repelísseis";
            ["3"] = "repelissem";
          };
          sing = { ["1"] = "repelisse", ["2"] = "repelisses", ["3"] = "repelisse" };
        };
        pres =
        {
          plur = { ["1"] = "repilamos", ["2"] = "repilais", ["3"] = "repilam" };
          sing = { ["1"] = "repila", ["2"] = "repilas", ["3"] = "repila" };
        };
      };
    };
    irregular = false;
    suffix = "ir";
    verb = "repelir";
  };
  reunir =
  {
    comments = { };
    examples = { "reunir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "reunamos", ["2"] = "reuni", ["3"] = "reúnam" };
          sing = { ["2"] = "reúne", ["3"] = "reúna" };
        };
        negt =
        {
          plur = { ["1"] = "reunamos", ["2"] = "reunais", ["3"] = "reúnam" };
          sing = { ["2"] = "reúnas", ["3"] = "reúna" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "reuniríamos", ["2"] = "reuniríeis", ["3"] = "reuniriam" };
          sing = { ["1"] = "reuniria", ["2"] = "reunirias", ["3"] = "reuniria" };
        };
        futu =
        {
          plur = { ["1"] = "reuniremos", ["2"] = "reunireis", ["3"] = "reunirão" };
          sing = { ["1"] = "reunirei", ["2"] = "reunirás", ["3"] = "reunirá" };
        };
        impf =
        {
          plur = { ["1"] = "reuníamos", ["2"] = "reuníeis", ["3"] = "reuniam" };
          sing = { ["1"] = "reunia", ["2"] = "reunias", ["3"] = "reunia" };
        };
        plpf =
        {
          plur = { ["1"] = "reuníramos", ["2"] = "reuníreis", ["3"] = "reuniram" };
          sing = { ["1"] = "reunira", ["2"] = "reuniras", ["3"] = "reunira" };
        };
        pres =
        {
          plur = { ["1"] = "reunimos", ["2"] = "reunis", ["3"] = "reúnem" };
          sing = { ["1"] = "reúno", ["2"] = "reúnes", ["3"] = "reúne" };
        };
        pret =
        {
          plur = { ["1"] = "reunimos", ["2"] = "reunistes", ["3"] = "reuniram" };
          sing = { ["1"] = "reuni", ["2"] = "reuniste", ["3"] = "reuniu" };
        };
      };
      infn =
      {
        impe = "reunir";
        pers =
        {
          plur = { ["1"] = "reunirmos", ["2"] = "reunirdes", ["3"] = "reunirem" };
          sing = { ["1"] = "reunir", ["2"] = "reunires", ["3"] = "reunir" };
        };
      };
      part_past =
      {
        plur = { f = "reunidas", m = "reunidos" };
        sing = { f = "reunida", m = "reunido" };
      };
      part_pres = "reunindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "reunirmos", ["2"] = "reunirdes", ["3"] = "reunirem" };
          sing = { ["1"] = "reunir", ["2"] = "reunires", ["3"] = "reunir" };
        };
        impf =
        {
          plur = { ["1"] = "reuníssemos", ["2"] = "reunísseis", ["3"] = "reunissem" };
          sing = { ["1"] = "reunisse", ["2"] = "reunisses", ["3"] = "reunisse" };
        };
        pres =
        {
          plur = { ["1"] = "reúnamos", ["2"] = "reúnais", ["3"] = "reúnam" };
          sing = { ["1"] = "reúna", ["2"] = "reúnas", ["3"] = "reúna" };
        };
      };
    };
    suffix = "ir";
    verb = "reunir";
  };
  rir =
  {
    comments = { };
    examples = { "rir", "sorrir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "riamos", ["2"] = "ride", ["3"] = "riam" };
          sing = { ["2"] = "ri", ["3"] = "ria" };
        };
        negt =
        {
          plur = { ["1"] = "riamos", ["2"] = "riais", ["3"] = "riam" };
          sing = { ["2"] = "rias", ["3"] = "ria" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "riríamos", ["2"] = "riríeis", ["3"] = "ririam" };
          sing = { ["1"] = "riria", ["2"] = "ririas", ["3"] = "riria" };
        };
        futu =
        {
          plur = { ["1"] = "riremos", ["2"] = "rireis", ["3"] = "rirão" };
          sing = { ["1"] = "rirei", ["2"] = "rirás", ["3"] = "rirá" };
        };
        impf =
        {
          plur = { ["1"] = "ríamos", ["2"] = "ríeis", ["3"] = "riam" };
          sing = { ["1"] = "ria", ["2"] = "rias", ["3"] = "ria" };
        };
        plpf =
        {
          plur = { ["1"] = "ríramos", ["2"] = "ríreis", ["3"] = "riram" };
          sing = { ["1"] = "rira", ["2"] = "riras", ["3"] = "rira" };
        };
        pres =
        {
          plur = { ["1"] = "rimos", ["2"] = "rides", ["3"] = "riem" };
          sing = { ["1"] = "rio", ["2"] = "ris", ["3"] = "ri" };
        };
        pret =
        {
          plur = { ["1"] = "rimos", ["2"] = "ristes", ["3"] = "riram" };
          sing = { ["1"] = "ri", ["2"] = "riste", ["3"] = "riu" };
        };
      };
      infn =
      {
        impe = "rir";
        pers =
        {
          plur = { ["1"] = "rirmos", ["2"] = "rirdes", ["3"] = "rirem" };
          sing = { ["1"] = "rir", ["2"] = "rires", ["3"] = "rir" };
        };
      };
      part_past =
      {
        plur = { f = "ridas", m = "ridos" };
        sing = { f = "rida", m = "rido" };
      };
      part_pres = "rindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "rirmos", ["2"] = "rirdes", ["3"] = "rirem" };
          sing = { ["1"] = "rir", ["2"] = "rires", ["3"] = "rir" };
        };
        impf =
        {
          plur = { ["1"] = "ríssemos", ["2"] = "rísseis", ["3"] = "rissem" };
          sing = { ["1"] = "risse", ["2"] = "risses", ["3"] = "risse" };
        };
        pres =
        {
          plur = { ["1"] = "riamos", ["2"] = "riais", ["3"] = "riam" };
          sing = { ["1"] = "ria", ["2"] = "rias", ["3"] = "ria" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "rir";
  };
  ruir =
  {
    comments = { };
    defective = true;
    examples = { };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["2"] = "ruí", ["3_defective"] = "ruam" };
          sing = { ["2"] = "rui" };
        };
        negt =
        {
          plur =
          {
            ["1_defective"] = "ruamos";
            ["2_defective"] = "ruais";
            ["3_defective"] = "ruam";
          };
          sing = { ["2_defective"] = "ruas", ["3_defective"] = "rua" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ruiríamos", ["2"] = "ruiríeis", ["3"] = "ruiriam" };
          sing = { ["1"] = "ruiria", ["2"] = "ruirias", ["3"] = "ruiria" };
        };
        futu =
        {
          plur = { ["1"] = "ruiremos", ["2"] = "ruireis", ["3"] = "ruirão" };
          sing = { ["1"] = "ruirei", ["2"] = "ruirás", ["3"] = "ruirá" };
        };
        impf =
        {
          plur = { ["1"] = "ruíamos", ["2"] = "ruíeis", ["3"] = "ruíam" };
          sing = { ["1"] = "ruía", ["2"] = "ruías", ["3"] = "ruía" };
        };
        plpf =
        {
          plur = { ["1"] = "ruíramos", ["2"] = "ruíreis", ["3"] = "ruíram" };
          sing = { ["1"] = "ruíra", ["2"] = "ruíras", ["3"] = "ruíra" };
        };
        pres =
        {
          plur = { ["1"] = "ruímos", ["2"] = "ruís", ["3"] = "ruem" };
          sing = { ["1_defective"] = "ruo", ["2"] = "ruis", ["3"] = "rui" };
        };
        pret =
        {
          plur = { ["1"] = "ruímos", ["2"] = "ruístes", ["3"] = "ruíram" };
          sing = { ["1"] = "ruí", ["2"] = "ruíste", ["3"] = "rruiu" };
        };
      };
      infn =
      {
        impe = "ruir";
        pers =
        {
          plur = { ["1"] = "ruirmos", ["2"] = "ruirdes", ["3"] = "ruírem" };
          sing = { ["1"] = "ruir", ["2"] = "ruíres", ["3"] = "ruir" };
        };
      };
      part_past =
      {
        plur = { f = "ruídas", m = "ruídos" };
        sing = { f = "ruída", m = "ruído" };
      };
      part_pres = "ruindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ruirmos", ["2"] = "ruirdes", ["3"] = "ruírem" };
          sing = { ["1"] = "ruir", ["2"] = "ruíres", ["3"] = "ruir" };
        };
        impf =
        {
          plur = { ["1"] = "ruíssemos", ["2"] = "ruísseis", ["3"] = "ruíssem" };
          sing = { ["1"] = "ruísse", ["2"] = "ruísses", ["3"] = "ruísse" };
        };
        pres =
        {
          plur =
          {
            ["1_defective"] = "ruamos";
            ["2_defective"] = "ruais";
            ["3_defective"] = "ruam";
          };
          sing =
          {
            ["1_defective"] = "rua";
            ["2_defective"] = "ruas";
            ["3_defective"] = "rua";
          };
        };
      };
    };
    suffix = "ir";
    verb = "ruir";
  };
  saber =
  {
    comments = { };
    examples = { "saber" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "saibamos", ["2"] = "sabei", ["3"] = "saibam" };
          sing = { ["1"] = "saiba", ["2"] = "sabe", ["3"] = "saiba" };
        };
        negt =
        {
          plur = { ["1"] = "saibamos", ["2"] = "saibais", ["3"] = "saibam" };
          sing = { ["1"] = "saiba", ["2"] = "saibas", ["3"] = "saiba" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "saberíamos", ["2"] = "saberíeis", ["3"] = "saberiam" };
          sing = { ["1"] = "saberia", ["2"] = "saberias", ["3"] = "saberia" };
        };
        futu =
        {
          plur = { ["1"] = "saberemos", ["2"] = "sabereis", ["3"] = "saberão" };
          sing = { ["1"] = "saberei", ["2"] = "saberás", ["3"] = "saberá" };
        };
        impf =
        {
          plur = { ["1"] = "sabíamos", ["2"] = "sabíeis", ["3"] = "sabiam" };
          sing = { ["1"] = "sabia", ["2"] = "sabias", ["3"] = "sabia" };
        };
        plpf =
        {
          plur = { ["1"] = "soubéramos", ["2"] = "soubéreis", ["3"] = "soubéram" };
          sing = { ["1"] = "soubera", ["2"] = "souberas", ["3"] = "soubera" };
        };
        pres =
        {
          plur = { ["1"] = "sabemos", ["2"] = "sabeis", ["3"] = "sabem" };
          sing = { ["1"] = "sei", ["2"] = "sabes", ["3"] = "sabe" };
        };
        pret =
        {
          plur = { ["1"] = "soubemos", ["2"] = "soubestes", ["3"] = "souberam" };
          sing = { ["1"] = "soube", ["2"] = "soubeste", ["3"] = "soube" };
        };
      };
      infn =
      {
        impe = "saber";
        pers =
        {
          plur = { ["1"] = "sabermos", ["2"] = "saberdes", ["3"] = "saberem" };
          sing = { ["1"] = "saber", ["2"] = "saberes", ["3"] = "saber" };
        };
      };
      part_past =
      {
        plur = { f = "sabidas", m = "sabidos" };
        sing = { f = "sabida", m = "sabido" };
      };
      part_pres = "sabendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "soubermos", ["2"] = "souberdes", ["3"] = "souberem" };
          sing = { ["1"] = "souber", ["2"] = "souberes", ["3"] = "souber" };
        };
        impf =
        {
          plur = { ["1"] = "soubessémos", ["2"] = "soubesséis", ["3"] = "soubessem" };
          sing = { ["1"] = "soubesse", ["2"] = "soubesses", ["3"] = "soubesse" };
        };
        pres =
        {
          plur = { ["1"] = "saibamos", ["2"] = "saibais", ["3"] = "saibam" };
          sing = { ["1"] = "saiba", ["2"] = "saibas", ["3"] = "saiba" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "saber";
  };
  saudar =
  {
    comments = { };
    examples = { "saudar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "saudemos", ["2"] = "saudai", ["3"] = "saúdem" };
          sing = { ["2"] = "saúda", ["3"] = "saúde" };
        };
        negt =
        {
          plur = { ["1"] = "saudemos", ["2"] = "saudeis", ["3"] = "saúdem" };
          sing = { ["2"] = "saúdes", ["3"] = "saúde" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "saudaríamos", ["2"] = "saudaríeis", ["3"] = "saudariam" };
          sing = { ["1"] = "saudaria", ["2"] = "saudarias", ["3"] = "saudaria" };
        };
        futu =
        {
          plur = { ["1"] = "saudaremos", ["2"] = "saudareis", ["3"] = "saudarão" };
          sing = { ["1"] = "saudarei", ["2"] = "saudarás", ["3"] = "saudará" };
        };
        impf =
        {
          plur = { ["1"] = "saudávamos", ["2"] = "saudáveis", ["3"] = "saudavam" };
          sing = { ["1"] = "saudava", ["2"] = "saudavas", ["3"] = "saudava" };
        };
        plpf =
        {
          plur = { ["1"] = "saudáramos", ["2"] = "saudáreis", ["3"] = "saudaram" };
          sing = { ["1"] = "saudara", ["2"] = "saudaras", ["3"] = "saudara" };
        };
        pres =
        {
          plur = { ["1"] = "saudamos", ["2"] = "saudais", ["3"] = "saúdam" };
          sing = { ["1"] = "saúdo", ["2"] = "saúdas", ["3"] = "saúda" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "saudamos";
            ["1_alt"] = { "saudámos" };
            ["2"] = "saudastes";
            ["3"] = "saudaram";
          };
          sing = { ["1"] = "saudei", ["2"] = "saudaste", ["3"] = "saudou" };
        };
      };
      infn =
      {
        impe = "saudar";
        pers =
        {
          plur = { ["1"] = "saudarmos", ["2"] = "saudardes", ["3"] = "saudarem" };
          sing = { ["1"] = "saudar", ["2"] = "saudares", ["3"] = "saudar" };
        };
      };
      part_past =
      {
        plur = { f = "saudadas", m = "saudados" };
        sing = { f = "saudada", m = "saudado" };
      };
      part_pres = "saudando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "saudarmos", ["2"] = "saudardes", ["3"] = "saudarem" };
          sing = { ["1"] = "saudar", ["2"] = "saudares", ["3"] = "saudar" };
        };
        impf =
        {
          plur = { ["1"] = "saudássemos", ["2"] = "saudásseis", ["3"] = "saudassem" };
          sing = { ["1"] = "saudasse", ["2"] = "saudasses", ["3"] = "saudasse" };
        };
        pres =
        {
          plur = { ["1"] = "saudemos", ["2"] = "saudeis", ["3"] = "saúdem" };
          sing = { ["1"] = "saúde", ["2"] = "saúdes", ["3"] = "saúde" };
        };
      };
    };
    suffix = "ar";
    verb = "saudar";
  };
  screver =
  {
    comments = { };
    examples = { "escrever", "descrever" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "screvamos", ["2"] = "screvei", ["3"] = "screvam" };
          sing = { ["2"] = "screve", ["3"] = "screva" };
        };
        negt =
        {
          plur = { ["1"] = "screvamos", ["2"] = "screvais", ["3"] = "screvam" };
          sing = { ["2"] = "screvas", ["3"] = "screva" };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "screveríamos";
            ["2"] = "screveríeis";
            ["3"] = "screveriam";
          };
          sing = { ["1"] = "screveria", ["2"] = "screverias", ["3"] = "screveria" };
        };
        futu =
        {
          plur = { ["1"] = "screveremos", ["2"] = "screvereis", ["3"] = "screverão" };
          sing = { ["1"] = "screverei", ["2"] = "screverás", ["3"] = "screverá" };
        };
        impf =
        {
          plur = { ["1"] = "screvíamos", ["2"] = "screvíeis", ["3"] = "screviam" };
          sing = { ["1"] = "screvia", ["2"] = "screvias", ["3"] = "screvia" };
        };
        plpf =
        {
          plur = { ["1"] = "screvêramos", ["2"] = "screvêreis", ["3"] = "screveram" };
          sing = { ["1"] = "screvera", ["2"] = "screveras", ["3"] = "screvera" };
        };
        pres =
        {
          plur = { ["1"] = "screvemos", ["2"] = "screveis", ["3"] = "screvem" };
          sing = { ["1"] = "screvo", ["2"] = "screves", ["3"] = "screve" };
        };
        pret =
        {
          plur = { ["1"] = "screvemos", ["2"] = "screvestes", ["3"] = "screveram" };
          sing = { ["1"] = "screvi", ["2"] = "screveste", ["3"] = "screveu" };
        };
      };
      infn =
      {
        impe = "screver";
        pers =
        {
          plur = { ["1"] = "screvermos", ["2"] = "screverdes", ["3"] = "screverem" };
          sing = { ["1"] = "screver", ["2"] = "screveres", ["3"] = "screver" };
        };
      };
      part_past =
      {
        plur = { f = "scritas", m = "scritos" };
        sing = { f = "scrita", m = "scrito" };
      };
      part_pres = "screvendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "screvermos", ["2"] = "screverdes", ["3"] = "screverem" };
          sing = { ["1"] = "screver", ["2"] = "screveres", ["3"] = "screver" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "screvêssemos";
            ["2"] = "screvêsseis";
            ["3"] = "screvessem";
          };
          sing = { ["1"] = "screvesse", ["2"] = "screvesses", ["3"] = "screvesse" };
        };
        pres =
        {
          plur = { ["1"] = "screvamos", ["2"] = "screvais", ["3"] = "screvam" };
          sing = { ["1"] = "screva", ["2"] = "screvas", ["3"] = "screva" };
        };
      };
    };
    suffix = "er";
    verb = "screver";
  };
  seguir =
  {
    comments = { };
    examples = { "conseguir", "perseguir", "prosseguir", "seguir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "sigamos", ["2"] = "segui", ["3"] = "sigam" };
          sing = { ["1"] = "siga", ["2"] = "segue", ["3"] = "siga" };
        };
        negt =
        {
          plur = { ["1"] = "sigamos", ["2"] = "sigais", ["3"] = "sigam" };
          sing = { ["1"] = "siga", ["2"] = "sigas", ["3"] = "siga" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "seguiríamos", ["2"] = "seguiríeis", ["3"] = "seguiriam" };
          sing = { ["1"] = "seguiria", ["2"] = "seguirias", ["3"] = "seguiria" };
        };
        futu =
        {
          plur = { ["1"] = "seguiremos", ["2"] = "seguireis", ["3"] = "seguirão" };
          sing = { ["1"] = "seguirei", ["2"] = "seguirás", ["3"] = "seguirá" };
        };
        impf =
        {
          plur = { ["1"] = "seguíamos", ["2"] = "seguíeis", ["3"] = "seguiam" };
          sing = { ["1"] = "seguia", ["2"] = "seguias", ["3"] = "seguia" };
        };
        plpf =
        {
          plur = { ["1"] = "seguíramos", ["2"] = "seguíreis", ["3"] = "seguiram" };
          sing = { ["1"] = "seguira", ["2"] = "seguiras", ["3"] = "seguira" };
        };
        pres =
        {
          plur = { ["1"] = "seguimos", ["2"] = "seguis", ["3"] = "seguem" };
          sing = { ["1"] = "sigo", ["2"] = "segues", ["3"] = "segue" };
        };
        pret =
        {
          plur = { ["1"] = "seguimos", ["2"] = "seguistes", ["3"] = "seguiram" };
          sing = { ["1"] = "segui", ["2"] = "seguiste", ["3"] = "seguiu" };
        };
      };
      infn =
      {
        impe = "seguir";
        pers =
        {
          plur = { ["1"] = "seguirmos", ["2"] = "seguirdes", ["3"] = "seguirem" };
          sing = { ["1"] = "seguir", ["2"] = "seguires", ["3"] = "seguir" };
        };
      };
      part_past =
      {
        plur = { f = "seguidas", m = "seguidos" };
        sing = { f = "seguida", m = "seguido" };
      };
      part_pres = "seguindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "seguirmos", ["2"] = "seguirdes", ["3"] = "seguirem" };
          sing = { ["1"] = "seguir", ["2"] = "seguires", ["3"] = "seguir" };
        };
        impf =
        {
          plur = { ["1"] = "seguíssemos", ["2"] = "seguísseis", ["3"] = "seguissem" };
          sing = { ["1"] = "seguisse", ["2"] = "seguisses", ["3"] = "seguisse" };
        };
        pres =
        {
          plur = { ["1"] = "sigamos", ["2"] = "sigais", ["3"] = "sigam" };
          sing = { ["1"] = "siga", ["2"] = "sigas", ["3"] = "siga" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "seguir";
  };
  ser =
  {
    comments = { };
    examples = { "ser" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "sejamos", ["2"] = "sede", ["3"] = "sejam" };
          sing = { ["1"] = "seja", ["2"] = "sê", ["3"] = "seja" };
        };
        negt =
        {
          plur = { ["1"] = "sejamos", ["2"] = "sejais", ["3"] = "sejam" };
          sing = { ["1"] = "seja", ["2"] = "sejas", ["3"] = "seja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "seríamos", ["2"] = "seríeis", ["3"] = "seriam" };
          sing = { ["1"] = "seria", ["2"] = "serias", ["3"] = "seria" };
        };
        futu =
        {
          plur = { ["1"] = "seremos", ["2"] = "sereis", ["3"] = "serão" };
          sing = { ["1"] = "serei", ["2"] = "serás", ["3"] = "será" };
        };
        impf =
        {
          plur = { ["1"] = "éramos", ["2"] = "éreis", ["3"] = "eram" };
          sing = { ["1"] = "era", ["2"] = "eras", ["3"] = "era" };
        };
        plpf =
        {
          plur = { ["1"] = "fôramos", ["2"] = "fôreis", ["3"] = "foram" };
          sing = { ["1"] = "fora", ["2"] = "foras", ["3"] = "fora" };
        };
        pres =
        {
          plur = { ["1"] = "somos", ["2"] = "sois", ["3"] = "são" };
          sing = { ["1"] = "sou", ["2"] = "és", ["3"] = "é" };
        };
        pret =
        {
          plur = { ["1"] = "fomos", ["2"] = "fostes", ["3"] = "foram" };
          sing = { ["1"] = "fui", ["2"] = "foste", ["3"] = "foi" };
        };
      };
      infn =
      {
        impe = "ser";
        pers =
        {
          plur = { ["1"] = "sermos", ["2"] = "serdes", ["3"] = "serem" };
          sing = { ["1"] = "ser", ["2"] = "seres", ["3"] = "ser" };
        };
      };
      part_past =
      {
        plur = { f = "sidas", m = "sidos" };
        sing = { f = "sida", m = "sido" };
      };
      part_pres = "sendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "formos", ["2"] = "fordes", ["3"] = "forem" };
          sing = { ["1"] = "for", ["2"] = "fores", ["3"] = "for" };
        };
        impf =
        {
          plur = { ["1"] = "fôssemos", ["2"] = "fôsseis", ["3"] = "fossem" };
          sing = { ["1"] = "fosse", ["2"] = "fosses", ["3"] = "fosse" };
        };
        pres =
        {
          plur = { ["1"] = "sejamos", ["2"] = "sejais", ["3"] = "sejam" };
          sing = { ["1"] = "seja", ["2"] = "sejas", ["3"] = "seja" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "ser";
  };
  star =
  {
    comments = { };
    examples = { "sobestar", "sobrestar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "stejamos", ["2"] = "stai", ["3"] = "stejam" };
          sing = { ["1"] = "steja", ["2"] = "stá", ["3"] = "steja" };
        };
        negt =
        {
          plur = { ["1"] = "stejamos", ["2"] = "stejais", ["3"] = "stejam" };
          sing = { ["1"] = "steja", ["2"] = "stejas", ["3"] = "steja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "staríamos", ["2"] = "staríeis", ["3"] = "stariam" };
          sing = { ["1"] = "staria", ["2"] = "starias", ["3"] = "staria" };
        };
        futu =
        {
          plur = { ["1"] = "staremos", ["2"] = "stareis", ["3"] = "starão" };
          sing = { ["1"] = "starei", ["2"] = "stareis", ["3"] = "stará" };
        };
        impf =
        {
          plur = { ["1"] = "stávamos", ["2"] = "stáveis", ["3"] = "stávam" };
          sing = { ["1"] = "stava", ["2"] = "stavas", ["3"] = "stava" };
        };
        plpf =
        {
          plur = { ["1"] = "stivéramos", ["2"] = "stivéreis", ["3"] = "stiveram" };
          sing = { ["1"] = "stivera", ["2"] = "stiveras", ["3"] = "stivera" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "stamos";
            ["1_alt"] = { "stámos" };
            ["2"] = "stais";
            ["3"] = "stão";
          };
          sing = { ["1"] = "stou", ["2"] = "stás", ["3"] = "stá" };
        };
        pret =
        {
          plur = { ["1"] = "stivemos", ["2"] = "stivestes", ["3"] = "stiveram" };
          sing = { ["1"] = "stive", ["2"] = "stiveste", ["3"] = "steve" };
        };
      };
      infn =
      {
        impe = "star";
        pers =
        {
          plur = { ["1"] = "starmos", ["2"] = "stardes", ["3"] = "starem" };
          sing = { ["1"] = "star", ["2"] = "stares", ["3"] = "star" };
        };
      };
      part_past =
      {
        plur = { f = "stadas", m = "stados" };
        sing = { f = "stada", m = "stado" };
      };
      part_pres = "stando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "stivermos", ["2"] = "stiverdes", ["3"] = "stiverem" };
          sing = { ["1"] = "stiver", ["2"] = "stiveres", ["3"] = "stiver" };
        };
        impf =
        {
          plur = { ["1"] = "stivéssemos", ["2"] = "stivéssies", ["3"] = "stivessem" };
          sing = { ["1"] = "stivesse", ["2"] = "stivesses", ["3"] = "stivesse" };
        };
        pres =
        {
          plur = { ["1"] = "stejamos", ["2"] = "stejais", ["3"] = "stejam" };
          sing = { ["1"] = "steja", ["2"] = "stejas", ["3"] = "steja" };
        };
      };
    };
    irregular = true;
    suffix = "ar";
    verb = "star";
  };
  subir =
  {
    comments = { };
    examples = { "subir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "subamos", ["2"] = "subi", ["3"] = "subam" };
          sing = { ["2"] = "sobe", ["3"] = "suba" };
        };
        negt =
        {
          plur = { ["1"] = "subamos", ["2"] = "subais", ["3"] = "subam" };
          sing = { ["2"] = "subas", ["3"] = "suba" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "subiríamos", ["2"] = "subiríeis", ["3"] = "subiriam" };
          sing = { ["1"] = "subiria", ["2"] = "subirias", ["3"] = "subiria" };
        };
        futu =
        {
          plur = { ["1"] = "subiremos", ["2"] = "subireis", ["3"] = "subirão" };
          sing = { ["1"] = "subirei", ["2"] = "subirás", ["3"] = "subirá" };
        };
        impf =
        {
          plur = { ["1"] = "subíamos", ["2"] = "subíeis", ["3"] = "subiam" };
          sing = { ["1"] = "subia", ["2"] = "subias", ["3"] = "subia" };
        };
        plpf =
        {
          plur = { ["1"] = "subíramos", ["2"] = "subíreis", ["3"] = "subiram" };
          sing = { ["1"] = "subira", ["2"] = "subiras", ["3"] = "subira" };
        };
        pres =
        {
          plur = { ["1"] = "subimos", ["2"] = "subis", ["3"] = "sobem" };
          sing = { ["1"] = "subo", ["2"] = "sobes", ["3"] = "sobe" };
        };
        pret =
        {
          plur = { ["1"] = "subimos", ["2"] = "subistes", ["3"] = "subiram" };
          sing = { ["1"] = "subi", ["2"] = "subiste", ["3"] = "subiu" };
        };
      };
      infn =
      {
        impe = "subir";
        pers =
        {
          plur = { ["1"] = "subirmos", ["2"] = "subirdes", ["3"] = "subirem" };
          sing = { ["1"] = "subir", ["2"] = "subires", ["3"] = "subir" };
        };
      };
      part_past =
      {
        plur = { f = "subidas", m = "subidos" };
        sing = { f = "subida", m = "subido" };
      };
      part_pres = "subindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "subirmos", ["2"] = "subirdes", ["3"] = "subirem" };
          sing = { ["1"] = "subir", ["2"] = "subires", ["3"] = "subir" };
        };
        impf =
        {
          plur = { ["1"] = "subíssemos", ["2"] = "subísseis", ["3"] = "subissem" };
          sing = { ["1"] = "subisse", ["2"] = "subisses", ["3"] = "subisse" };
        };
        pres =
        {
          plur = { ["1"] = "subamos", ["2"] = "subais", ["3"] = "subam" };
          sing = { ["1"] = "suba", ["2"] = "subas", ["3"] = "suba" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "subir";
  };
  ter =
  {
    comments = { };
    examples = { "ter", "abster" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tende", ["3"] = "tenham" };
          sing = { ["1"] = "tenha", ["2"] = "tenhas", ["3"] = "tenha" };
        };
        negt =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tenhais", ["3"] = "tenham" };
          sing = { ["1"] = "tenha", ["2"] = "tenhas", ["3"] = "tenha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "teríamos", ["2"] = "teríeis", ["3"] = "teriam" };
          sing = { ["1"] = "teria", ["2"] = "terias", ["3"] = "teria" };
        };
        futu =
        {
          plur = { ["1"] = "teremos", ["2"] = "tereis", ["3"] = "terão" };
          sing = { ["1"] = "terei", ["2"] = "terás", ["3"] = "terá" };
        };
        impf =
        {
          plur = { ["1"] = "tínhamos", ["2"] = "tínheis", ["3"] = "tinham" };
          sing = { ["1"] = "tinha", ["2"] = "tinhas", ["3"] = "tinha" };
        };
        plpf =
        {
          plur = { ["1"] = "tivéramos", ["2"] = "tivéreis", ["3"] = "tiveram" };
          sing = { ["1"] = "tivera", ["2"] = "tiveras", ["3"] = "tivera" };
        };
        pres =
        {
          plur = { ["1"] = "temos", ["2"] = "tendes", ["3"] = "têm" };
          sing = { ["1"] = "tenho", ["2"] = "tens", ["3"] = "tem" };
        };
        pret =
        {
          plur = { ["1"] = "tivemos", ["2"] = "tivestes", ["3"] = "tiveram" };
          sing = { ["1"] = "tive", ["2"] = "tiveste", ["3"] = "teve" };
        };
      };
      infn =
      {
        impe = "ter";
        pers =
        {
          plur = { ["1"] = "termos", ["2"] = "terdes", ["3"] = "terem" };
          sing = { ["1"] = "ter", ["2"] = "teres", ["3"] = "ter" };
        };
      };
      part_past =
      {
        plur = { f = "tidas", m = "tidos" };
        sing = { f = "tida", m = "tido" };
      };
      part_pres = "tendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "tivermos", ["2"] = "tiverdes", ["3"] = "tiverem" };
          sing = { ["1"] = "tiver", ["2"] = "tiveres", ["3"] = "tiver" };
        };
        impf =
        {
          plur = { ["1"] = "tivéssemos", ["2"] = "tivésseis", ["3"] = "tivessem" };
          sing = { ["1"] = "tivesse", ["2"] = "tivesses", ["3"] = "tivesse" };
        };
        pres =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tenhais", ["3"] = "tenham" };
          sing = { ["1"] = "tenha", ["2"] = "tenhas", ["3"] = "tenha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "ter";
  };
  ter2 =
  {
    comments = { };
    examples = { "conter", "suster" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tende", ["3"] = "tenham" };
          sing = { ["2"] = "tém", ["3"] = "tenha" };
        };
        negt =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tenhais", ["3"] = "tenham" };
          sing = { ["2"] = "tenhas", ["3"] = "tenha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "teríamos", ["2"] = "teríeis", ["3"] = "teriam" };
          sing = { ["1"] = "teria", ["2"] = "terias", ["3"] = "teria" };
        };
        futu =
        {
          plur = { ["1"] = "teremos", ["2"] = "tereis", ["3"] = "terão" };
          sing = { ["1"] = "terei", ["2"] = "terás", ["3"] = "terá" };
        };
        impf =
        {
          plur = { ["1"] = "tínhamos", ["2"] = "tínheis", ["3"] = "tinham" };
          sing = { ["1"] = "tinha", ["2"] = "tinhas", ["3"] = "tinha" };
        };
        plpf =
        {
          plur = { ["1"] = "tivéramos", ["2"] = "tivéreis", ["3"] = "tiveram" };
          sing = { ["1"] = "tivera", ["2"] = "tiveras", ["3"] = "tivera" };
        };
        pres =
        {
          plur = { ["1"] = "temos", ["2"] = "tendes", ["3"] = "têm" };
          sing = { ["1"] = "tenho", ["2"] = "téns", ["3"] = "tém" };
        };
        pret =
        {
          plur = { ["1"] = "tivemos", ["2"] = "tivestes", ["3"] = "tiveram" };
          sing = { ["1"] = "tive", ["2"] = "tiveste", ["3"] = "teve" };
        };
      };
      infn =
      {
        impe = "ter";
        pers =
        {
          plur = { ["1"] = "termos", ["2"] = "terdes", ["3"] = "terem" };
          sing = { ["1"] = "ter", ["2"] = "teres", ["3"] = "ter" };
        };
      };
      part_past =
      {
        plur = { f = "tidas", m = "tidos" };
        sing = { f = "tida", m = "tido" };
      };
      part_pres = "tendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "tivermos", ["2"] = "tiverdes", ["3"] = "tiverem" };
          sing = { ["1"] = "tiver", ["2"] = "tiveres", ["3"] = "tiver" };
        };
        impf =
        {
          plur = { ["1"] = "tivéssemos", ["2"] = "tivésseis", ["3"] = "tivessem" };
          sing = { ["1"] = "tivesse", ["2"] = "tivesses", ["3"] = "tivesse" };
        };
        pres =
        {
          plur = { ["1"] = "tenhamos", ["2"] = "tenhais", ["3"] = "tenham" };
          sing = { ["1"] = "tenha", ["2"] = "tenhas", ["3"] = "tenha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "ter2";
  };
  tossir =
  {
    comments = { };
    examples = { "tossir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "tussamos", ["2"] = "tossi", ["3"] = "tussam" };
          sing = { ["2"] = "tosse", ["3"] = "tussa" };
        };
        negt =
        {
          plur = { ["1"] = "tussamos", ["2"] = "tussais", ["3"] = "tussam" };
          sing = { ["2"] = "tussas", ["3"] = "tussa" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "tossiríamos", ["2"] = "tossiríeis", ["3"] = "tossiriam" };
          sing = { ["1"] = "tossiria", ["2"] = "tossirias", ["3"] = "tossiria" };
        };
        futu =
        {
          plur = { ["1"] = "tossiremos", ["2"] = "tossireis", ["3"] = "tossirão" };
          sing = { ["1"] = "tossirei", ["2"] = "tossirás", ["3"] = "tossirá" };
        };
        impf =
        {
          plur = { ["1"] = "tossíamos", ["2"] = "tossíeis", ["3"] = "tossiam" };
          sing = { ["1"] = "tossia", ["2"] = "tossias", ["3"] = "tossia" };
        };
        plpf =
        {
          plur = { ["1"] = "tossíramos", ["2"] = "tossíreis", ["3"] = "tossiram" };
          sing = { ["1"] = "tossira", ["2"] = "tossiras", ["3"] = "tossira" };
        };
        pres =
        {
          plur = { ["1"] = "tossimos", ["2"] = "tossis", ["3"] = "tossem" };
          sing = { ["1"] = "tusso", ["2"] = "tosses", ["3"] = "tosse" };
        };
        pret =
        {
          plur = { ["1"] = "tossimos", ["2"] = "tossistes", ["3"] = "tossiram" };
          sing = { ["1"] = "tossi", ["2"] = "tossiste", ["3"] = "tossiu" };
        };
      };
      infn =
      {
        impe = "tossir";
        pers =
        {
          plur = { ["1"] = "tossirmos", ["2"] = "tossirdes", ["3"] = "tossirem" };
          sing = { ["1"] = "tossir", ["2"] = "tossires", ["3"] = "tossir" };
        };
      };
      part_past =
      {
        plur = { f = "tossidas", m = "tossidos" };
        sing = { f = "tossida", m = "tossido" };
      };
      part_pres = "tossindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "tossirmos", ["2"] = "tossirdes", ["3"] = "tossirem" };
          sing = { ["1"] = "tossir", ["2"] = "tossires", ["3"] = "tossir" };
        };
        impf =
        {
          plur = { ["1"] = "tossíssemos", ["2"] = "tossísseis", ["3"] = "tossissem" };
          sing = { ["1"] = "tossisse", ["2"] = "tossisses", ["3"] = "tossisse" };
        };
        pres =
        {
          plur = { ["1"] = "tussamos", ["2"] = "tussais", ["3"] = "tussam" };
          sing = { ["1"] = "tussa", ["2"] = "tussas", ["3"] = "tussa" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "tossir";
  };
  trazer =
  {
    comments = { };
    examples = { };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "trazamos", ["2"] = "trazei", ["3"] = "tragam" };
          sing = { ["1"] = "traga", ["2"] = "traze", ["3"] = "traga" };
        };
        negt =
        {
          plur = { ["1"] = "tragamos", ["2"] = "tragais", ["3"] = "tragam" };
          sing = { ["1"] = "traga", ["2"] = "tragas", ["3"] = "traga" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "traríamos", ["2"] = "traríeis", ["3"] = "trariam" };
          sing = { ["1"] = "traria", ["2"] = "trarias", ["3"] = "traria" };
        };
        futu =
        {
          plur = { ["1"] = "traremos", ["2"] = "trareis", ["3"] = "trarão" };
          sing = { ["1"] = "trarei", ["2"] = "trarás", ["3"] = "trará" };
        };
        impf =
        {
          plur = { ["1"] = "trazíamos", ["2"] = "trazíeis", ["3"] = "traziam" };
          sing = { ["1"] = "trazia", ["2"] = "trazias", ["3"] = "trazia" };
        };
        plpf =
        {
          plur = { ["1"] = "trouxéramos", ["2"] = "trouxéreis", ["3"] = "trouxeram" };
          sing = { ["1"] = "trouxera", ["2"] = "trouxeras", ["3"] = "trouxera" };
        };
        pres =
        {
          plur = { ["1"] = "trazemos", ["2"] = "trazeis", ["3"] = "trazem" };
          sing = { ["1"] = "trago", ["2"] = "trazes", ["3"] = "traz" };
        };
        pret =
        {
          plur = { ["1"] = "trouxemos", ["2"] = "trouxestes", ["3"] = "trouxeram" };
          sing = { ["1"] = "trouxe", ["2"] = "trouxeste", ["3"] = "trouxe" };
        };
      };
      infn =
      {
        impe = "trazer";
        pers =
        {
          plur = { ["1"] = "trazermos", ["2"] = "trazerdes", ["3"] = "trazerem" };
          sing = { ["1"] = "trazer", ["2"] = "trazeres", ["3"] = "trazer" };
        };
      };
      part_past =
      {
        plur = { f = "trazidas", m = "trazidos" };
        sing = { f = "trazida", m = "trazido" };
      };
      part_pres = "trazendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "trouxermos", ["2"] = "trouxerdes", ["3"] = "trouxerem" };
          sing = { ["1"] = "trouxer", ["2"] = "trouxeres", ["3"] = "trouxer" };
        };
        impf =
        {
          plur =
          {
            ["1"] = "trouxéssemos";
            ["2"] = "trouxésseis";
            ["3"] = "trouxessem";
          };
          sing = { ["1"] = "trouxesse", ["2"] = "trouxesses", ["3"] = "trouxesse" };
        };
        pres =
        {
          plur = { ["1"] = "tragamos", ["2"] = "tragais", ["3"] = "tragam" };
          sing = { ["1"] = "traga", ["2"] = "tragas", ["3"] = "traga" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "trazer";
  };
  truir =
  {
    comments = { };
    examples = { "construir", "desconstruir", "destruir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "truamos", ["2"] = "truí", ["3"] = "truam" };
          sing = { ["2"] = "trói", ["3"] = "trua" };
        };
        negt =
        {
          plur = { ["1"] = "truamos", ["2"] = "truais", ["3"] = "truam" };
          sing = { ["2"] = "truas", ["3"] = "trua" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "truiríamos", ["2"] = "truiríeis", ["3"] = "truiriam" };
          sing = { ["1"] = "truiria", ["2"] = "truirias", ["3"] = "truiria" };
        };
        futu =
        {
          plur = { ["1"] = "truiremos", ["2"] = "truireis", ["3"] = "truirão" };
          sing = { ["1"] = "truirei", ["2"] = "truirás", ["3"] = "truirá" };
        };
        impf =
        {
          plur = { ["1"] = "truíamos", ["2"] = "truíeis", ["3"] = "truíam" };
          sing = { ["1"] = "truía", ["2"] = "truias", ["3"] = "truía" };
        };
        plpf =
        {
          plur = { ["1"] = "truíramos", ["2"] = "truíreis", ["3"] = "truíram" };
          sing = { ["1"] = "truíra", ["2"] = "truíras", ["3"] = "truíra" };
        };
        pres =
        {
          plur = { ["1"] = "truímos", ["2"] = "truís", ["3"] = "troem" };
          sing = { ["1"] = "truo", ["2"] = "tróis", ["3"] = "trói" };
        };
        pret =
        {
          plur = { ["1"] = "truímos", ["2"] = "truístes", ["3"] = "truíram" };
          sing = { ["1"] = "truí", ["2"] = "truíste", ["3"] = "truiu" };
        };
      };
      infn =
      {
        impe = "truir";
        pers =
        {
          plur = { ["1"] = "truirmos", ["2"] = "truirdes", ["3"] = "truírem" };
          sing = { ["1"] = "truir", ["2"] = "truíres", ["3"] = "truir" };
        };
      };
      part_past =
      {
        plur = { f = "truídas", m = "truídos" };
        sing = { f = "truída", m = "truído" };
      };
      part_pres = "truindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "truirmos", ["2"] = "truirdes", ["3"] = "truirem" };
          sing = { ["1"] = "truir", ["2"] = "truires", ["3"] = "truir" };
        };
        impf =
        {
          plur = { ["1"] = "truíssemos", ["2"] = "truísseis", ["3"] = "truíssem" };
          sing = { ["1"] = "truísse", ["2"] = "truísses", ["3"] = "truísse" };
        };
        pres =
        {
          plur = { ["1"] = "truamos", ["2"] = "truais", ["3"] = "truam" };
          sing = { ["1"] = "trua", ["2"] = "truas", ["3"] = "trua" };
        };
      };
    };
    suffix = "ir";
    verb = "truir";
  };
  uar =
  {
    comments =
    {
      "An acute accent is added to a few conjugated forms.";
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced.";
    };
    examples = { "apaziguar", "averiguar", "santiguar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "uemos";
            ["1_obsolete"] = "üemos";
            ["2"] = "uai";
            ["3"] = "úem";
          };
          sing = { ["1"] = "úe", ["2"] = "ua", ["3"] = "úe" };
        };
        negt =
        {
          plur =
          {
            ["1"] = "uemos";
            ["1_obsolete"] = "üemos";
            ["2"] = "ueis";
            ["2_obsolete"] = "üeis";
            ["3"] = "úem";
          };
          sing = { ["1"] = "úe", ["2"] = "úes", ["3"] = "úe" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "uaríamos", ["2"] = "uaríeis", ["3"] = "uariam" };
          sing = { ["1"] = "uaria", ["2"] = "uarias", ["3"] = "uaria" };
        };
        futu =
        {
          plur = { ["1"] = "uaremos", ["2"] = "uareis", ["3"] = "uarão" };
          sing = { ["1"] = "uarei", ["2"] = "uarás", ["3"] = "uará" };
        };
        impf =
        {
          plur = { ["1"] = "uávamos", ["2"] = "uáveis", ["3"] = "uavam" };
          sing = { ["1"] = "uava", ["2"] = "uavas", ["3"] = "uava" };
        };
        plpf =
        {
          plur = { ["1"] = "uáramos", ["2"] = "uáreis", ["3"] = "uaram" };
          sing = { ["1"] = "uara", ["2"] = "uaras", ["3"] = "uara" };
        };
        pres =
        {
          plur = { ["1"] = "uamos", ["2"] = "uais", ["3"] = "uam" };
          sing = { ["1"] = "uo", ["2"] = "uas", ["3"] = "ua" };
        };
        pret =
        {
          plur =
          {
            ["1"] = "uamos";
            ["1_alt"] = { "uámos" };
            ["2"] = "uastes";
            ["3"] = "uaram";
          };
          sing =
          {
            ["1"] = "uei";
            ["1_obsolete"] = "üei";
            ["2"] = "uaste";
            ["3"] = "uou";
          };
        };
      };
      infn =
      {
        impe = "uar";
        pers =
        {
          plur = { ["1"] = "uarmos", ["2"] = "uardes", ["3"] = "uarem" };
          sing = { ["1"] = "uar", ["2"] = "uares", ["3"] = "uar" };
        };
      };
      part_past =
      {
        plur = { f = "uadas", m = "uados" };
        sing = { f = "uada", m = "uado" };
      };
      part_pres = "uando";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "uarmos", ["2"] = "uardes", ["3"] = "uarem" };
          sing = { ["1"] = "uar", ["2"] = "uares", ["3"] = "uar" };
        };
        impf =
        {
          plur = { ["1"] = "uássemos", ["2"] = "uásseis", ["3"] = "uassem" };
          sing = { ["1"] = "uasse", ["2"] = "uasses", ["3"] = "uasse" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "uemos";
            ["1_obsolete"] = "üemos";
            ["2"] = "ueis";
            ["2_obsolete"] = "üeis";
            ["3"] = "úem";
          };
          sing = { ["1"] = "úe", ["2"] = "úes", ["3"] = "úe" };
        };
      };
    };
    suffix = "ar";
    verb = "uar";
  };
  uentar =
  {
    comments =
    {
      "An obsolete treatment is to add a diaresis when the letter ''u'' is pronounced. ";
    };
    examples = { "aguentar", "ensanguentar", "frequentar" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur =
          {
            ["1"] = "uentemos";
            ["1_obsolete"] = "üentemos";
            ["2"] = "uentai";
            ["2_obsolete"] = "üentai";
            ["3"] = "uentem";
            ["3_obsolete"] = "üentem";
          };
          sing =
          {
            ["1"] = "uente";
            ["1_obsolete"] = "üente";
            ["2"] = "uenta";
            ["2_obsolete"] = "üenta";
            ["3"] = "uente";
            ["3_obsolete"] = "üente";
          };
        };
        negt =
        {
          plur =
          {
            ["1"] = "uentemos";
            ["1_obsolete"] = "üentemos";
            ["2"] = "uenteis";
            ["2_obsolete"] = "üenteis";
            ["3"] = "uentem";
            ["3_obsolete"] = "üentem";
          };
          sing =
          {
            ["1"] = "uente";
            ["1_obsolete"] = "üente";
            ["2"] = "uentes";
            ["2_obsolete"] = "üentes";
            ["3"] = "uente";
            ["3_obsolete"] = "üente";
          };
        };
      };
      indi =
      {
        cond =
        {
          plur =
          {
            ["1"] = "uentaríamos";
            ["1_obsolete"] = "üentaríamos";
            ["2"] = "uentaríeis";
            ["2_obsolete"] = "üentaríeis";
            ["3"] = "uentariam";
            ["3_obsolete"] = "üentariam";
          };
          sing =
          {
            ["1"] = "uentaria";
            ["1_obsolete"] = "üentaria";
            ["2"] = "uentarias";
            ["2_obsolete"] = "üentarias";
            ["3"] = "uentaria";
            ["3_obsolete"] = "üentaria";
          };
        };
        futu =
        {
          plur =
          {
            ["1"] = "uentaremos";
            ["1_obsolete"] = "üentaremos";
            ["2"] = "uentareis";
            ["2_obsolete"] = "üentareis";
            ["3"] = "uentarão";
            ["3_obsolete"] = "üentarão";
          };
          sing =
          {
            ["1"] = "uentarei";
            ["1_obsolete"] = "üentarei";
            ["2"] = "uentarás";
            ["2_obsolete"] = "üentarás";
            ["3"] = "uentará";
            ["3_obsolete"] = "üentará";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "uentávamos";
            ["1_obsolete"] = "üentávamos";
            ["2"] = "uentáveis";
            ["2_obsolete"] = "üentáveis";
            ["3"] = "uentavam";
            ["3_obsolete"] = "üentavam";
          };
          sing =
          {
            ["1"] = "uentava";
            ["1_obsolete"] = "üentava";
            ["2"] = "uentavas";
            ["2_obsolete"] = "üentavas";
            ["3"] = "uentava";
            ["3_obsolete"] = "üentava";
          };
        };
        plpf =
        {
          plur =
          {
            ["1"] = "uentáramos";
            ["1_obsolete"] = "üentáramos";
            ["2"] = "uentáreis";
            ["2_obsolete"] = "üentáreis";
            ["3"] = "uentaram";
            ["3_obsolete"] = "üentaram";
          };
          sing =
          {
            ["1"] = "uentara";
            ["1_obsolete"] = "üentara";
            ["2"] = "uentaras";
            ["2_obsolete"] = "üentaras";
            ["3"] = "uentara";
            ["3_obsolete"] = "üentara";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "uentamos";
            ["1_obsolete"] = "üentamos";
            ["2"] = "uentais";
            ["2_obsolete"] = "üentais";
            ["3"] = "uentam";
            ["3_obsolete"] = "üentam";
          };
          sing =
          {
            ["1"] = "uento";
            ["1_obsolete"] = "üento";
            ["2"] = "uentas";
            ["2_obsolete"] = "üentas";
            ["3"] = "uenta";
            ["3_obsolete"] = "üenta";
          };
        };
        pret =
        {
          plur =
          {
            ["1"] = "uentamos";
            ["1_alt"] = { "uentámos" };
            ["1_obsolete"] = "üentamos";
            ["1_obsolete_alt"] = { "üentámos" };
            ["2"] = "uentastes";
            ["2_obsolete"] = "üentastes";
            ["3"] = "uentaram";
            ["3_obsolete"] = "üentaram";
          };
          sing =
          {
            ["1"] = "uentei";
            ["1_obsolete"] = "üentei";
            ["2"] = "uentaste";
            ["2_obsolete"] = "üentaste";
            ["3"] = "uentou";
            ["3_obsolete"] = "üentou";
          };
        };
      };
      infn =
      {
        impe = "üentar";
        pers =
        {
          plur =
          {
            ["1"] = "uentarmos";
            ["1_obsolete"] = "üentarmos";
            ["2"] = "uentardes";
            ["2_obsolete"] = "üentardes";
            ["3"] = "uentarem";
            ["3_obsolete"] = "üentarem";
          };
          sing =
          {
            ["1"] = "uentar";
            ["1_obsolete"] = "üentar";
            ["2"] = "uentares";
            ["2_obsolete"] = "üentares";
            ["3"] = "uentar";
            ["3_obsolete"] = "üentar";
          };
        };
      };
      part_past =
      {
        plur = { f = "üentadas", m = "üentados" };
        sing = { f = "üentada", m = "üentado" };
      };
      part_pres = "üentando";
      subj =
      {
        futu =
        {
          plur =
          {
            ["1"] = "uentarmos";
            ["1_obsolete"] = "üentarmos";
            ["2"] = "uentardes";
            ["2_obsolete"] = "üentardes";
            ["3"] = "uentarem";
            ["3_obsolete"] = "üentarem";
          };
          sing =
          {
            ["1"] = "uentar";
            ["1_obsolete"] = "üentar";
            ["2"] = "uentares";
            ["2_obsolete"] = "üentares";
            ["3"] = "uentar";
            ["3_obsolete"] = "üentar";
          };
        };
        impf =
        {
          plur =
          {
            ["1"] = "uentássemos";
            ["1_obsolete"] = "üentássemos";
            ["2"] = "uentásseis";
            ["2_obsolete"] = "üentásseis";
            ["3"] = "uentassem";
            ["3_obsolete"] = "üentassem";
          };
          sing =
          {
            ["1"] = "uentasse";
            ["1_obsolete"] = "üentasse";
            ["2"] = "uentasses";
            ["2_obsolete"] = "üentasses";
            ["3"] = "uentasse";
            ["3_obsolete"] = "üentasse";
          };
        };
        pres =
        {
          plur =
          {
            ["1"] = "uentemos";
            ["1_obsolete"] = "üentemos";
            ["2"] = "uenteis";
            ["2_obsolete"] = "üenteis";
            ["3"] = "uentem";
            ["3_obsolete"] = "üentem";
          };
          sing =
          {
            ["1"] = "uente";
            ["1_obsolete"] = "üente";
            ["2"] = "uentes";
            ["2_obsolete"] = "üentes";
            ["3"] = "uente";
            ["3_obsolete"] = "üente";
          };
        };
      };
    };
    suffix = "ar";
    verb = "uentar";
  };
  ugir =
  {
    comments = { };
    examples = { "fugir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "ujamos", ["2"] = "ugi", ["3"] = "ujam" };
          sing = { ["2"] = "oge", ["3"] = "uja" };
        };
        negt =
        {
          plur = { ["1"] = "ujamos", ["2"] = "ujais", ["3"] = "ujam" };
          sing = { ["2"] = "ujas", ["3"] = "uja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "ugiríamos", ["2"] = "ugiríeis", ["3"] = "ugiriam" };
          sing = { ["1"] = "ugiria", ["2"] = "ugirias", ["3"] = "ugiria" };
        };
        futu =
        {
          plur = { ["1"] = "ugiremos", ["2"] = "ugireis", ["3"] = "ugirão" };
          sing = { ["1"] = "ugirei", ["2"] = "ugirás", ["3"] = "ugirá" };
        };
        impf =
        {
          plur = { ["1"] = "ugíamos", ["2"] = "ugíeis", ["3"] = "ugiam" };
          sing = { ["1"] = "ugia", ["2"] = "ugias", ["3"] = "ugia" };
        };
        plpf =
        {
          plur = { ["1"] = "ugíramos", ["2"] = "ugíreis", ["3"] = "ugiram" };
          sing = { ["1"] = "ugira", ["2"] = "ugiras", ["3"] = "ugira" };
        };
        pres =
        {
          plur = { ["1"] = "ugimos", ["2"] = "ugis", ["3"] = "ogem" };
          sing = { ["1"] = "ujo", ["2"] = "oges", ["3"] = "oge" };
        };
        pret =
        {
          plur = { ["1"] = "ugimos", ["2"] = "ugistes", ["3"] = "ugiram" };
          sing = { ["1"] = "ugi", ["2"] = "ugiste", ["3"] = "ugiu" };
        };
      };
      infn =
      {
        impe = "ugir";
        pers =
        {
          plur = { ["1"] = "ugirmos", ["2"] = "ugirdes", ["3"] = "ugirem" };
          sing = { ["1"] = "ugir", ["2"] = "ugires", ["3"] = "ugir" };
        };
      };
      part_past =
      {
        plur = { f = "ugidas", m = "ugidos" };
        sing = { f = "ugida", m = "ugido" };
      };
      part_pres = "ugindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "ugirmos", ["2"] = "ugirdes", ["3"] = "ugirem" };
          sing = { ["1"] = "ugir", ["2"] = "ugires", ["3"] = "ugir" };
        };
        impf =
        {
          plur = { ["1"] = "ugíssemos", ["2"] = "ugísseis", ["3"] = "ugissem" };
          sing = { ["1"] = "ugisse", ["2"] = "ugisses", ["3"] = "ugisse" };
        };
        pres =
        {
          plur = { ["1"] = "ujamos", ["2"] = "ujais", ["3"] = "ujam" };
          sing = { ["1"] = "uja", ["2"] = "ujas", ["3"] = "uja" };
        };
      };
    };
    suffix = "ir";
    verb = "ugir";
  };
  uir =
  {
    comments = { };
    examples = { "atribuir", "constituir", "substituir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "uamos", ["2"] = "uí", ["3"] = "uam" };
          sing = { ["2"] = "ui", ["3"] = "ua" };
        };
        negt =
        {
          plur = { ["1"] = "uamos", ["2"] = "uais", ["3"] = "uam" };
          sing = { ["2"] = "uas", ["3"] = "ua" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "uiríamos", ["2"] = "uiríeis", ["3"] = "uiriam" };
          sing = { ["1"] = "uiria", ["2"] = "uirias", ["3"] = "uiria" };
        };
        futu =
        {
          plur = { ["1"] = "uiremos", ["2"] = "uireis", ["3"] = "uirão" };
          sing = { ["1"] = "uirei", ["2"] = "uirás", ["3"] = "uirá" };
        };
        impf =
        {
          plur = { ["1"] = "uíamos", ["2"] = "uíeis", ["3"] = "uíam" };
          sing = { ["1"] = "uía", ["2"] = "uías", ["3"] = "uía" };
        };
        plpf =
        {
          plur = { ["1"] = "uíramos", ["2"] = "uíreis", ["3"] = "uíram" };
          sing = { ["1"] = "uíra", ["2"] = "uíras", ["3"] = "uíra" };
        };
        pres =
        {
          plur = { ["1"] = "uímos", ["2"] = "uís", ["3"] = "uem" };
          sing = { ["1"] = "uo", ["2"] = "uis", ["3"] = "ui" };
        };
        pret =
        {
          plur = { ["1"] = "uímos", ["2"] = "uístes", ["3"] = "uíram" };
          sing = { ["1"] = "uí", ["2"] = "uíste", ["3"] = "uiu" };
        };
      };
      infn =
      {
        impe = "uir";
        pers =
        {
          plur = { ["1"] = "uirmos", ["2"] = "uirdes", ["3"] = "uírem" };
          sing = { ["1"] = "uir", ["2"] = "uíres", ["3"] = "uir" };
        };
      };
      part_past =
      {
        plur = { f = "uídas", m = "uídos" };
        sing = { f = "uída", m = "uído" };
      };
      part_pres = "uindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "uirmos", ["2"] = "uirdes", ["3"] = "uírem" };
          sing = { ["1"] = "uir", ["2"] = "uíres", ["3"] = "uir" };
        };
        impf =
        {
          plur = { ["1"] = "uíssemos", ["2"] = "uísseis", ["3"] = "uíssem" };
          sing = { ["1"] = "uísse", ["2"] = "uísses", ["3"] = "uísse" };
        };
        pres =
        {
          plur = { ["1"] = "uamos", ["2"] = "uais", ["3"] = "uam" };
          sing = { ["1"] = "ua", ["2"] = "uas", ["3"] = "ua" };
        };
      };
    };
    suffix = "ir";
    verb = "uir";
  };
  umir =
  {
    comments = { };
    examples = { "sumir", "consumir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "umamos", ["2"] = "umi", ["3"] = "umam" };
          sing = { ["2"] = "ome", ["3"] = "uma" };
        };
        negt =
        {
          plur = { ["1"] = "umamos", ["2"] = "umais", ["3"] = "umam" };
          sing = { ["2"] = "umas", ["3"] = "uma" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "umiríamos", ["2"] = "umiríeis", ["3"] = "umiriam" };
          sing = { ["1"] = "umiria", ["2"] = "umirias", ["3"] = "umiria" };
        };
        futu =
        {
          plur = { ["1"] = "umiremos", ["2"] = "umireis", ["3"] = "umirão" };
          sing = { ["1"] = "umirei", ["2"] = "umirás", ["3"] = "umirá" };
        };
        impf =
        {
          plur = { ["1"] = "umíamos", ["2"] = "umíeis", ["3"] = "umiam" };
          sing = { ["1"] = "umia", ["2"] = "umias", ["3"] = "umia" };
        };
        plpf =
        {
          plur = { ["1"] = "umíramos", ["2"] = "umíreis", ["3"] = "umiram" };
          sing = { ["1"] = "umira", ["2"] = "umiras", ["3"] = "umira" };
        };
        pres =
        {
          plur = { ["1"] = "umimos", ["2"] = "umis", ["3"] = "omem" };
          sing = { ["1"] = "umo", ["2"] = "omes", ["3"] = "ome" };
        };
        pret =
        {
          plur = { ["1"] = "umimos", ["2"] = "umistes", ["3"] = "umiram" };
          sing = { ["1"] = "umi", ["2"] = "umiste", ["3"] = "umiu" };
        };
      };
      infn =
      {
        impe = "umir";
        pers =
        {
          plur = { ["1"] = "umirmos", ["2"] = "umirdes", ["3"] = "umirem" };
          sing = { ["1"] = "umir", ["2"] = "umires", ["3"] = "umir" };
        };
      };
      part_past =
      {
        plur = { f = "umidas", m = "umidos" };
        sing = { f = "umida", m = "umido" };
      };
      part_pres = "umindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "umirmos", ["2"] = "umirdes", ["3"] = "umirem" };
          sing = { ["1"] = "umir", ["2"] = "umires", ["3"] = "umir" };
        };
        impf =
        {
          plur = { ["1"] = "umíssemos", ["2"] = "umísseis", ["3"] = "umissem" };
          sing = { ["1"] = "umisse", ["2"] = "umisses", ["3"] = "umisse" };
        };
        pres =
        {
          plur = { ["1"] = "umamos", ["2"] = "umais", ["3"] = "umam" };
          sing = { ["1"] = "uma", ["2"] = "umas", ["3"] = "uma" };
        };
      };
    };
    suffix = "ir";
    verb = "umir";
  };
  uzir =
  {
    comments = { };
    examples = { };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "uzamos", ["2"] = "uzi", ["3"] = "uzam" };
          sing =
          {
            ["1"] = "uza";
            ["2"] = "uz";
            ["2_alt"] = { "uze" };
            ["3"] = "uza";
          };
        };
        negt =
        {
          plur = { ["1"] = "uzamos", ["2"] = "uzais", ["3"] = "uzam" };
          sing = { ["1"] = "uza", ["2"] = "uzas", ["3"] = "uza" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "uziríamos", ["2"] = "uziríeis", ["3"] = "uziriam" };
          sing = { ["1"] = "uziria", ["2"] = "uzirias", ["3"] = "uziria" };
        };
        futu =
        {
          plur = { ["1"] = "uziremos", ["2"] = "uzireis", ["3"] = "uzirão" };
          sing = { ["1"] = "uzirei", ["2"] = "uzirás", ["3"] = "uzirá" };
        };
        impf =
        {
          plur = { ["1"] = "uzíamos", ["2"] = "uzíeis", ["3"] = "uziam" };
          sing = { ["1"] = "uzia", ["2"] = "uzias", ["3"] = "uzia" };
        };
        plpf =
        {
          plur = { ["1"] = "uzíramos", ["2"] = "uzíreis", ["3"] = "uziram" };
          sing = { ["1"] = "uzira", ["2"] = "uziras", ["3"] = "uzira" };
        };
        pres =
        {
          plur = { ["1"] = "uzimos", ["2"] = "uzis", ["3"] = "uzem" };
          sing = { ["1"] = "uzo", ["2"] = "uzes", ["3"] = "uz" };
        };
        pret =
        {
          plur = { ["1"] = "uzimos", ["2"] = "uzistes", ["3"] = "uziram" };
          sing = { ["1"] = "uzi", ["2"] = "uziste", ["3"] = "uziu" };
        };
      };
      infn =
      {
        impe = "uzir";
        pers =
        {
          plur = { ["1"] = "uzirmos", ["2"] = "uzirdes", ["3"] = "uzirem" };
          sing = { ["1"] = "uzir", ["2"] = "uzires", ["3"] = "uzir" };
        };
      };
      part_past =
      {
        plur = { f = "uzidas", m = "uzidos" };
        sing = { f = "uzida", m = "uzido" };
      };
      part_pres = "uzindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "uzirmos", ["2"] = "uzirdes", ["3"] = "uzirem" };
          sing = { ["1"] = "uzir", ["2"] = "uzires", ["3"] = "uzir" };
        };
        impf =
        {
          plur = { ["1"] = "uzíssemos", ["2"] = "uzísseis", ["3"] = "uzissem" };
          sing = { ["1"] = "uzisse", ["2"] = "uzisses", ["3"] = "uzisse" };
        };
        pres =
        {
          plur = { ["1"] = "uzamos", ["2"] = "uzais", ["3"] = "uzam" };
          sing = { ["1"] = "uza", ["2"] = "uzas", ["3"] = "uza" };
        };
      };
    };
    suffix = "uzir";
    verb = "uzir";
  };
  valer =
  {
    abundant = true;
    comments = { };
    examples = { "desvaler", "equivaler", "valer" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "valhamos", ["2"] = "valei", ["3"] = "valham" };
          sing = { ["1"] = "valha", ["2"] = "vale", ["3"] = "valha" };
        };
        negt =
        {
          plur = { ["1"] = "valhamos", ["2"] = "valhais", ["3"] = "valham" };
          sing = { ["1"] = "valha", ["2"] = "valhas", ["3"] = "valha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "valeríamos", ["2"] = "valeríeis", ["3"] = "valeriam" };
          sing = { ["1"] = "valeria", ["2"] = "valerias", ["3"] = "valeria" };
        };
        futu =
        {
          plur = { ["1"] = "valeremos", ["2"] = "valereis", ["3"] = "valerão" };
          sing = { ["1"] = "valerei", ["2"] = "valerás", ["3"] = "valerá" };
        };
        impf =
        {
          plur = { ["1"] = "valíamos", ["2"] = "valíeis", ["3"] = "valiam" };
          sing = { ["1"] = "valia", ["2"] = "valias", ["3"] = "valia" };
        };
        plpf =
        {
          plur = { ["1"] = "valêramos", ["2"] = "valêreis", ["3"] = "valeram" };
          sing = { ["1"] = "valera", ["2"] = "valeras", ["3"] = "valera" };
        };
        pres =
        {
          plur = { ["1"] = "valemos", ["2"] = "valeis", ["3"] = "valem" };
          sing =
          {
            ["1"] = "valho";
            ["2"] = "vales";
            ["3"] = "vale";
            ["3_alt"] = { "val" };
          };
        };
        pret =
        {
          plur = { ["1"] = "valemos", ["2"] = "valestes", ["3"] = "valeram" };
          sing = { ["1"] = "vali", ["2"] = "valeste", ["3"] = "valeu" };
        };
      };
      infn =
      {
        impe = "valer";
        pers =
        {
          plur = { ["1"] = "valermos", ["2"] = "valerdes", ["3"] = "valerem" };
          sing = { ["1"] = "valer", ["2"] = "valeres", ["3"] = "valer" };
        };
      };
      part_past =
      {
        plur = { f = "validas", m = "validos" };
        sing = { f = "valida", m = "valido" };
      };
      part_pres = "valendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "valermos", ["2"] = "valerdes", ["3"] = "valerem" };
          sing = { ["1"] = "valer", ["2"] = "valeres", ["3"] = "valer" };
        };
        impf =
        {
          plur = { ["1"] = "valêssemos", ["2"] = "valêsseis", ["3"] = "valessem" };
          sing = { ["1"] = "valesse", ["2"] = "valesses", ["3"] = "valesse" };
        };
        pres =
        {
          plur = { ["1"] = "valhamos", ["2"] = "valhais", ["3"] = "valham" };
          sing = { ["1"] = "valha", ["2"] = "valhas", ["3"] = "valha" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "valer";
  };
  ver =
  {
    comments = { };
    examples = { "antever", "entrever", "prever", "rever", "ver" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "vejamos", ["2"] = "vede", ["3"] = "vejam" };
          sing = { ["1"] = "veja", ["2"] = "vê", ["3"] = "veja" };
        };
        negt =
        {
          plur = { ["1"] = "vejamos", ["2"] = "vejais", ["3"] = "vejam" };
          sing = { ["1"] = "veja", ["2"] = "vejas", ["3"] = "veja" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "veríamos", ["2"] = "veríeis", ["3"] = "veriam" };
          sing = { ["1"] = "veria", ["2"] = "verias", ["3"] = "veria" };
        };
        futu =
        {
          plur = { ["1"] = "veremos", ["2"] = "vereis", ["3"] = "verão" };
          sing = { ["1"] = "verei", ["2"] = "verás", ["3"] = "verá" };
        };
        impf =
        {
          plur = { ["1"] = "víamos", ["2"] = "víeis", ["3"] = "viam" };
          sing = { ["1"] = "via", ["2"] = "vias", ["3"] = "via" };
        };
        plpf =
        {
          plur = { ["1"] = "víramos", ["2"] = "víreis", ["3"] = "viram" };
          sing = { ["1"] = "vira", ["2"] = "viras", ["3"] = "vira" };
        };
        pres =
        {
          plur =
          {
            ["1"] = "vemos";
            ["2"] = "vedes";
            ["3"] = "veem";
            ["3_obsolete"] = "vêem";
          };
          sing = { ["1"] = "vejo", ["2"] = "vês", ["3"] = "vê" };
        };
        pret =
        {
          plur = { ["1"] = "vimos", ["2"] = "vistes", ["3"] = "viram" };
          sing = { ["1"] = "vi", ["2"] = "viste", ["3"] = "viu" };
        };
      };
      infn =
      {
        impe = "ver";
        pers =
        {
          plur = { ["1"] = "vermos", ["2"] = "verdes", ["3"] = "verem" };
          sing = { ["1"] = "ver", ["2"] = "veres", ["3"] = "ver" };
        };
      };
      part_past =
      {
        plur = { f = "vistas", m = "vistos" };
        sing = { f = "vista", m = "visto" };
      };
      part_pres = "vendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "virmos", ["2"] = "virdes", ["3"] = "virem" };
          sing = { ["1"] = "vir", ["2"] = "vires", ["3"] = "vir" };
        };
        impf =
        {
          plur = { ["1"] = "víssemos", ["2"] = "vísseis", ["3"] = "vissem" };
          sing = { ["1"] = "visse", ["2"] = "visses", ["3"] = "visse" };
        };
        pres =
        {
          plur = { ["1"] = "vejamos", ["2"] = "vejais", ["3"] = "vejam" };
          sing = { ["1"] = "veja", ["2"] = "vejas", ["3"] = "veja" };
        };
      };
    };
    irregular = true;
    suffix = "er";
    verb = "ver";
  };
  vestir =
  {
    comments = { };
    examples = { "vestir", "investir", "revestir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "vistamos", ["2"] = "vesti", ["3"] = "vistam" };
          sing = { ["2"] = "veste", ["3"] = "vista" };
        };
        negt =
        {
          plur = { ["1"] = "vistamos", ["2"] = "vistais", ["3"] = "vistam" };
          sing = { ["2"] = "vistas", ["3"] = "vista" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "vestiríamos", ["2"] = "vestiríeis", ["3"] = "vestiriam" };
          sing = { ["1"] = "vestiria", ["2"] = "vestirias", ["3"] = "vestiria" };
        };
        futu =
        {
          plur = { ["1"] = "vestiremos", ["2"] = "vestireis", ["3"] = "vestirão" };
          sing = { ["1"] = "vestirei", ["2"] = "vestirás", ["3"] = "vestirá" };
        };
        impf =
        {
          plur = { ["1"] = "vestíamos", ["2"] = "vestíeis", ["3"] = "vestiam" };
          sing = { ["1"] = "vestia", ["2"] = "vestias", ["3"] = "vestia" };
        };
        plpf =
        {
          plur = { ["1"] = "vestíramos", ["2"] = "vestíreis", ["3"] = "vestiram" };
          sing = { ["1"] = "vestira", ["2"] = "vestiras", ["3"] = "vestira" };
        };
        pres =
        {
          plur = { ["1"] = "vestimos", ["2"] = "vestis", ["3"] = "vestem" };
          sing = { ["1"] = "visto", ["2"] = "vestes", ["3"] = "veste" };
        };
        pret =
        {
          plur = { ["1"] = "vestimos", ["2"] = "vestistes", ["3"] = "vestiram" };
          sing = { ["1"] = "vesti", ["2"] = "vestiste", ["3"] = "vestiu" };
        };
      };
      infn =
      {
        impe = "vestir";
        pers =
        {
          plur = { ["1"] = "vestirmos", ["2"] = "vestirdes", ["3"] = "vestirem" };
          sing = { ["1"] = "vestir", ["2"] = "vestires", ["3"] = "vestir" };
        };
      };
      part_past =
      {
        plur = { f = "vestidas", m = "vestidos" };
        sing = { f = "vestida", m = "vestido" };
      };
      part_pres = "vestindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "vestirmos", ["2"] = "vestirdes", ["3"] = "vestirem" };
          sing = { ["1"] = "vestir", ["2"] = "vestires", ["3"] = "vestir" };
        };
        impf =
        {
          plur = { ["1"] = "vestíssemos", ["2"] = "vestísseis", ["3"] = "vestissem" };
          sing = { ["1"] = "vestisse", ["2"] = "vestisses", ["3"] = "vestisse" };
        };
        pres =
        {
          plur = { ["1"] = "vistamos", ["2"] = "vistais", ["3"] = "vistam" };
          sing = { ["1"] = "vista", ["2"] = "vistas", ["3"] = "vista" };
        };
      };
    };
    irregular = false;
    suffix = "ir";
    verb = "vestir";
  };
  vir =
  {
    comments = { };
    examples = { "vir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "venhamos", ["2"] = "vinde", ["3"] = "venham" };
          sing = { ["2"] = "vem", ["3"] = "venha" };
        };
        negt =
        {
          plur = { ["1"] = "venhamos", ["2"] = "venhais", ["3"] = "venham" };
          sing = { ["2"] = "venhas", ["3"] = "venha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "viríamos", ["2"] = "viríeis", ["3"] = "viriam" };
          sing = { ["1"] = "viria", ["2"] = "virias", ["3"] = "viria" };
        };
        futu =
        {
          plur = { ["1"] = "viremos", ["2"] = "vireis", ["3"] = "virão" };
          sing = { ["1"] = "virei", ["2"] = "virás", ["3"] = "virá" };
        };
        impf =
        {
          plur = { ["1"] = "vínhamos", ["2"] = "vínheis", ["3"] = "vinham" };
          sing = { ["1"] = "vinha", ["2"] = "vinhas", ["3"] = "vinha" };
        };
        plpf =
        {
          plur = { ["1"] = "viéramos", ["2"] = "viéreis", ["3"] = "vieram" };
          sing = { ["1"] = "viera", ["2"] = "vieras", ["3"] = "viera" };
        };
        pres =
        {
          plur = { ["1"] = "vimos", ["2"] = "vindes", ["3"] = "vêm" };
          sing = { ["1"] = "venho", ["2"] = "véns", ["3"] = "vem" };
        };
        pret =
        {
          plur = { ["1"] = "viemos", ["2"] = "viestes", ["3"] = "vieram" };
          sing = { ["1"] = "vim", ["2"] = "vieste", ["3"] = "veio" };
        };
      };
      infn =
      {
        impe = "vir";
        pers =
        {
          plur = { ["1"] = "virmos", ["2"] = "virdes", ["3"] = "virem" };
          sing = { ["1"] = "vir", ["2"] = "vires", ["3"] = "vir" };
        };
      };
      part_past =
      {
        plur = { f = "vindas", m = "vindos" };
        sing = { f = "vinda", m = "vindo" };
      };
      part_pres = "vindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "viermos", ["2"] = "vierdes", ["3"] = "vierem" };
          sing = { ["1"] = "vier", ["2"] = "vieres", ["3"] = "vier" };
        };
        impf =
        {
          plur = { ["1"] = "viéssemos", ["2"] = "viésseis", ["3"] = "viessem" };
          sing = { ["1"] = "viesse", ["2"] = "viesses", ["3"] = "viesse" };
        };
        pres =
        {
          plur = { ["1"] = "venhamos", ["2"] = "venhais", ["3"] = "venham" };
          sing = { ["1"] = "venha", ["2"] = "venhas", ["3"] = "venha" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "vir";
  };
  vir2 =
  {
    comments = { };
    examples = { "advir", "avir", "convir", "desavir", "intervir", "provir", "sobrevir" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "venhamos", ["2"] = "vinde", ["3"] = "venham" };
          sing = { ["2"] = "vém", ["3"] = "venha" };
        };
        negt =
        {
          plur = { ["1"] = "venhamos", ["2"] = "venhais", ["3"] = "venham" };
          sing = { ["2"] = "venhas", ["3"] = "venha" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "viríamos", ["2"] = "viríeis", ["3"] = "viriam" };
          sing = { ["1"] = "viria", ["2"] = "virias", ["3"] = "viria" };
        };
        futu =
        {
          plur = { ["1"] = "viremos", ["2"] = "vireis", ["3"] = "virão" };
          sing = { ["1"] = "virei", ["2"] = "virás", ["3"] = "virá" };
        };
        impf =
        {
          plur = { ["1"] = "vínhamos", ["2"] = "vínheis", ["3"] = "vinham" };
          sing = { ["1"] = "vinha", ["2"] = "vinhas", ["3"] = "vinha" };
        };
        plpf =
        {
          plur = { ["1"] = "viéramos", ["2"] = "viéreis", ["3"] = "vieram" };
          sing = { ["1"] = "viera", ["2"] = "vieras", ["3"] = "viera" };
        };
        pres =
        {
          plur = { ["1"] = "vimos", ["2"] = "vindes", ["3"] = "vêm" };
          sing = { ["1"] = "venho", ["2"] = "véns", ["3"] = "vém" };
        };
        pret =
        {
          plur = { ["1"] = "viemos", ["2"] = "viestes", ["3"] = "vieram" };
          sing = { ["1"] = "vim", ["2"] = "vieste", ["3"] = "veio" };
        };
      };
      infn =
      {
        impe = "vir";
        pers =
        {
          plur = { ["1"] = "virmos", ["2"] = "virdes", ["3"] = "virem" };
          sing = { ["1"] = "vir", ["2"] = "vires", ["3"] = "vir" };
        };
      };
      part_past =
      {
        plur = { f = "vindas", m = "vindos" };
        sing = { f = "vinda", m = "vindo" };
      };
      part_pres = "vindo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "viermos", ["2"] = "vierdes", ["3"] = "vierem" };
          sing = { ["1"] = "vier", ["2"] = "vieres", ["3"] = "vier" };
        };
        impf =
        {
          plur = { ["1"] = "viéssemos", ["2"] = "viésseis", ["3"] = "viessem" };
          sing = { ["1"] = "viesse", ["2"] = "viesses", ["3"] = "viesse" };
        };
        pres =
        {
          plur = { ["1"] = "venhamos", ["2"] = "venhais", ["3"] = "venham" };
          sing = { ["1"] = "venha", ["2"] = "venhas", ["3"] = "venha" };
        };
      };
    };
    irregular = true;
    suffix = "ir";
    verb = "vir2";
  };
  volver =
  {
    abundant = true;
    comments =
    {
      "There are additional, shorter past participles, the masculine singular ending in -volto.";
    };
    examples = { "desenvolver", "revolver" };
    forms =
    {
      impe =
      {
        affr =
        {
          plur = { ["1"] = "volvamos", ["2"] = "volvei", ["3"] = "volvam" };
          sing = { ["1"] = "volva", ["2"] = "volve", ["3"] = "volva" };
        };
        negt =
        {
          plur = { ["1"] = "volvamos", ["2"] = "volvais", ["3"] = "volvam" };
          sing = { ["1"] = "volva", ["2"] = "volvas", ["3"] = "volva" };
        };
      };
      indi =
      {
        cond =
        {
          plur = { ["1"] = "volveríamos", ["2"] = "volveríeis", ["3"] = "volveriam" };
          sing = { ["1"] = "volveria", ["2"] = "volverias", ["3"] = "volveria" };
        };
        futu =
        {
          plur = { ["1"] = "volveremos", ["2"] = "volvereis", ["3"] = "volverão" };
          sing = { ["1"] = "volverei", ["2"] = "volverás", ["3"] = "volverá" };
        };
        impf =
        {
          plur = { ["1"] = "volvíamos", ["2"] = "volvíeis", ["3"] = "volviam" };
          sing = { ["1"] = "volvia", ["2"] = "volvias", ["3"] = "volvia" };
        };
        plpf =
        {
          plur = { ["1"] = "volvêramos", ["2"] = "volvêreis", ["3"] = "volveram" };
          sing = { ["1"] = "volvera", ["2"] = "volveras", ["3"] = "volvera" };
        };
        pres =
        {
          plur = { ["1"] = "volvemos", ["2"] = "volveis", ["3"] = "volvem" };
          sing = { ["1"] = "volvo", ["2"] = "volves", ["3"] = "volve" };
        };
        pret =
        {
          plur = { ["1"] = "volvemos", ["2"] = "volvestes", ["3"] = "volveram" };
          sing = { ["1"] = "volvi", ["2"] = "volveste", ["3"] = "volveu" };
        };
      };
      infn =
      {
        impe = "volver";
        pers =
        {
          plur = { ["1"] = "volvermos", ["2"] = "volverdes", ["3"] = "volverem" };
          sing = { ["1"] = "volver", ["2"] = "volveres", ["3"] = "volver" };
        };
      };
      part_past =
      {
        plur = { f = "voltas", m = "voltos" };
        sing = { f = "volta", m = "volto" };
      };
      part_pres = "volvendo";
      subj =
      {
        futu =
        {
          plur = { ["1"] = "volvermos", ["2"] = "volverdes", ["3"] = "volverem" };
          sing = { ["1"] = "volver", ["2"] = "volveres", ["3"] = "volver" };
        };
        impf =
        {
          plur = { ["1"] = "volvêssemos", ["2"] = "volvêsseis", ["3"] = "volvessem" };
          sing = { ["1"] = "volvesse", ["2"] = "volvesses", ["3"] = "volvesse" };
        };
        pres =
        {
          plur = { ["1"] = "volvamos", ["2"] = "volvais", ["3"] = "volvam" };
          sing = { ["1"] = "volva", ["2"] = "volvas", ["3"] = "volva" };
        };
      };
    };
    suffix = "er";
    verb = "volver";
  };
}
