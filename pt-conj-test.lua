#!/usr/bin/env lua
require('luaunit')

function inflect(stem, type)
  return lookup(stem, type).forms
end

function lookup(stem, type)
  local pt_conj = require('pt-conj')
  return pt_conj.inflect(stem, type)
end

function describe(stem, type, compound)
    local pt_conj = require('pt-conj')
    return pt_conj.describe(stem, type, compound)
end

TestInflection = {}
function TestInflection:testInflectWithAr()
   local inflected = inflect('trabalh', 'ar')
   assertIsTable(inflected)
   assertEquals(inflected.indi.pres.sing['1'], 'trabalho')
end

function TestInflection:testInflectWithCaber()
   local inflected = inflect('', 'caber')
   assertIsTable(inflected)
   assertEquals(inflected.indi.pres.sing['1'], 'caibo')
end

function TestInflection:testLookupWithAr()
   local data = lookup('trabalh', 'ar')
   assertEquals(data.forms.indi.pres.sing['1'], 'trabalho')
   assertEquals(data.comments, { })
   assertEquals(data.examples, {
        'amar',
        'cantar',
        'gritar',
        'marchar',
        'mostrar',
        'nadar',
        'parar',
        'participar',
        'retirar',
        'separar',
        'viajar'})
end

function TestInflection:testDescribeWithCaber()
    local data = describe('', 'caber')
    assertEquals(data, 'foo')
end

lu = LuaUnit.new()
lu:setOutputType('tap')
os.exit(lu:runSuite())
