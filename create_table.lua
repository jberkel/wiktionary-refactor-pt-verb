#!/usr/bin/env lua

require('mw_html')
local inflector = require 'pt-conj'
local creator =  require('pt-conj-create-table')

local verbdata = inflector.inflect('', 'matar')

print(creator.createNavAndTable(verbdata))
