require 'lua-nucleo'
local json = require 'dkjson'
local tpretty = require 'lua-nucleo/tpretty'

local input = io.read("*all")
local data = json.decode(input)
print(tpretty.tpretty_ordered(data))

